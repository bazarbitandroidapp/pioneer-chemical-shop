package com.pureweblopment.pioneerchemicalshop.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.SendMail;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;
import com.pureweblopment.pioneerchemicalshop.Model.Sliders;
import com.pureweblopment.pioneerchemicalshop.R;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

public class FullViewImageAdapter extends PagerAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<Sliders> sliderses;

    public FullViewImageAdapter(Context context, ArrayList<Sliders> sliderses) {
        this.context = context;
        this.sliderses = sliderses;
    }

    @Override
    public int getCount() {
        return sliderses.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.row_items_full_view_images, container, false);

        // Locate the ImageView in viewpager_item.xml
        ImageView mImgFullView = itemView.findViewById(R.id.imgFullView);
        final RelativeLayout mRlImgHolder = itemView.findViewById(R.id.rlImgHolder);
        ProgressBar mPbImgHolder =itemView.findViewById(R.id.pbImgHolder);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mPbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
            }
        }
        String picUrl = null;
        try {
            URL urla = null;
            urla = new URL(sliderses.get(position).getImageurl().replaceAll("%20", " "));
            URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
            picUrl = String.valueOf(urin.toURL());
            // Capture position and set to the ImageView
            Picasso.get()
                    .load(picUrl)
                    .into(mImgFullView, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            //holder.pbHome.setVisibility(View.INVISIBLE);
                            mRlImgHolder.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            //holder.pbHome.setVisibility(View.INVISIBLE);
                        }
                    });
        } catch (MalformedURLException e) {
            e.printStackTrace();
            //Creating SendMail object
            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
            //Executing sendmail to send email
            sm.execute();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            //Creating SendMail object
            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
            //Executing sendmail to send email
            sm.execute();
        }
        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);


        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}
