package com.pureweblopment.pioneerchemicalshop.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.pureweblopment.pioneerchemicalshop.Model.PaymentGateway;
import com.pureweblopment.pioneerchemicalshop.R;

import java.util.ArrayList;

public class PaymentGatewayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    Fragment mFragment;
    ArrayList<PaymentGateway> paymentGateways;
    SelectPaymentgateway mSelectPaymentgateway;

    public PaymentGatewayAdapter(Context mContext, Fragment mFragment, ArrayList<PaymentGateway> paymentGateways) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        this.paymentGateways = paymentGateways;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_paymentgateway, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            final PaymentGateway paymentGateway = paymentGateways.get(position);
            viewHolder.mTxtPaymentgateway.setText(paymentGateway.getName());

            if (paymentGateway.isSelect()) {
                viewHolder.mRbPaymentGateway.setChecked(true);
                mSelectPaymentgateway.selectedPaymentgateway(paymentGateway.getSlug(), paymentGateway.getPgconst());
            } else {
                viewHolder.mRbPaymentGateway.setChecked(false);
            }

            viewHolder.mLlPaymentGateway.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectPaymentgateway = (SelectPaymentgateway) mFragment;
                    for (int i = 0; i < paymentGateways.size(); i++) {
                        PaymentGateway paymentGateway = paymentGateways.get(i);
                        if (i == position) {
                            paymentGateway.setSelect(true);
                        } else {
                            paymentGateway.setSelect(false);
                        }
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return paymentGateways.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mLlPaymentGateway;
        private RadioButton mRbPaymentGateway;
        private TextView mTxtPaymentgateway;

        public ViewHolder(View itemView) {
            super(itemView);
            mLlPaymentGateway = itemView.findViewById(R.id.llPaymentgateway);
            mRbPaymentGateway = itemView.findViewById(R.id.rbPaymentGateway);
            mTxtPaymentgateway = itemView.findViewById(R.id.txtPaymentgateway);

            mTxtPaymentgateway.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        }
    }

    public interface SelectPaymentgateway {
        void selectedPaymentgateway(String PaymentgatewayName, String PaymentgatewayConst);
    }
}
