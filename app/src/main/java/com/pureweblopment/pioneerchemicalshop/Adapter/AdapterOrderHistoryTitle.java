package com.pureweblopment.pioneerchemicalshop.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;
import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.pureweblopment.pioneerchemicalshop.Model.Footer;
import com.pureweblopment.pioneerchemicalshop.Model.Item;
import com.pureweblopment.pioneerchemicalshop.Model.OrderHistory;
import com.pureweblopment.pioneerchemicalshop.Model.OrderHistoryItems;
import com.pureweblopment.pioneerchemicalshop.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by divya on 20/12/17.
 */

public class AdapterOrderHistoryTitle extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_POST = 0;
    private static final int TYPE_FOOTER = 1;
    Context context;
    List<Item> orders;
    OrderDetail orderDetail;


    public AdapterOrderHistoryTitle(Context context, List<Item> orders) {
        this.orders = orders;
        this.context = context;
    }

    public void clearData() {
        int size = orders.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                orders.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (orders.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            return TYPE_POST;
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        public FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.footer);
        }

        public ProgressBar getProgressBar() {
            return progressBar;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == TYPE_POST) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_orderhistory_title, parent, false);
            return new ViewHolder(view);
        } else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_footer, parent, false);
            FooterViewHolder vh = new FooterViewHolder(row);
            return vh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, final int position) {
        if (viewholder instanceof ViewHolder) {
            final ViewHolder paletteViewHolder = (ViewHolder) viewholder;
            final OrderHistory orderHistory = (OrderHistory) orders.get(position);
            if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID) != null) {
                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID).equals("")) {
                    paletteViewHolder.txtProductOrderID.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID) + " "
                            + orderHistory.getOrder_id());
                }else {
                    paletteViewHolder.txtProductOrderID.setText(context.getString(R.string.order_id) + " "
                            + orderHistory.getOrder_id());
                }
            }else {
                paletteViewHolder.txtProductOrderID.setText(context.getString(R.string.order_id) + " "
                        + orderHistory.getOrder_id());
            }

            if (orderHistory.getIs_prebooking() != null) {
                if (orderHistory.getIs_prebooking().equalsIgnoreCase("1")) {
                    paletteViewHolder.mTxtPrebooking.setVisibility(View.VISIBLE);
                } else {
                    paletteViewHolder.mTxtPrebooking.setVisibility(View.GONE);
                }
            } else {
                paletteViewHolder.mTxtPrebooking.setVisibility(View.GONE);
            }
            ArrayList<OrderHistoryItems> orderHistoryItemses = orderHistory.getArrayOrderHistoryItems();
            AdapterOrderHistory adapterOrderHistory =
                    new AdapterOrderHistory(context, orderHistoryItemses, orderHistory.getOrder_payment_type());
            paletteViewHolder.recyclerviewOrderItems.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            paletteViewHolder.recyclerviewOrderItems.setAdapter(adapterOrderHistory);

            paletteViewHolder.llOrderHistory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String order_id = orderHistory.getOrder_id();
                    orderDetail = (OrderDetail) context;
                    orderDetail.gotoOrderDetailscreen(order_id);
                    SharedPreference.CreatePreference(context, StaticUtility.PREFERENCEOrderHistory);
                    SharedPreference.SavePreference(StaticUtility.strOrderHistoryLastClick, String.valueOf(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtProductOrderID, mTxtPrebooking;
        RecyclerView recyclerviewOrderItems;
        LinearLayout llOrderHistory;

        public ViewHolder(View itemView) {
            super(itemView);
            txtProductOrderID = itemView.findViewById(R.id.txtProductOrderID);
            mTxtPrebooking = itemView.findViewById(R.id.txtPrebooking);
            recyclerviewOrderItems = itemView.findViewById(R.id.recyclerviewOrderItems);
            llOrderHistory = itemView.findViewById(R.id.llOrderHistory);

            txtProductOrderID.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRE_BOOKING) != null) {
                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRE_BOOKING).equalsIgnoreCase("")) {
                    mTxtPrebooking.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPRE_BOOKING));
                } else {
                    mTxtPrebooking.setText(context.getString(R.string.pre_booking));
                }
            } else {
                mTxtPrebooking.setText(context.getString(R.string.pre_booking));
            }
            txtProductOrderID.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        }
    }

    public interface OrderDetail {
        void gotoOrderDetailscreen(String Order_Id);
    }
}
