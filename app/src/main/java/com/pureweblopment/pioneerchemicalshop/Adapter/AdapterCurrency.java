package com.pureweblopment.pioneerchemicalshop.Adapter;

import android.content.Context;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;
import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.pureweblopment.pioneerchemicalshop.Model.Currency;
import com.pureweblopment.pioneerchemicalshop.R;

import java.util.ArrayList;

public class AdapterCurrency extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    ArrayList<Currency> currencies = new ArrayList<>();
    CurrencySelection currencySelection;
    String strSelectedCurrency = "";
    String strType = "", strCurrencyCode = "";

    public AdapterCurrency(Context context, ArrayList<Currency> currencies,
                           String strType, String strCurrencyCode) {
        this.mContext = context;
        this.currencies = currencies;
        this.strType = strType;
        this.strCurrencyCode = strCurrencyCode;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout_currency, viewGroup, false);
        return new AdapterCurrency.Viewholder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof Viewholder) {
            final Viewholder viewholder = (Viewholder) holder;

            final Currency currency = currencies.get(position);

           if(strCurrencyCode.equalsIgnoreCase(currency.getCode())){
               viewholder.mImgSelect.setVisibility(View.VISIBLE);
               viewholder.mTxtCurrency.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
           }else {
               viewholder.mImgSelect.setVisibility(View.GONE);
               viewholder.mTxtCurrency.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
           }
            /*if (strSelectedCurrency.equalsIgnoreCase(currency.getCurrency_id())) {
                viewholder.mImgSelect.setVisibility(View.VISIBLE);
                viewholder.mTxtCurrency.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
            } else {
                viewholder.mImgSelect.setVisibility(View.GONE);
                viewholder.mTxtCurrency.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }*/


            viewholder.mTxtCurrency.setText(currency.getCurrency());

            viewholder.mLlCurrency.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    currencySelection = (CurrencySelection) mContext;
                    strSelectedCurrency = currency.getCurrency_id();
                    strCurrencyCode = currency.getCode();
                    notifyDataSetChanged();
                    currencySelection.SelectedCurrency(currency.getCode(), currency.getCurrency());
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return currencies.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private TextView mTxtCurrency;
        private LinearLayout mLlCurrency;
        private View mViewDivider;
        private ImageView mImgSelect;

        public Viewholder(View itemView) {
            super(itemView);
            mTxtCurrency = itemView.findViewById(R.id.txtCurrency);
            mLlCurrency = itemView.findViewById(R.id.llCurrency);
            mViewDivider = itemView.findViewById(R.id.viewDivider);
            mImgSelect = itemView.findViewById(R.id.imgSelect);

            mTxtCurrency.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            mTxtCurrency.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            mImgSelect.setColorFilter(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

        }
    }

    public interface CurrencySelection {
        void SelectedCurrency(String strCurrencyCode, String strCurrency);
    }
}
