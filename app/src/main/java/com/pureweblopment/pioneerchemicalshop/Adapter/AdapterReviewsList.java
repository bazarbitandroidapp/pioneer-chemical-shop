package com.pureweblopment.pioneerchemicalshop.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.pureweblopment.pioneerchemicalshop.Model.Reviews;

import java.util.ArrayList;

import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;

import com.pureweblopment.pioneerchemicalshop.R;

/**
 * Created by divya on 16/9/17.
 */

public class AdapterReviewsList extends RecyclerView.Adapter<AdapterReviewsList.Viewholder> {

    Context context;
    ArrayList<Reviews> reviewses;

    public AdapterReviewsList(Context context, ArrayList<Reviews> reviewses) {
        this.context = context;
        this.reviewses = reviewses;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = null;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_reviews, viewGroup, false);
        return new AdapterReviewsList.Viewholder(view);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, int position) {
        Reviews reviews = reviewses.get(position);
        holder.txtUserName.setText(reviews.getUserName());
        holder.txtReviewDate.setText(Global.changeDateFormate(reviews.getReviewsDate(), "yyyy-MM-dd hh:mm:ss", "dd-MMM-yyyy"));
        holder.txtReviewDescription.setText(reviews.getDescription());
        holder.reviewRatingbar.setRating(Float.valueOf(reviews.getRating()));
    }

    @Override
    public int getItemCount() {
        return reviewses.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        TextView txtUserName, txtReviewDate, txtReviewDescription;
        RatingBar reviewRatingbar;

        public Viewholder(View itemView) {
            super(itemView);
            txtUserName = (TextView) itemView.findViewById(R.id.txtUserName);
            txtReviewDate = (TextView) itemView.findViewById(R.id.txtReviewDate);
            txtReviewDescription = (TextView) itemView.findViewById(R.id.txtReviewDescription);
            reviewRatingbar = (RatingBar) itemView.findViewById(R.id.reviewRatingbar);

            txtUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtReviewDate.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtReviewDescription.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                txtUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor) != "") {
                txtReviewDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                txtReviewDescription.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            }
           /* LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.yellow), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(context.getResources().getColor(R.color.yellow), PorterDuff.Mode.SRC_ATOP);*/

            LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
               /* stars.getDrawable(2).setColorFilter(Color.parseColor("#FFFF99"), PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);*/
            stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        }
    }
}
