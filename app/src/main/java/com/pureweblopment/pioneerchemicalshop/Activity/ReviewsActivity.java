package com.pureweblopment.pioneerchemicalshop.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.SendMail;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;
import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.pureweblopment.pioneerchemicalshop.Model.Footer;
import com.pureweblopment.pioneerchemicalshop.Model.Item;
import com.pureweblopment.pioneerchemicalshop.Model.Review;
import com.pureweblopment.pioneerchemicalshop.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class ReviewsActivity extends AppCompatActivity {

    RecyclerView recyclerviewReviews;
    RelativeLayout relativeProgress;
    LinearLayout llReviewsEmpty;
    TextView txtReviewsempty;

    ImageView imageCartBack;
    ProgressBar progress;
    TextView txtCatName;
    FrameLayout flToolbar;

    private List<Item> postdata;
    private ArrayList<HashMap<String, String>> postList = new ArrayList<HashMap<String, String>>();
    private boolean hasMore;
    int limit = 10, offset = 0;
    private boolean flag = true, isFooter = true;
    AdapterReviewsList adapterReviewsList;

    Context context = ReviewsActivity.this;

    //Internet Alert
    public EventBus eventBus = EventBus.getDefault();
    public static int i = 0;
    public static AlertDialog internetAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_reviews);
        eventBus.register(this);
        Initialization();
        TypeFace();
        AppSettings();

        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onBackPressed();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        if (postdata != null) {
            try {
                adapterReviewsList.clearData();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            postList.clear();
            postdata.clear();
        }
        getReviews(true);

        recyclerviewReviews.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (hasMore && !(hasFooter())) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    //position starts at 0
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int itemCount = layoutManager.getItemCount();
                    if (layoutManager.findLastVisibleItemPosition() >= layoutManager.getItemCount() - 5) {
                        flag = true;
                        getReviews(false);
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void AppSettings() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }
        flToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtReviewsempty.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
    }

    private void Initialization() {
        flToolbar = findViewById(R.id.flToolbar);
        progress = findViewById(R.id.progress);
        imageCartBack = findViewById(R.id.imageCartBack);
        txtCatName = findViewById(R.id.txtCatName);
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREVIEW) != null) {
        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREVIEW).equals("")) {
            txtCatName.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREVIEW));
        }else {
            txtCatName.setText(R.string.reviews);
        }
        } else {
            txtCatName.setText(R.string.reviews);
        }
        recyclerviewReviews = findViewById(R.id.recyclerviewReviews);
        relativeProgress = findViewById(R.id.relativeProgress);
        llReviewsEmpty = findViewById(R.id.llReviewsEmpty);
        txtReviewsempty = findViewById(R.id.txtReviewsempty);
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO_REVIEW_AVAILABLE) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO_REVIEW_AVAILABLE).equals("")) {
                txtReviewsempty.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO_REVIEW_AVAILABLE));
            } else {
                txtReviewsempty.setText(R.string.no_review);
            }
        } else {
            txtReviewsempty.setText(R.string.no_review);
        }
    }

    private void TypeFace() {
        txtReviewsempty.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
    }

    private boolean hasFooter() {
        return postdata.get(postdata.size() - 1) instanceof Footer;
    }

    //region FOR getReviews API..
    private void getReviews(final boolean isSetUp) {
        relativeProgress.setVisibility(View.VISIBLE);

        if (!isSetUp) {
            if (isFooter) {
                isFooter = false;
                postdata.add(new Footer());
                recyclerviewReviews.getAdapter().notifyItemInserted(postdata.size() - 1);
            }
        }

        String[] key = {"offset", "limit"};
        String[] val = {String.valueOf(offset), String.valueOf(limit)};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.ALLReviews);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
//                                reviewses = new ArrayList<>();
                                if (strStatus.equals("ok")) {
                                    Object object = response.get("payload");
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    JSONArray jsonArrayReviews = jsonObjectPayload.getJSONArray("reviews");
                                    ArrayList<Item> reviews = new ArrayList<Item>(6);
                                    if (jsonArrayReviews.length() > 0) {
                                        for (int i = 0; i < jsonArrayReviews.length(); i++) {
                                            JSONObject jsonObjectReviews = jsonArrayReviews.getJSONObject(i);
                                            String Username = jsonObjectReviews.getString("name");
                                            String description = jsonObjectReviews.getString("description");
                                            String Reviewdate = jsonObjectReviews.getString("added_on");
                                            String rating = jsonObjectReviews.getString("rating");
                                            Review review = new Review(Username, Reviewdate, rating, description);
                                            reviews.add(review);
                                        }
                                        if (!isSetUp) {
                                            offset = offset + limit;

                                            if (reviews.size() > 0 && hasMore) {
                                                hasMore = true;
                                                isFooter = true;
                                                int size = postdata.size();
                                                postdata.remove(size - 1);
                                                postdata.addAll(reviews);
                                                recyclerviewReviews.getAdapter().notifyDataSetChanged();
                                            } else {
                                                hasMore = false;
                                                isFooter = true;
                                                int size = postdata.size();
                                                postdata.remove(size - 1);
                                                postdata.addAll(reviews);
                                                recyclerviewReviews.getAdapter().notifyDataSetChanged();
                                                llReviewsEmpty.setVisibility(View.VISIBLE);
                                            }
                                            if (limit > reviews.size()) {
                                                hasMore = false;
                                            }
                                        } else {
                                            postdata = reviews;
                                            offset = offset + limit;

                                            if (postdata.size() > 0) {
                                                hasMore = true;
                                                llReviewsEmpty.setVisibility(View.GONE);
                                                recyclerviewReviews.setVisibility(View.VISIBLE);
                                                AdapterReviewsList adapterReviewsList = new AdapterReviewsList(context, postdata);
                                                recyclerviewReviews.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                                                recyclerviewReviews.setAdapter(adapterReviewsList);
                                            } else {
                                                hasMore = false;
                                                llReviewsEmpty.setVisibility(View.VISIBLE);
                                                recyclerviewReviews.setVisibility(View.GONE);
                                            }
                                        }

                                    } else {
                                        if (!isSetUp) {
                                            hasMore = false;
                                            isFooter = true;
                                            int size = postdata.size();
                                            postdata.remove(size - 1);
                                            postdata.addAll(reviews);
                                            recyclerviewReviews.getAdapter().notifyDataSetChanged();
                                        } else {
                                            hasMore = false;
                                            llReviewsEmpty.setVisibility(View.VISIBLE);
                                            recyclerviewReviews.setVisibility(View.GONE);
                                        }

                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ReviewsFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterReviewsList..
    public class AdapterReviewsList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private static final int TYPE_POST = 0;
        private static final int TYPE_FOOTER = 1;
        Context context;
        List<Item> orders;

        public AdapterReviewsList(Context context, List<Item> orders) {
            this.orders = orders;
            this.context = context;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = null;
            if (viewType == TYPE_POST) {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_reviews, viewGroup, false);
                return new AdapterReviewsList.ViewHolder(view);
            } else {
                View row = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.progress_footer, viewGroup, false);
                AdapterReviewsList.FooterViewHolder vh = new AdapterReviewsList.FooterViewHolder(row);
                return vh;
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof AdapterReviewsList.ViewHolder) {
                final AdapterReviewsList.ViewHolder viewholder = (AdapterReviewsList.ViewHolder) holder;
                Review reviews = (Review) orders.get(position);
                viewholder.txtUserName.setText(reviews.getUserName());
                viewholder.txtReviewDate.setText(Global.changeDateFormate(reviews.getReviewsDate(), "yyyy-MM-dd", "dd-MMM-yyyy"));
                viewholder.txtReviewDescription.setText(reviews.getDescription());
                viewholder.reviewRatingbar.setRating(Float.valueOf(reviews.getRating()));
            }
        }

        public void clearData() {
            int size = orders.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    orders.remove(0);
                }
                this.notifyItemRangeRemoved(0, size);
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (orders.get(position) instanceof Footer) {
                return TYPE_FOOTER;
            } else {
                return TYPE_POST;
            }
        }

        public class FooterViewHolder extends RecyclerView.ViewHolder {
            private ProgressBar progressBar;

            public FooterViewHolder(View itemView) {
                super(itemView);
                progressBar = (ProgressBar) itemView.findViewById(R.id.footer);
            }

            public ProgressBar getProgressBar() {
                return progressBar;
            }
        }

        @Override
        public int getItemCount() {
            return orders.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView txtUserName, txtReviewDate, txtReviewDescription;
            RatingBar reviewRatingbar, ratingBar;

            public ViewHolder(View itemView) {
                super(itemView);
                txtUserName = itemView.findViewById(R.id.txtUserName);
                txtReviewDate = itemView.findViewById(R.id.txtReviewDate);
                txtReviewDescription = itemView.findViewById(R.id.txtReviewDescription);
                reviewRatingbar = itemView.findViewById(R.id.reviewRatingbar);
                ratingBar = itemView.findViewById(R.id.ratingBar);

                txtUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtReviewDate.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtReviewDescription.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                txtUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtReviewDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                txtReviewDescription.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));

                LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

                LayerDrawable starRating = (LayerDrawable) ratingBar.getProgressDrawable();
                starRating.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                starRating.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

            }
        }
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = row.findViewById(R.id.tvTitle);
        final Button btnSettings = row.findViewById(R.id.btnSettings);
        final Button btnExit = row.findViewById(R.id.btnExit);

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF).equals("")) {
                tvTitle.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF));
            } else {
                tvTitle.setText(getText(R.string.your_data_is_off));
            }
        } else {
            tvTitle.setText(getText(R.string.your_data_is_off));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI).equals("")) {
                tvAlertText.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI));
            } else {
                tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
            }
        } else {
            tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS).equals("")) {
                btnSettings.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS));
            } else {
                btnSettings.setText(getText(R.string.settings));
            }
        } else {
            btnSettings.setText(getText(R.string.settings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT).equals("")) {
                btnExit.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT));
            } else {
                btnExit.setText(getText(R.string.exit));
            }
        } else {
            btnExit.setText(getText(R.string.exit));
        }

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion
}
