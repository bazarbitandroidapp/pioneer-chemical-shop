package com.pureweblopment.pioneerchemicalshop.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.pioneerchemicalshop.Fragment.CheckoutFragment;
import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.SendMail;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;
import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.pureweblopment.pioneerchemicalshop.Model.Country;
import com.pureweblopment.pioneerchemicalshop.Model.State;
import com.pureweblopment.pioneerchemicalshop.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class CheckoutAddAddressActivity extends AppCompatActivity implements View.OnClickListener {

    Context context = CheckoutAddAddressActivity.this;

    String strCountry = "", strState = "", strBillingCountry = "", strBillingState = "";
    private Country country;
    private State state;
    ArrayList<Country> countries = new ArrayList<>();
    ArrayList<State> states = new ArrayList<>();

    Button btnAddAddress;

    EditText editFirstName, editLastName, editAddress, editLandmark, editPincode, editCity, editPhoneNo;
    EditText editBillingFirstName, editBillingLastName, editBillingAddress, editBillingLandmark, editBillingPincode,
            editBillingCity, editBillingPhoneNo;
    TextView txtAddShippingAddress, txtAddBillingAddress, txtSameAsShippingAddress;

    LinearLayout llAddBillingAddress;
    CheckBox checkBoxBilling;

    RelativeLayout relativeProgress;

    CoordinatorLayout coodinator;
    Boolean ischeck = false;

    ImageView imageCartBack;
    LinearLayout llPaymentHeader, llToolbar;
    String ActivityType = "", AddressType = "", Type = "";

    AutoCompleteTextView actCountry, actState, actBillingCountry, actBillingState;
    TextView txtAddressName;
    TextView txtAddress, txtPayment, txtSuccess;

    FrameLayout flCountry, flState, flBillingCountry, flBillingState;

    LinearLayout llSameBillingCheck;
    LinearLayout llShippingAddress;
    LinearLayout llAddShippingAddress;
    TextView txtShippingAddress, txtUserNameForShipping, txtShippingLastAddress, txtShippingPhoneNo;
    private EventBus eventBus = EventBus.getDefault();
    private AlertDialog internetAlert;
    private int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_add_address);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        MainActivity.manageBackPress(false);
        eventBus.register(this);
        /*MainActivity.isCheckoutBack = "ADDAddress";*/

        ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        ActivityType = getIntent().getStringExtra("ActivityType");
        AddressType = getIntent().getStringExtra("AddressType");
        Type = getIntent().getStringExtra("Type");

        String strShippingUserName = getIntent().getStringExtra("ShippingUserName");
        String strShippingAddress = getIntent().getStringExtra("ShippingAddress");
        String strShippingPhoneNo = getIntent().getStringExtra("ShippingPhoneNo");

        Initialization();
        TypeFace();
        OnClickListener();
        AppSetting();
        setDynamicString();

        if (AddressType.equalsIgnoreCase("Billing Address")) {
            if (strShippingUserName != null) {
                if (!strShippingUserName.equalsIgnoreCase("")) {
                    llShippingAddress.setVisibility(View.VISIBLE);
                    llSameBillingCheck.setVisibility(View.GONE);
                    llAddBillingAddress.setVisibility(View.VISIBLE);
                    llAddShippingAddress.setVisibility(View.GONE);
                    txtUserNameForShipping.setText(strShippingUserName);
                    txtShippingLastAddress.setText(strShippingAddress);
                    txtShippingPhoneNo.setText(strShippingPhoneNo);
                } else {
                    llAddShippingAddress.setVisibility(View.VISIBLE);
                    llShippingAddress.setVisibility(View.GONE);
                    llSameBillingCheck.setVisibility(View.VISIBLE);
                    llAddBillingAddress.setVisibility(View.GONE);
                }
            } else {
                llAddShippingAddress.setVisibility(View.VISIBLE);
                llShippingAddress.setVisibility(View.GONE);
                llSameBillingCheck.setVisibility(View.VISIBLE);
                llAddBillingAddress.setVisibility(View.GONE);
            }
        } else {
            llAddShippingAddress.setVisibility(View.VISIBLE);
            llShippingAddress.setVisibility(View.GONE);
            llSameBillingCheck.setVisibility(View.VISIBLE);
            llAddBillingAddress.setVisibility(View.GONE);
        }

        /*if (ActivityType.equalsIgnoreCase("MainActivity")) {
            llPaymentHeader.setVisibility(View.GONE);
            llToolbar.setVisibility(View.VISIBLE);

        } else if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
            try {
                llPaymentHeader.setVisibility(View.VISIBLE);
                llToolbar.setVisibility(View.VISIBLE);
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    llPaymentHeader.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
                }
            } catch (NullPointerException e) {

            }
        } else {
            llToolbar.setVisibility(View.VISIBLE);
            llPaymentHeader.setVisibility(View.GONE);
        }*/

        chanageEditTextBorder(editFirstName);
        chanageEditTextBorder(editLastName);
        chanageEditTextBorder(editAddress);
        chanageEditTextBorder(editLandmark);
        chanageEditTextBorder(editPincode);
        chanageEditTextBorder(editCity);
        chanageEditTextBorder(editPhoneNo);
        chanageButton(btnAddAddress);
        chanageFramelayoutBorder(flCountry, actCountry);
        chanageFramelayoutBorder(flState, actState);
        chanageFramelayoutBorder(flBillingCountry, actBillingCountry);
        chanageFramelayoutBorder(flBillingState, actBillingState);
        chanageEditTextBorder(editBillingFirstName);
        chanageEditTextBorder(editBillingLastName);
        chanageEditTextBorder(editBillingAddress);
        chanageEditTextBorder(editBillingLandmark);
        chanageEditTextBorder(editBillingPincode);
        chanageEditTextBorder(editBillingCity);
        chanageEditTextBorder(editBillingPhoneNo);

        getCountryAPI();
        if (actCountry.length() > 0) {
            actState.setEnabled(true);
            getStateAPI();
        } else {
            actState.setEnabled(false);
        }

        if (actBillingCountry.length() > 0) {
            actBillingState.setEnabled(true);
            getBillingStateAPI();
        } else {
            actBillingState.setEnabled(false);
        }

        actCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    actState.setEnabled(true);
                    getStateAPI();
                } else {
                    actState.setEnabled(false);
                    actState.setText("");
                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE) != null) {
                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE).equals("")) {
                            actState.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE));
                        } else {
                            actState.setHint(getString(R.string.select_state));
                        }
                    } else {
                        actState.setHint(getString(R.string.select_state));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        actBillingCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    actBillingState.setEnabled(true);
                    getBillingStateAPI();
                } else {
                    actBillingState.setEnabled(false);
                    actBillingState.setText("");
                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE).equals("")) {
                        actBillingState.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE));
                    }else {
                        actBillingState.setHint(getString(R.string.select_state));
                    }
                    } else {
                        actBillingState.setHint(getString(R.string.select_state));
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    //region AppSetting..
    @SuppressLint("NewApi")
    private void AppSetting() {
        editFirstName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editLastName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editLandmark.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editPincode.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editCity.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

        editBillingFirstName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBillingLastName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBillingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBillingLandmark.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBillingPincode.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBillingCity.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBillingPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

        txtAddShippingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtAddBillingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtSameAsShippingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

        llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        llPaymentHeader.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtAddressName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtPayment.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtSuccess.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        checkBoxBilling.setButtonTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));

    }
    //endregion

    //region OnClickListener...
    private void OnClickListener() {
        btnAddAddress.setOnClickListener(this);
        checkBoxBilling.setOnClickListener(this);
        actCountry.setOnClickListener(this);
        actState.setOnClickListener(this);
        imageCartBack.setOnClickListener(this);
    }
    //endregion

    //region TypeFace...
    private void TypeFace() {
        editFirstName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editLastName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editLandmark.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editPincode.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editCity.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtAddShippingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtAddBillingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtSameAsShippingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnAddAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        editBillingLastName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editBillingFirstName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editBillingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editBillingLandmark.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editBillingPincode.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editBillingCity.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editBillingPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtAddressName.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtAddress.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtPayment.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtSuccess.setTypeface(Typefaces.TypefaceCalibri_bold(context));

        txtShippingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtUserNameForShipping.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtShippingLastAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtShippingPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
    }
    //endregion

    //region Initialization...
    private void Initialization() {
        btnAddAddress = findViewById(R.id.btnAddAddress);
        editFirstName = findViewById(R.id.editFirstName);
        editLastName = findViewById(R.id.editLastName);
        editAddress = findViewById(R.id.editAddress);
        editLandmark = findViewById(R.id.editLandmark);
        editPincode = findViewById(R.id.editPincode);
        editCity = findViewById(R.id.editCity);
        editPhoneNo = findViewById(R.id.editPhoneNo);
        relativeProgress = findViewById(R.id.relativeProgress);

        llAddBillingAddress = findViewById(R.id.llAddBillingAddress);
        checkBoxBilling = findViewById(R.id.checkBoxBilling);

        coodinator = findViewById(R.id.coodinator);

        txtAddBillingAddress = findViewById(R.id.txtAddBillingAddress);
        txtSameAsShippingAddress = findViewById(R.id.txtSameAsShippingAddress);
        txtAddShippingAddress = findViewById(R.id.txtAddShippingAddress);

        txtAddressName = findViewById(R.id.txtAddressName);
        txtAddress = findViewById(R.id.txtAddress);
        txtPayment = findViewById(R.id.txtPayment);
        txtSuccess = findViewById(R.id.txtSuccess);

        editBillingFirstName = findViewById(R.id.editBillingFirstName);
        editBillingLastName = findViewById(R.id.editBillingLastName);
        editBillingAddress = findViewById(R.id.editBillingAddress);
        editBillingLandmark = findViewById(R.id.editBillingLandmark);
        editBillingPincode = findViewById(R.id.editBillingPincode);
        editBillingCity = findViewById(R.id.editBillingCity);
        editBillingPhoneNo = findViewById(R.id.editBillingPhoneNo);

        llPaymentHeader = findViewById(R.id.llPaymentHeader);
        llToolbar = findViewById(R.id.llToolbar);
        imageCartBack = findViewById(R.id.imageCartBack);

        actCountry = findViewById(R.id.actCountry);
        actState = findViewById(R.id.actState);
        actCountry.setThreshold(1);
        actState.setThreshold(1);
        actBillingCountry = findViewById(R.id.actBillingCountry);
        actBillingState = findViewById(R.id.actBillingState);
        actBillingCountry.setThreshold(1);
        actBillingState.setThreshold(1);
        flCountry = findViewById(R.id.flCountry);
        flState = findViewById(R.id.flState);
        flBillingCountry = findViewById(R.id.flBillingCountry);
        flBillingState = findViewById(R.id.flBillingState);

        llAddShippingAddress = findViewById(R.id.llAddShippingAddress);

        llSameBillingCheck = findViewById(R.id.llSameBillingCheck);

        llShippingAddress = findViewById(R.id.llShippingAddress);
        txtShippingAddress = findViewById(R.id.txtShippingAddress);
        txtUserNameForShipping = findViewById(R.id.txtUserNameForShipping);
        txtShippingLastAddress = findViewById(R.id.txtShippingLastAddress);
        txtShippingPhoneNo = findViewById(R.id.txtShippingPhoneNo);
    }
    //endregion

    //region setDynamicString
    private void setDynamicString() {
        //region Shipping
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIREST_NAME) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIREST_NAME).equals("")) {
                editFirstName.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIREST_NAME));
            } else {
                editFirstName.setHint(getString(R.string.first_name));
            }
        } else {
            editFirstName.setHint(getString(R.string.first_name));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLAST_NAME) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLAST_NAME).equals("")) {
                editLastName.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLAST_NAME));
            } else {
                editLastName.setHint(getString(R.string.last_name));
            }
        } else {
            editLastName.setHint(getString(R.string.last_name));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS).equals("")) {
                editAddress.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS));
            } else {
                editAddress.setHint(getString(R.string.address));
            }
        } else {
            editAddress.setHint(getString(R.string.address));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK).equals("")) {
                editLandmark.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK));
            } else {
                editLandmark.setHint(getString(R.string.landmark));
            }
        } else {
            editLandmark.setHint(getString(R.string.landmark));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE).equals("")) {
                editPincode.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE));
            } else {
                editPincode.setHint(getString(R.string.pincode));
            }
        } else {
            editPincode.setHint(getString(R.string.pincode));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_COUNTRY) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_COUNTRY).equals("")) {
                actCountry.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_COUNTRY));
            } else {
                actCountry.setHint(getString(R.string.select_country));
            }
        } else {
            actCountry.setHint(getString(R.string.select_country));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE).equals("")) {
                actState.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE));
            } else {
                actState.setHint(getString(R.string.select_state));
            }
        } else {
            actState.setHint(getString(R.string.select_state));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY).equals("")) {
                editCity.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY));
            } else {
                editCity.setHint(getString(R.string.city));
            }
        } else {
            editCity.setHint(getString(R.string.city));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_NUMBER) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_NUMBER).equals("")) {
                editPhoneNo.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_NUMBER));
            } else {
                editPhoneNo.setHint(getString(R.string.phone_number));
            }
        } else {
            editPhoneNo.setHint(getString(R.string.phone_number));
        }
        //endregion

        //region Billing
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIREST_NAME) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIREST_NAME).equals("")) {
                editBillingFirstName.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIREST_NAME));
            } else {
                editBillingFirstName.setHint(getString(R.string.first_name));
            }
        } else {
            editBillingFirstName.setHint(getString(R.string.first_name));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLAST_NAME) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLAST_NAME).equals("")) {
                editBillingLastName.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLAST_NAME));
            } else {
                editBillingLastName.setHint(getString(R.string.last_name));
            }
        } else {
            editBillingLastName.setHint(getString(R.string.last_name));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS).equals("")) {
                editBillingAddress.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS));
            } else {
                editBillingAddress.setHint(getString(R.string.address));
            }
        } else {
            editBillingAddress.setHint(getString(R.string.address));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK).equals("")) {
                editBillingLandmark.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK));
            } else {
                editBillingLandmark.setHint(getString(R.string.landmark));
            }
        } else {
            editBillingLandmark.setHint(getString(R.string.landmark));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE).equals("")) {
                editBillingPincode.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE));
            } else {
                editBillingPincode.setHint(getString(R.string.pincode));
            }
        } else {
            editBillingPincode.setHint(getString(R.string.pincode));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_COUNTRY) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_COUNTRY).equals("")) {
                actBillingCountry.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_COUNTRY));
            } else {
                actBillingCountry.setHint(getString(R.string.select_country));
            }
        } else {
            actBillingCountry.setHint(getString(R.string.select_country));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE).equals("")) {
                actBillingState.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_STATE));
            } else {
                actBillingState.setHint(getString(R.string.select_state));
            }
        } else {
            actBillingState.setHint(getString(R.string.select_state));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY).equals("")) {
                editBillingCity.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY));
            } else {
                editBillingCity.setHint(getString(R.string.city));
            }
        } else {
            editBillingCity.setHint(getString(R.string.city));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_NUMBER) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_NUMBER).equals("")) {
                editBillingPhoneNo.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_NUMBER));
            } else {
                editBillingPhoneNo.setHint(getString(R.string.phone_number));
            }
        } else {
            editBillingPhoneNo.setHint(getString(R.string.phone_number));
        }
        //endregion

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSHIPPING_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSHIPPING_ADDRESS).equals("")) {
                txtShippingAddress.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSHIPPING_ADDRESS));
            } else {
                txtShippingAddress.setText(getString(R.string.shipping_address));
            }
        } else {
            txtShippingAddress.setText(getString(R.string.shipping_address));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBILLING_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBILLING_ADDRESS).equals("")) {
                txtAddBillingAddress.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBILLING_ADDRESS));
            } else {
                txtAddBillingAddress.setText(getString(R.string.billing_address));
            }
        } else {
            txtAddBillingAddress.setText(getString(R.string.billing_address));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSAME_AS_SHIPPING_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSAME_AS_SHIPPING_ADDRESS).equals("")) {
                txtSameAsShippingAddress.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSAME_AS_SHIPPING_ADDRESS));
            } else {
                txtSameAsShippingAddress.setText(getString(R.string.same_as_shipping_address));
            }
        } else {
            txtSameAsShippingAddress.setText(getString(R.string.same_as_shipping_address));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_ADDRESS).equals("")) {
                btnAddAddress.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_ADDRESS));
            } else {
                btnAddAddress.setText(getString(R.string.add_address));
            }
        } else {
            btnAddAddress.setText(getString(R.string.add_address));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS).equals("")) {
                txtAddressName.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS));
            } else {
                txtAddressName.setText(getString(R.string.myaddress));
            }
        } else {
            txtAddressName.setText(getString(R.string.myaddress));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS).equals("")) {
                txtAddress.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS));
            } else {
                txtAddress.setText(getString(R.string.address));
            }
        } else {
            txtAddress.setText(getString(R.string.address));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT).equals("")) {
                txtPayment.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT));
            } else {
                txtPayment.setText(getString(R.string.payment));
            }
        } else {
            txtPayment.setText(getString(R.string.payment));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS).equals("")) {
                txtSuccess.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS));
            } else {
                txtSuccess.setText(getString(R.string.success));
            }
        } else {
            txtSuccess.setText(getString(R.string.success));
        }
    }//endregion

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddAddress:
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(
                        btnAddAddress.getWindowToken(), 0);
                if (llShippingAddress.getVisibility() == View.VISIBLE) {
                    if (ValidationBilling()) {
                        AddBillingAddress();
                    }
                } else {
                    if (ischeck) {
                        if (ValidationShipping()) {
                            if ((ValidationBilling())) {
                                AddShippingAddress();
                            }
                        }
                    } else {
                        if (ValidationShipping()) {
                            AddShippingAddress();
                        }
                    }
                }

                /*if (ischeck) {
                    if (ValidationShipping()) {
                        AddShippingAddress("0");
                    }
                } else {
                    if (ValidationShipping()) {
                        AddShippingAddress("1");
                    }
                }*/
                break;
            case R.id.checkBoxBilling:
                if (checkBoxBilling.isChecked()) {
                    llAddBillingAddress.setVisibility(View.GONE);
                    ischeck = false;
                } else {
                    llAddBillingAddress.setVisibility(View.VISIBLE);
                    ischeck = true;
                }
                break;

            case R.id.actCountry:
                actCountry.showDropDown();
                break;

            case R.id.actState:
                actState.showDropDown();
                break;

            case R.id.imageCartBack:
                onBackPressed();
                finish();
                break;
        }
    }

    //region FOR GET Country API...
    private void getCountryAPI() {
        String[] key = {};
        String[] val = {};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.Countries);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        countries.clear();
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("OK")) {
                                    JSONArray payloadArray = response.getJSONArray("payload");
                                    for (int i = 0; i < payloadArray.length(); i++) {
                                        JSONObject jsonObjectProduct = payloadArray.getJSONObject(i);
                                        String CountryCode = jsonObjectProduct.getString("country_id");
                                        String Countryname = jsonObjectProduct.getString("country_name");

                                        country = new Country();
                                        country.setCountryName(Countryname);
                                        country.setCountryId(CountryCode);
                                        countries.add(country);
                                    }
                                    if (countries.size() > 0) {
                                        AutoCompleteForCountryAdapter autoCompleteForCountryAdapter = new AutoCompleteForCountryAdapter(context, R.layout.row_items, countries, actCountry);
                                        actCountry.setAdapter(autoCompleteForCountryAdapter);

                                        AutoCompleteForCountryAdapter autoCompleteForBillingCountryAdapter = new AutoCompleteForCountryAdapter(context, R.layout.row_items, countries, actBillingCountry);
                                        actBillingCountry.setAdapter(autoCompleteForBillingCountryAdapter);
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ADDAddressesFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER For COUNTRY...
    public class AutoCompleteForCountryAdapter extends ArrayAdapter<Country> {
        Context mContext;
        ArrayList<Country> mDepartments;
        ArrayList<Country> mDepartments_All;
        ArrayList<Country> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteForCountryAdapter(Context context, int resource, ArrayList<Country> departments, AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public Country getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final Country department = getItem(position);
                TextView name = convertView.findViewById(R.id.ItemName);
                TextView id = convertView.findViewById(R.id.ItemId);
                name.setText(department.getCountryName());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                /*id.setText(department.getCountryName());*/

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Country pi = getItem(position);
                        strCountry = pi.getCountryName();
                        if (!strCountry.equals("")) {
                            getStateAPI();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((Country) resultValue).getCountryName();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (Country department : mDepartments_All) {
                            if (department.getCountryName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof Country) {
                                mDepartments.add((Country) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region FOR GET State API...
    private void getStateAPI() {
        String[] key = {"country_name"};
        String[] val = {actCountry.getText().toString()};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.States);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        states.clear();
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("OK")) {
                                    JSONArray payloadArray = response.getJSONArray("payload");
                                    for (int i = 1; i < payloadArray.length(); i++) {
                                        JSONObject jsonObjectProduct = payloadArray.getJSONObject(i);
                                        String country_id = jsonObjectProduct.getString("country_id");
                                        String StateId = jsonObjectProduct.getString("state_id");
                                        String Statename = jsonObjectProduct.getString("state_name");
                                        String status = jsonObjectProduct.getString("status");

                                        state = new State();
                                        state.setStateName(Statename);
                                        state.setStateId(StateId);
                                        state.setCountry_id(country_id);
                                        state.setStatus(status);
                                        states.add(state);
                                    }
                                    if (states.size() > 0) {
                                        AutoCompleteForStateAdapter autoCompleteForStateAdapter = new AutoCompleteForStateAdapter(context, R.layout.row_items, states, actState);
                                        actState.setAdapter(autoCompleteForStateAdapter);
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ADDAddressesFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    // region FOR GET Billing State API...
    private void getBillingStateAPI() {
        String[] key = {"country_name"};
        String[] val = {actBillingCountry.getText().toString()};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.States);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        states.clear();
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("OK")) {
                                    JSONArray payloadArray = response.getJSONArray("payload");
                                    for (int i = 1; i < payloadArray.length(); i++) {
                                        JSONObject jsonObjectProduct = payloadArray.getJSONObject(i);
                                        String country_id = jsonObjectProduct.getString("country_id");
                                        String StateId = jsonObjectProduct.getString("state_id");
                                        String Statename = jsonObjectProduct.getString("state_name");
                                        String status = jsonObjectProduct.getString("status");

                                        state = new State();
                                        state.setStateName(Statename);
                                        state.setStateId(StateId);
                                        state.setCountry_id(country_id);
                                        state.setStatus(status);
                                        states.add(state);
                                    }
                                    if (states.size() > 0) {
                                        AutoCompleteForStateAdapter autoCompleteForStateAdapter = new AutoCompleteForStateAdapter(context, R.layout.row_items, states, actBillingState);
                                        actBillingState.setAdapter(autoCompleteForStateAdapter);
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ADDAddressesFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AUTO COMPLETE ADAPTER FOR STATE...
    public class AutoCompleteForStateAdapter extends ArrayAdapter<State> {
        Context mContext;
        ArrayList<State> mDepartments;
        ArrayList<State> mDepartments_All;
        ArrayList<State> mDepartments_Suggestion;
        int mLayoutResourceId;
        private AutoCompleteTextView autoCompleteTextView;

        public AutoCompleteForStateAdapter(Context context, int resource, ArrayList<State> departments, AutoCompleteTextView autoCompleteTextView) {
            super(context, resource, departments);
            this.mContext = context;
            this.mLayoutResourceId = resource;
            this.mDepartments = new ArrayList<>(departments);
            this.mDepartments_All = new ArrayList<>(departments);
            this.mDepartments_Suggestion = new ArrayList<>();
            this.autoCompleteTextView = autoCompleteTextView;
        }

        public int getCount() {
            return mDepartments.size();
        }

        public State getItem(int position) {
            return mDepartments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                    convertView = inflater.inflate(mLayoutResourceId, parent, false);
                }
                final State department = getItem(position);
                TextView name = convertView.findViewById(R.id.ItemName);
                TextView id = convertView.findViewById(R.id.ItemId);
                name.setText(department.getStateName());
                name.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                /*id.setText(department.getCountryName());*/

                autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        State pi = getItem(position);
                        strState = pi.getStateName();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                public String convertResultToString(Object resultValue) {
                    return ((State) resultValue).getStateName();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    if (constraint != null) {
                        mDepartments_Suggestion.clear();
                        for (State department : mDepartments_All) {
                            if (department.getStateName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                mDepartments_Suggestion.add(department);
                            }
                        }
                        FilterResults filterResults = new FilterResults();
                        filterResults.values = mDepartments_Suggestion;
                        filterResults.count = mDepartments_Suggestion.size();
                        return filterResults;
                    } else {
                        return new FilterResults();
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    mDepartments.clear();
                    if (results != null && results.count > 0) {
                        // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                        List<?> result = (List<?>) results.values;
                        for (Object object : result) {
                            if (object instanceof State) {
                                mDepartments.add((State) object);
                            }
                        }
                    } else if (constraint == null) {
                        // no filter, add entire original list back in
                        mDepartments.addAll(mDepartments_All);
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }
    //endregion

    //region FOR AddShippingAddress...
    private void AddShippingAddress() {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {"useraddress_id", "first_name", "last_name", "address", "landmark",
                "pincode", "country", "state", "city", "phone_number", "is_shipping"};
        String[] val = {"", editFirstName.getText().toString(), editLastName.getText().toString(), editAddress.getText().toString(),
                editLandmark.getText().toString(), editPincode.getText().toString(), actCountry.getText().toString(), actState.getText().toString(), editCity.getText().toString(),
                editPhoneNo.getText().toString(), "0"};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.ADDAddresses);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    Bundle bundleShipping = new Bundle();
                                    bundleShipping.putString("ShippingFname", editFirstName.getText().toString());
                                    bundleShipping.putString("ShippingLname", editLastName.getText().toString());
                                    bundleShipping.putString("ShippingAddress", editAddress.getText().toString());
                                    bundleShipping.putString("ShippingLandmark", editLandmark.getText().toString());
                                    bundleShipping.putString("ShippingPincode", editPincode.getText().toString());
                                    bundleShipping.putString("ShippingCountry", actCountry.getText().toString());
                                    bundleShipping.putString("ShippingState", actState.getText().toString());
                                    bundleShipping.putString("ShippingCity", editCity.getText().toString());
                                    bundleShipping.putString("ShippingPhoneNo", editPhoneNo.getText().toString());
                                    if (Type.equalsIgnoreCase("Checkout") && AddressType.equalsIgnoreCase("Shipping Address")) {
                                        CheckoutFragment.FirstShippingAddress(bundleShipping);
                                    } else if (Type.equalsIgnoreCase("Checkout") && AddressType.equalsIgnoreCase("Both")) {
                                        CheckoutFragment.FirstShippingAddress(bundleShipping);
                                    }
                                    if (ischeck) {
                                        AddBillingAddress();
                                    } else {
                                        AddSameShippingAddress();
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ADDAddressesFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region FOR AddBillingAddress...
    private void AddBillingAddress() {
        String[] key = {"useraddress_id", "first_name", "last_name", "address", "landmark", "pincode", "country", "state",
                "city", "phone_number", "is_shipping"};
        String[] val = {"", editBillingFirstName.getText().toString(), editBillingLastName.getText().toString(), editBillingAddress.getText().toString(),
                editBillingLandmark.getText().toString(), editBillingPincode.getText().toString(), actBillingCountry.getText().toString(), actBillingState.getText().toString(), editBillingCity.getText().toString(),
                editBillingPhoneNo.getText().toString(), "1"};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.ADDAddresses);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();

                                    Bundle bundleBilling = new Bundle();
                                    bundleBilling.putString("BillingFname", editBillingFirstName.getText().toString());
                                    bundleBilling.putString("BillingLname", editBillingLastName.getText().toString());
                                    bundleBilling.putString("BillingAddress", editBillingAddress.getText().toString());
                                    bundleBilling.putString("BillingLandmark", editBillingLandmark.getText().toString());
                                    bundleBilling.putString("BillingPincode", editBillingPincode.getText().toString());
                                    bundleBilling.putString("BillingCountry", actBillingCountry.getText().toString());
                                    bundleBilling.putString("BillingState", actBillingState.getText().toString());
                                    bundleBilling.putString("BillingCity", editBillingCity.getText().toString());
                                    bundleBilling.putString("BillingPhoneNo", editBillingPhoneNo.getText().toString());
                                    if (Type.equalsIgnoreCase("Checkout") && AddressType.equalsIgnoreCase("Billing Address")) {
                                        /*CheckoutFragment.isBillingAddress = true;*/
                                        CheckoutFragment.isBillingAddressChange = true;
                                        CheckoutFragment.isBillingAddress = false;
                                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                        CheckoutFragment.FirstBillingAddress(bundleBilling);
                                    } else if (Type.equalsIgnoreCase("Checkout") && AddressType.equalsIgnoreCase("Both")) {
                                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                        CheckoutFragment.isBillingAddress = false;
                                        CheckoutFragment.isBillingAddressChange = true;
                                        CheckoutFragment.FirstBillingAddress(bundleBilling);
                                    }

                                    editFirstName.setText("");
                                    editLastName.setText("");
                                    editAddress.setText("");
                                    editLandmark.setText("");
                                    editPincode.setText("");
                                    editCity.setText("");
                                    editPhoneNo.setText("");
                                    editBillingFirstName.setText("");
                                    editBillingLastName.setText("");
                                    editBillingAddress.setText("");
                                    editBillingLandmark.setText("");
                                    editBillingPincode.setText("");
                                    editBillingCity.setText("");
                                    editBillingPhoneNo.setText("");
                                    actCountry.setText("");
                                    actState.setText("");
                                    actBillingCountry.setText("");
                                    actBillingState.setText("");
                                    onBackPressed();
                                    finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ADDAddressesFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region FOR AddSameShippingAddress...
    private void AddSameShippingAddress() {
        String[] key = {"useraddress_id", "first_name", "last_name", "address", "landmark",
                "pincode", "country", "state", "city", "phone_number", "is_shipping"};
        String[] val = {"", editFirstName.getText().toString(), editLastName.getText().toString(), editAddress.getText().toString(),
                editLandmark.getText().toString(), editPincode.getText().toString(), actCountry.getText().toString(), actState.getText().toString(), editCity.getText().toString(),
                editPhoneNo.getText().toString(), "1"};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.ADDAddresses);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();

                                    Bundle bundleBilling = new Bundle();
                                    bundleBilling.putString("BillingFname", editFirstName.getText().toString());
                                    bundleBilling.putString("BillingLname", editLastName.getText().toString());
                                    bundleBilling.putString("BillingAddress", editAddress.getText().toString());
                                    bundleBilling.putString("BillingLandmark", editLandmark.getText().toString());
                                    bundleBilling.putString("BillingPincode", editPincode.getText().toString());
                                    bundleBilling.putString("BillingCountry", actCountry.getText().toString());
                                    bundleBilling.putString("BillingState", actState.getText().toString());
                                    bundleBilling.putString("BillingCity", editCity.getText().toString());
                                    bundleBilling.putString("BillingPhoneNo", editPhoneNo.getText().toString());
                                    if (Type.equalsIgnoreCase("Checkout") && AddressType.equalsIgnoreCase("Billing Address")) {
                                        /*CheckoutFragment.isBillingAddress = true;*/
                                        CheckoutFragment.isBillingAddressChange = true;
                                        CheckoutFragment.isBillingAddress = false;
                                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                        CheckoutFragment.FirstBillingAddress(bundleBilling);
                                    } else if (Type.equalsIgnoreCase("Checkout") && AddressType.equalsIgnoreCase("Both")) {
                                        SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                        CheckoutFragment.isBillingAddress = false;
                                        CheckoutFragment.isBillingAddressChange = true;
                                        CheckoutFragment.FirstBillingAddress(bundleBilling);
                                    }

                                    editFirstName.setText("");
                                    editLastName.setText("");
                                    editAddress.setText("");
                                    editLandmark.setText("");
                                    editPincode.setText("");
                                    editCity.setText("");
                                    editPhoneNo.setText("");
                                    editBillingFirstName.setText("");
                                    editBillingLastName.setText("");
                                    editBillingAddress.setText("");
                                    editBillingLandmark.setText("");
                                    editBillingPincode.setText("");
                                    editBillingCity.setText("");
                                    editBillingPhoneNo.setText("");
                                    actCountry.setText("");
                                    actState.setText("");
                                    actBillingCountry.setText("");
                                    actBillingState.setText("");
                                    onBackPressed();
                                    finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ADDAddressesFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(context, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    //region ValidationShipping
    public Boolean ValidationShipping() {
        Boolean valid = false;
        if (!TextUtils.isEmpty(editFirstName.getText().toString())) {
            editFirstName.setError(null);
            if (!TextUtils.isEmpty(editLastName.getText().toString())) {
                editLastName.setError(null);
                if (!TextUtils.isEmpty(editAddress.getText().toString())) {
                    editAddress.setError(null);
                    if (!TextUtils.isEmpty(editLandmark.getText().toString())) {
                        editLandmark.setError(null);
                        if (!TextUtils.isEmpty(editPincode.getText().toString())) {
                            editPincode.setError(null);
                            if (!TextUtils.isEmpty(actCountry.getText().toString())) {
                                actCountry.setError(null);
                                if (!TextUtils.isEmpty(actState.getText().toString())) {
                                    actState.setError(null);
                                    if (!TextUtils.isEmpty(editCity.getText().toString())) {
                                        editCity.setError(null);
                                        if (!TextUtils.isEmpty(editPhoneNo.getText().toString())) {
                                            editPhoneNo.setError(null);
                                            if (editPhoneNo.length() >= 10) {
                                                editPhoneNo.setError(null);
                                                valid = true;
                                            } else {
                                                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_VALID) != null) {
                                                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_VALID).equals("")) {
                                                        editPhoneNo.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_VALID));
                                                    } else {
                                                        editPhoneNo.setError(getString(R.string.valid_phone_number_error_message));
                                                    }
                                                } else {
                                                    editPhoneNo.setError(getString(R.string.valid_phone_number_error_message));
                                                }
                                            }
                                        } else {
                                            if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_EMPTY) != null) {
                                                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_EMPTY).equals("")) {
                                                    editPhoneNo.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_EMPTY));
                                                } else {
                                                    editPhoneNo.setError(getString(R.string.null_phone_number_error_message));
                                                }
                                            } else {
                                                editPhoneNo.setError(getString(R.string.null_phone_number_error_message));
                                            }
                                        }
                                    } else {
                                        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY_CHECK_EMPTY) != null) {
                                            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY_CHECK_EMPTY).equals("")) {
                                                editCity.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY_CHECK_EMPTY));
                                            } else {
                                                editCity.setError(getString(R.string.null_city_error_message));
                                            }
                                        } else {
                                            editCity.setError(getString(R.string.null_city_error_message));
                                        }
                                    }
                                } else {
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSTATE_CHECK_EMPTY) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSTATE_CHECK_EMPTY).equals("")) {
                                            actState.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSTATE_CHECK_EMPTY));
                                        } else {
                                            actState.setError(getString(R.string.null_state_error_message));
                                        }
                                    } else {
                                        actState.setError(getString(R.string.null_state_error_message));
                                    }
                                }
                            } else {
                                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOUNTRY_CHECK_EMPTY) != null) {
                                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOUNTRY_CHECK_EMPTY).equals("")) {
                                        actCountry.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOUNTRY_CHECK_EMPTY));
                                    } else {
                                        actCountry.setError(getString(R.string.null_country_error_message));
                                    }
                                } else {
                                    actCountry.setError(getString(R.string.null_country_error_message));
                                }
                            }

                        } else {
                            if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE_CHECK_EMPTY) != null) {
                                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE_CHECK_EMPTY).equals("")) {
                                    editPincode.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE_CHECK_EMPTY));
                                } else {
                                    editPincode.setError(getString(R.string.null_pincode_error_message));
                                }
                            } else {
                                editPincode.setError(getString(R.string.null_pincode_error_message));
                            }
                        }
                    } else {
                        if(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK_CHECK_EMPTY)!=null){
                            if(!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK_CHECK_EMPTY).equals("")) {
                                editLandmark.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK_CHECK_EMPTY));
                            }else {
                                editLandmark.setError(getString(R.string.null_landmark_error_message));
                            }
                        }else {
                            editLandmark.setError(getString(R.string.null_landmark_error_message));
                        }
                    }
                } else {
                    if(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS_CHECK_EMPTY)!=null){
                        if(!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS_CHECK_EMPTY).equals("")) {
                            editAddress.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS_CHECK_EMPTY));
                        }else {
                            editAddress.setError(getString(R.string.null_address_error_message));
                        }
                    }else {
                        editAddress.setError(getString(R.string.null_address_error_message));
                    }
                }
            } else {
                if(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLASTNAME_CHECK_EMPTY)!=null){
                    if(!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLASTNAME_CHECK_EMPTY).equals("")) {
                        editLastName.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLASTNAME_CHECK_EMPTY));
                    }else {
                        editLastName.setError(getString(R.string.null_lname_error_message));
                    }
                }else {
                    editLastName.setError(getString(R.string.null_lname_error_message));
                }
            }
        } else {
            if(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIRSTNAME_CHECK_EMPTY)!=null){
                if(!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIRSTNAME_CHECK_EMPTY).equals("")) {
                    editFirstName.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIRSTNAME_CHECK_EMPTY));
                }else {
                    editFirstName.setError(getString(R.string.null_fname_error_message));
                }
            }else {
                editFirstName.setError(getString(R.string.null_fname_error_message));
            }
        }
        return valid;


    }
    //endregion

    // region ValidationBilling
    public Boolean ValidationBilling() {
        Boolean valid = false;
        if (!TextUtils.isEmpty(editBillingFirstName.getText().toString())) {
            editBillingFirstName.setError(null);
            if (!TextUtils.isEmpty(editBillingLastName.getText().toString())) {
                editBillingLastName.setError(null);
                if (!TextUtils.isEmpty(editBillingAddress.getText().toString())) {
                    editBillingAddress.setError(null);
                    if (!TextUtils.isEmpty(editBillingLandmark.getText().toString())) {
                        editBillingLandmark.setError(null);
                        if (!TextUtils.isEmpty(editBillingPincode.getText().toString())) {
                            editBillingPincode.setError(null);
                            if (!TextUtils.isEmpty(actBillingCountry.getText().toString())) {
                                actBillingCountry.setError(null);
                                if (!TextUtils.isEmpty(actBillingState.getText().toString())) {
                                    actBillingState.setError(null);
                                    if (!TextUtils.isEmpty(editBillingCity.getText().toString())) {
                                        editBillingCity.setError(null);
                                        if (!TextUtils.isEmpty(editBillingPhoneNo.getText().toString())) {
                                            editBillingPhoneNo.setError(null);
                                            if (editBillingPhoneNo.length() >= 10) {
                                                editBillingPhoneNo.setError(null);
                                                valid = true;

                                            } else {
                                                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_VALID) != null) {
                                                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_VALID).equals("")) {
                                                        editBillingPhoneNo.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_VALID));
                                                    } else {
                                                        editBillingPhoneNo.setError(getString(R.string.valid_phone_number_error_message));
                                                    }
                                                } else {
                                                    editBillingPhoneNo.setError(getString(R.string.valid_phone_number_error_message));
                                                }
                                            }
                                        } else {
                                            if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_EMPTY) != null) {
                                                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_EMPTY).equals("")) {
                                                    editBillingPhoneNo.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPHONE_CHECK_EMPTY));
                                                } else {
                                                    editBillingPhoneNo.setError(getString(R.string.null_phone_number_error_message));
                                                }
                                            } else {
                                                editBillingPhoneNo.setError(getString(R.string.null_phone_number_error_message));
                                            }
                                        }
                                    } else {
                                        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY_CHECK_EMPTY) != null) {
                                            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY_CHECK_EMPTY).equals("")) {
                                                editBillingCity.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCITY_CHECK_EMPTY));
                                            } else {
                                                editBillingCity.setError(getString(R.string.null_city_error_message));
                                            }
                                        } else {
                                            editBillingCity.setError(getString(R.string.null_city_error_message));
                                        }
                                    }
                                } else {
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSTATE_CHECK_EMPTY) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSTATE_CHECK_EMPTY).equals("")) {
                                            actBillingState.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSTATE_CHECK_EMPTY));
                                        } else {
                                            actBillingState.setError(getString(R.string.null_state_error_message));
                                        }
                                    } else {
                                        actBillingState.setError(getString(R.string.null_state_error_message));
                                    }
                                }
                            } else {
                                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOUNTRY_CHECK_EMPTY) != null) {
                                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOUNTRY_CHECK_EMPTY).equals("")) {
                                        actBillingCountry.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOUNTRY_CHECK_EMPTY));
                                    } else {
                                        actBillingCountry.setError(getString(R.string.null_country_error_message));
                                    }
                                } else {
                                    actBillingCountry.setError(getString(R.string.null_country_error_message));
                                }
                            }
                        } else {
                            if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE_CHECK_EMPTY) != null) {
                                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE_CHECK_EMPTY).equals("")) {
                                    editBillingPincode.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPINCODE_CHECK_EMPTY));
                                } else {
                                    editBillingPincode.setError(getString(R.string.null_pincode_error_message));
                                }
                            } else {
                                editBillingPincode.setError(getString(R.string.null_pincode_error_message));
                            }
                        }
                    } else {
                        if(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK_CHECK_EMPTY)!=null){
                            if(!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK_CHECK_EMPTY).equals("")) {
                                editBillingLandmark.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLANDMARK_CHECK_EMPTY));
                            }else {
                                editBillingLandmark.setError(getString(R.string.null_landmark_error_message));
                            }
                        }else {
                            editBillingLandmark.setError(getString(R.string.null_landmark_error_message));
                        }
                    }
                } else {
                    if(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS_CHECK_EMPTY)!=null){
                        if(!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS_CHECK_EMPTY).equals("")) {
                            editBillingAddress.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS_CHECK_EMPTY));
                        }else {
                            editBillingAddress.setError(getString(R.string.null_address_error_message));
                        }
                    }else {
                        editBillingAddress.setError(getString(R.string.null_address_error_message));
                    }
                }
            } else {
                if(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLASTNAME_CHECK_EMPTY)!=null){
                    if(!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLASTNAME_CHECK_EMPTY).equals("")) {
                        editBillingLastName.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLASTNAME_CHECK_EMPTY));
                    }else {
                        editBillingLastName.setError(getString(R.string.null_lname_error_message));
                    }
                }else {
                    editBillingLastName.setError(getString(R.string.null_lname_error_message));
                }
            }
        } else {
            if(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIRSTNAME_CHECK_EMPTY)!=null){
                if(!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIRSTNAME_CHECK_EMPTY).equals("")) {
                    editBillingFirstName.setError(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFIRSTNAME_CHECK_EMPTY));
                }else {
                    editBillingFirstName.setError(getString(R.string.null_fname_error_message));
                }
            }else {
                editBillingFirstName.setError(getString(R.string.null_fname_error_message));
            }
        }
        return valid;


    }
    //endregion

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        button.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    // region chanage FrameLayout Border
    public void chanageFramelayoutBorder(FrameLayout frameLayout, AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        frameLayout.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) frameLayout.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setCornerRadius(60);
        gd.setColor(Color.parseColor("#FFFFFF"));
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = row.findViewById(R.id.tvTitle);
        final Button btnSettings = row.findViewById(R.id.btnSettings);
        final Button btnExit = row.findViewById(R.id.btnExit);

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF).equals("")) {
                tvTitle.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF));
            } else {
                tvTitle.setText(getText(R.string.your_data_is_off));
            }
        } else {
            tvTitle.setText(getText(R.string.your_data_is_off));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI).equals("")) {
                tvAlertText.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI));
            } else {
                tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
            }
        } else {
            tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS).equals("")) {
                btnSettings.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS));
            } else {
                btnSettings.setText(getText(R.string.settings));
            }
        } else {
            btnSettings.setText(getText(R.string.settings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT).equals("")) {
                btnExit.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT));
            } else {
                btnExit.setText(getText(R.string.exit));
            }
        } else {
            btnExit.setText(getText(R.string.exit));
        }

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion
}
