package com.pureweblopment.pioneerchemicalshop.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.pioneerchemicalshop.Global.Global;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import com.pureweblopment.pioneerchemicalshop.Global.SendMail;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;

import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.pureweblopment.pioneerchemicalshop.R;

import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class SplashActivity extends AppCompatActivity {

    Context context = SplashActivity.this;
    CoordinatorLayout coodinator;
    private TextView mTxtPoweredByBazarBit;
    private String is_multicurrency_active = "";

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_splash);

        checkAndRequestPermissions();

        coodinator = (CoordinatorLayout) findViewById(R.id.coodinator);
        mTxtPoweredByBazarBit = (TextView) findViewById(R.id.txtPoweredByBazarBit);
        mTxtPoweredByBazarBit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        if (SharedPreference.GetPreference(this, Global.preferenceNameGuestUSer,
                Global.SessionId) == null) {
            SharedPreference.CreatePreference(this, Global.preferenceNameGuestUSer);
            SharedPreference.SavePreference(Global.SessionId, Global.SessionId());
        }
        Global.DeviceInfo(context);

        if (SharedPreference.GetPreference(context, Global.PREFERENCECURRENCYSCREEN, StaticUtility.sCURRENCYSCREENVISIBILITY) == null) {
            SharedPreference.CreatePreference(context, Global.PREFERENCECURRENCYSCREEN);
            SharedPreference.SavePreference(StaticUtility.sCURRENCYSCREENVISIBILITY, "no");
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPOWERED_BY_BAZRBIT) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPOWERED_BY_BAZRBIT).equalsIgnoreCase("")) {
                mTxtPoweredByBazarBit.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPOWERED_BY_BAZRBIT));
            } else {
                mTxtPoweredByBazarBit.setText(R.string.powered_by_bazarbit);
            }
        } else {
            mTxtPoweredByBazarBit.setText(R.string.powered_by_bazarbit);
        }

        if (Global.isNetworkAvailable(context)) {
            ApplicationSettings();
        } else {
            Global.Toast(context, "Kindly check your internet connection...!");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    System.exit(0);
                }
            }, 5000);
        }

       /* //region If the device is having android oreo we will create a notification channel
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            String channelId = "fcm_default_channel";
            CharSequence channelName = "News";
            int importance = NotificationManager.IMPORTANCE_LOW;
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel =
                    new NotificationChannel(channelId, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);
        }//endregion*/
    }

    //region CheckAndRequestPermissions
    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        int receiveSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);

        int readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_SMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }//endregion

    //region FOR APP setting API...
    private void AppSetting() {
        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(
                StaticUtility.URL + StaticUtility.AppSetting + Global.queryStringUrl(context));
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                FeatureSetting();
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");

                                if (strStatus.equals("ok")) {
                                    String is_live = "", force_update_title = "", force_update_msg = "",
                                            app_type = "", app_version = "";
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    if (jsonObject.has("app_type")) {
                                        app_type = jsonObject.getString("app_type");
                                    }
                                    if (jsonObject.has("app_version")) {
                                        app_version = jsonObject.getString("app_version");
                                    }
                                    if (jsonObject.has("app_setting")) {
                                        JSONObject jsonObjectAppSetting = jsonObject.getJSONObject("app_setting");
                                        if (jsonObjectAppSetting.has("is_live")) {
                                            is_live = jsonObjectAppSetting.getString("is_live");
                                        }
                                        if (jsonObjectAppSetting.has("force_update_title")) {
                                            force_update_title = jsonObjectAppSetting.getString("force_update_title");
                                        }
                                        if (jsonObjectAppSetting.has("force_update_msg"))
                                            force_update_msg = jsonObjectAppSetting.getString("force_update_msg");
                                    }

                                    if (is_live.equalsIgnoreCase("1")) {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (is_multicurrency_active.equalsIgnoreCase("1")) {
                                                    if (SharedPreference.GetPreference(context, Global.PREFERENCECURRENCYSCREEN, StaticUtility.sCURRENCYSCREENVISIBILITY).equalsIgnoreCase("no")) {
                                                        Intent i = new Intent(SplashActivity.this, CurrencyScreen.class);
                                                        i.putExtra("redirect", "splashscreen");
                                                        startActivity(i);
                                                        finish();
                                                    } else {
                                                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
                                                        startActivity(i);
                                                        finish();
                                                    }
                                                } else {
                                                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                                                    startActivity(i);
                                                    finish();
                                                }
                                                /*Intent i = new Intent(SplashActivity.this, CurrencyScreen.class);
                                                startActivity(i);
                                                finish();*/
                                            }
                                        }, 3000);

                                    } else {
                                        new AlertDialog.Builder(context, R.style.MyDialogTheme)
                                                .setTitle(force_update_msg)
                                                .setMessage(force_update_title)
                                                .setCancelable(false)
                                                .setPositiveButton(getString(R.string.update), new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // continue with delete
                                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                                        try {
//                                                            Toast.makeText(getApplicationContext(), "App is in BETA version cannot update", Toast.LENGTH_SHORT).show();
                                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                        } catch (ActivityNotFoundException anfe) {
                                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                        }
                                                    }
                                                })
                                                .setIcon(android.R.drawable.ic_dialog_alert)
                                                .show();
                                    }

                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                FeatureSetting();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strMessage.equalsIgnoreCase("app-id and app-secret is required")) {
//                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    Global.showSnackBar(coodinator, "Application is not active !");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            System.exit(0);
                                        }
                                    }, 5000);

                                }
//                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in LoginActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR Application setting API...
    private void ApplicationSettings() {
        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(
                StaticUtility.URL + StaticUtility.ApplicationSettings);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                AppSetting();
                getMultiLanguage();
                PaymentGateway();
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    String strLogo = "", strGoogle = "", strFacebook = "",
                                            strInstagram = "", strLinkedinimage = "", strPinterestimage = "",
                                            strapplicationLogo = "", is_live = "", force_update_title = "", force_update_msg = "",
                                            CurrencyName = "", CurrencySign = "",
                                            CurrencyPosition = "", CurrencyCode = "", CurrencyCountry = "", CurrencyThousandSign = "",
                                            CurrencyDecimalSign = "", CurrencyDecimalPlace = "",
                                            app_type = "", app_version = "", strIsMegaMenuActivited = "";
//                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    if (jsonObject.has("logoimage")) {
                                        JSONArray Logo = jsonObject.getJSONArray("logoimage");
                                        strLogo = Logo.getString(0);
                                    }
                                    if (jsonObject.has("googleimage")) {
                                        JSONArray googleimage = jsonObject.getJSONArray("googleimage");
                                        strGoogle = googleimage.getString(0);
                                    }
                                    if (jsonObject.has("facebookimage")) {
                                        JSONArray facebookimage = jsonObject.getJSONArray("facebookimage");
                                        strFacebook = facebookimage.getString(0);
                                    }
                                    if (jsonObject.has("instagramimage")) {
                                        JSONArray instagramimage = jsonObject.getJSONArray("instagramimage");
                                        strInstagram = instagramimage.getString(0);
                                    }
                                    if (jsonObject.has("linkedinimage")) {
                                        JSONArray linkedinimage = jsonObject.getJSONArray("linkedinimage");
                                        strLinkedinimage = linkedinimage.getString(0);
                                    }
                                    if (jsonObject.has("pinterestimage")) {
                                        JSONArray pinterestimage = jsonObject.getJSONArray("pinterestimage");
                                        strPinterestimage = pinterestimage.getString(0);
                                    }
                                    if (jsonObject.has("applicationimage")) {
                                        JSONArray applicationimage = jsonObject.getJSONArray("applicationimage");
                                        strapplicationLogo = applicationimage.getString(0);
                                    }
                                    String strthemeprimarycolor = jsonObject.getString("themeprimarycolor");
                                    if (!strthemeprimarycolor.equals("")) {
                                        strthemeprimarycolor = "#" + strthemeprimarycolor;
                                    }
                                    String strthemedarkcolor = jsonObject.getString("themeprimarydarkcolor");
                                    if (!strthemedarkcolor.equals("")) {
                                        strthemedarkcolor = "#" + strthemedarkcolor;
                                    }
                                    String strtextcolor = jsonObject.getString("textprimarycolor");
                                    if (!strtextcolor.equals("")) {
                                        strtextcolor = "#" + strtextcolor;
                                        /*mTxtPoweredByBazarBit.setTextColor(Color.parseColor(strtextcolor));*/
                                    }
                                    String strtextlightcolor = jsonObject.getString("textlightcolor");
                                    if (!strtextlightcolor.equals("")) {
                                        strtextlightcolor = "#" + strtextlightcolor;
                                    }
                                    String strbtntextcolor = jsonObject.getString("buttontextcolor");
                                    if (!strbtntextcolor.equals("")) {
                                        strbtntextcolor = "#" + strbtntextcolor;
                                    }
                                    String is_slider_active = jsonObject.getString("is_slider_active");
                                    String is_cart_active = jsonObject.getString("is_cart_active");
                                    String is_order_active = jsonObject.getString("is_order_active");
                                    String is_wishlist_active = jsonObject.getString("is_wishlist_active");
                                    String is_arrival_active = jsonObject.getString("is_arrival_active");
                                    String is_seller_active = jsonObject.getString("is_seller_active");
                                    String is_mostwanted_active = jsonObject.getString("is_mostwanted_active");
                                    is_multicurrency_active = jsonObject.getString("is_multicurrency_active");
                                    String strAllowOTPVerification = jsonObject.getString("allow_otp_verification");
                                    String strRefCodeEnabled = jsonObject.getString("ref_code_enabled");
                                    String strPreBookingEnabled = jsonObject.getString("pre_booking_enabled");
                                    String strCheckPincodeAvailibility = jsonObject.getString("check_pincode_availibility");

                                  /*  if (jsonObject.has("currencydata")) {
                                        JSONObject jsonObjectCurrency = jsonObject.getJSONObject("currencydata");
                                        CurrencyName = jsonObjectCurrency.getString("name");
                                        CurrencySign = jsonObjectCurrency.getString("sign");
                                        CurrencyPosition = jsonObjectCurrency.getString("sign_position");
                                        CurrencyCode = jsonObjectCurrency.getString("code");
                                        CurrencyCountry = jsonObjectCurrency.getString("country");
                                        CurrencyThousandSign = jsonObjectCurrency.getString("thousand_sign");
                                        CurrencyDecimalSign = jsonObjectCurrency.getString("decimal_sign");
                                        CurrencyDecimalPlace = jsonObjectCurrency.getString("decimal_place");
                                    }*/

                                    CurrencyCode = jsonObject.optString("currencycode");
                                    CurrencySign = jsonObject.optString("currency_symbol");
                                    CurrencyPosition = jsonObject.optString("currency_place");
                                    CurrencyName = jsonObject.optString("currency");

                                    if (jsonObject.has("is_megamenu_active")) {
                                        strIsMegaMenuActivited = jsonObject.getString("is_megamenu_active");
                                    }

                                    SharedPreference.CreatePreference(context, Global.APPSetting_PREFERENCE);
                                    SharedPreference.SavePreference(StaticUtility.IMG_LOGO, strLogo);
                                    SharedPreference.SavePreference(StaticUtility.IMG_Facebook, strFacebook);
                                    SharedPreference.SavePreference(StaticUtility.IMG_google, strGoogle);
                                    SharedPreference.SavePreference(StaticUtility.IMG_insta, strInstagram);
                                    SharedPreference.SavePreference(StaticUtility.IMG_Linkedin, strLinkedinimage);
                                    SharedPreference.SavePreference(StaticUtility.IMG_pinterest, strPinterestimage);
                                    SharedPreference.SavePreference(StaticUtility.ThemePrimaryColor, strthemeprimarycolor);
                                    SharedPreference.SavePreference(StaticUtility.ThemePrimaryDarkColor, strthemedarkcolor);
                                    SharedPreference.SavePreference(StaticUtility.TextColor, strtextcolor);
                                    SharedPreference.SavePreference(StaticUtility.TextLightColor, strtextlightcolor);
                                    SharedPreference.SavePreference(StaticUtility.ButtonTextColor, strbtntextcolor);
                                    SharedPreference.SavePreference(StaticUtility.APP_LOGO, strapplicationLogo);
                                    SharedPreference.SavePreference(StaticUtility.Is_Slider_Active, is_slider_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Cart_Active, is_cart_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Order_Active, is_order_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Wishlist_Active, is_wishlist_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Arrival_Active, is_arrival_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Seller_Active, is_seller_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_Mostwanted_Active, is_mostwanted_active);
                                    SharedPreference.SavePreference(StaticUtility.Is_MEGAMENU_Active, strIsMegaMenuActivited);
                                    SharedPreference.SavePreference(StaticUtility.IS_MULTI_CURRENCY_ACTIVE, is_multicurrency_active);
                                    SharedPreference.SavePreference(StaticUtility.REF_CODE_ENABLED, strRefCodeEnabled);
                                    SharedPreference.SavePreference(StaticUtility.PRE_BOOKING_ENABLED, strPreBookingEnabled);
                                    SharedPreference.SavePreference(StaticUtility.CHECK_PINCODE_AVAILABILITY, strCheckPincodeAvailibility);
                                    SharedPreference.SavePreference(StaticUtility.ALLOW_OTP_VERIFICATION, strAllowOTPVerification);

                                        SharedPreference.CreatePreference(context, Global.PREFERENCECURRENCY);
                                        SharedPreference.SavePreference(StaticUtility.sCurrencyName, CurrencyName);
                                        SharedPreference.SavePreference(StaticUtility.sCurrencySign, CurrencySign);
                                        SharedPreference.SavePreference(StaticUtility.sCurrencySignPosition, CurrencyPosition);
                                        SharedPreference.SavePreference(StaticUtility.sCurrencyCode, CurrencyCode);

                                    /*if (jsonObject.has("is_live")) {
                                        is_live = jsonObject.getString("is_live");
                                    }

                                    if (jsonObject.has("is_force_update")) {
                                        JSONObject jsonObjectForceUpdate = jsonObject.getJSONObject("is_force_update");
                                        force_update_title = jsonObjectForceUpdate.getString("force_update_title");
                                        force_update_msg = jsonObjectForceUpdate.getString("force_update_msg");
                                    }
                                    if (jsonObject.has("app_type")) {
                                        app_type = jsonObject.getString("app_type");
                                    }
                                    if (jsonObject.has("app_version")) {
                                        app_version = jsonObject.getString("app_version");
                                    }
                                    if (is_live.equalsIgnoreCase("1")) {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                                                startActivity(i);
                                                finish();
                                            }
                                        }, 3000);
                                    } else {
                                        new AlertDialog.Builder(context, R.style.MyDialogTheme)
                                                .setTitle(force_update_msg)
                                                .setMessage(force_update_title)
                                                .setCancelable(false)
                                                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // continue with delete
                                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                                        try {
//                                                            Toast.makeText(getApplicationContext(), "App is in BETA version cannot update", Toast.LENGTH_SHORT).show();
                                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                        } catch (ActivityNotFoundException anfe) {
                                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                        }
                                                    }
                                                })
                                                .setIcon(android.R.drawable.ic_dialog_alert)
                                                .show();
                                    }*/

                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                AppSetting();
                getMultiLanguage();
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strMessage.equalsIgnoreCase("app-id and app-secret is required")) {
//                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    Global.showSnackBar(coodinator, "Application is not active !");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            System.exit(0);
                                        }
                                    }, 5000);

                                }
//                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in LoginActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    // region FOR Feature setting API...
    private void FeatureSetting() {
        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder =
                AndroidNetworking.post(StaticUtility.URL + StaticUtility.FeatureSetting);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            String strStatus = response.optString("status");
                            String strMessage = response.optString("message");
                            if (strStatus.equals("ok")) {
                                if (response.has("payload")) {
                                    JSONArray jsonArray = response.optJSONArray("payload");
                                    SharedPreference.CreatePreference(context, Global.APPSetting_PREFERENCE);
                                    SharedPreference.SavePreference(StaticUtility.Feature_List, jsonArray.toString());
                                    /*for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.optJSONObject(i);
                                        String slug = jsonObject.optString("slug");
                                        if (slug.equalsIgnoreCase("social-media-logins")) {
                                            String strSoicalMediaLogin = jsonObject.optString("feature_textval");
                                            SharedPreference.CreatePreference(context, Global.APPSetting_PREFERENCE);
                                            SharedPreference.SavePreference(StaticUtility.Feature_Social_Media, strSoicalMediaLogin);
                                        }
                                    }*/
                                }
//                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strMessage.equalsIgnoreCase("app-id and app-secret is required")) {
//                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    Global.showSnackBar(coodinator, "Application is not active !");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            System.exit(0);
                                        }
                                    }, 5000);

                                }
//                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in LoginActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    // region FOR get Multi Language API...
    private void getMultiLanguage() {
        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder =
                AndroidNetworking.post(StaticUtility.URL +
                        StaticUtility.GetMultiLanguageDate);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            String strStatus = response.optString("status");
                            String strMessage = response.optString("message");
                            if (strStatus.equals("ok")) {
                                JSONObject joPayload = response.optJSONObject("payload");
                                if(joPayload != null && !joPayload.equals("[]")){
                                    String success_order = joPayload.optString("success_order");
                                    String thank_you_order = joPayload.optString("thank_you_order");
                                    String thank_you_response = joPayload.optString("thank_you_response");
                                    String continue_shopping = joPayload.optString("continue_shopping");
                                    String expected_delivery = joPayload.optString("expected_delivery");
                                    String days_return_available = joPayload.optString("days_return_available");
                                    String read_more = joPayload.optString("read_more");
                                    String notify_me = joPayload.optString("notify_me");
                                    String select = joPayload.optString("select");
                                    String out_of_stock = joPayload.optString("out_of_stock");
                                    String go_to_cart = joPayload.optString("go_to_cart");
                                    String check_delivery_option = joPayload.optString("check_delivery_option");
                                    String check = joPayload.optString("check");
                                    String product_details = joPayload.optString("product_details");
                                    String related_product = joPayload.optString("related_product");
                                    String enter_pincode = joPayload.optString("enter_pincode");
                                    String popular_reviews = joPayload.optString("popular_reviews");
                                    String add_to_wishlist = joPayload.optString("add_to_wishlist");
                                    String add_to_cart = joPayload.optString("add_to_cart");
                                    String check_delivery_option_available_or_not = joPayload.optString("check_delivery_option_available_or_not");
                                    String enter_another_pincode = joPayload.optString("enter_another_pincode");
                                    String delivery_available = joPayload.optString("delivery_available");
                                    String delivery_not_available = joPayload.optString("delivery_not_available");
                                    String pincode_required = joPayload.optString("pincode_required");
                                    String filter = joPayload.optString("filter");
                                    String apply = joPayload.optString("apply");
                                    String clear = joPayload.optString("clear");
                                    String no_record_found = joPayload.optString("no_record_found");
                                    String are_you_sure_you_want_to_exit = joPayload.optString("are_you_sure_you want_to_exit");
                                    String are_you_sure_you_want_to_clear = joPayload.optString("are_you_sure_you want_to_clear");
                                    String wishlist_empty = joPayload.optString("wishlist_empty");
                                    String view_wishlist_detail = joPayload.optString("view_wishlist_detail");
                                    String remove_from_wishlist = joPayload.optString("remove_from_wishlist");
                                    String data_off = joPayload.optString("data_off");
                                    String turn_on_data_wifi = joPayload.optString("turn_on_data_wifi");
                                    String settings = joPayload.optString("settings");
                                    String exit = joPayload.optString("exit");
                                    String edit = joPayload.optString("edit");
                                    String Delete = joPayload.optString("Delete");
                                    String update_linkedin = joPayload.optString("update_linkedin");
                                    String connect_linkedin = joPayload.optString("connect_linkedin");
                                    String download = joPayload.optString("download");
                                    String cancel = joPayload.optString("cancel");
                                    String processing = joPayload.optString("processing");
                                    String please_wait = joPayload.optString("please_wait");

                                    String my_address = joPayload.optString("my_address");
                                    String first_name = joPayload.optString("first_name");
                                    String last_name = joPayload.optString("last_name");
                                    String address = joPayload.optString("address");
                                    String landmark = joPayload.optString("landmark");
                                    String pincode = joPayload.optString("pincode");
                                    String select_country = joPayload.optString("select_country");
                                    String select_state = joPayload.optString("select_state");
                                    String city = joPayload.optString("city");
                                    String code = joPayload.optString("code");
                                    String phone_number = joPayload.optString("phone_number");
                                    String is_shipping = joPayload.optString("is_shipping");
                                    String add_address = joPayload.optString("add_address");
                                    String old_password = joPayload.optString("old_password");
                                    String new_password = joPayload.optString("new_password");
                                    String confirm_password = joPayload.optString("confirm_password");
                                    String change_password = joPayload.optString("change_password");
                                    String payment = joPayload.optString("payment");
                                    String success = joPayload.optString("success");
                                    String no_address_available = joPayload.optString("no_address_available");
                                    String select_currency = joPayload.optString("select_currency");
                                    String currency = joPayload.optString("currency");
                                    String done = joPayload.optString("done");
                                    String skip = joPayload.optString("skip");
                                    String update = joPayload.optString("update");
                                    String forgot_password = joPayload.optString("forgot_password");
                                    String log_in_now = joPayload.optString("log_in_now");
                                    String email_address = joPayload.optString("email_address");
                                    String password = joPayload.optString("password");
                                    String forgot_your_password = joPayload.optString("forgot_your_password");
                                    String log_in = joPayload.optString("log_in");
                                    String not_have_an_account = joPayload.optString("not_have_an_account");
                                    String sign_up_now = joPayload.optString("sign_up_now");
                                    String logout = joPayload.optString("logout");
                                    String search_best_product = joPayload.optString("search_best_product");
                                    String our_best_seller = joPayload.optString("our_best_seller");
                                    String most_wanted = joPayload.optString("most_wanted");
                                    String new_arrivals = joPayload.optString("new_arrivals");
                                    String home = joPayload.optString("home");
                                    String category = joPayload.optString("category");
                                    String notification = joPayload.optString("notification");
                                    String wishlist = joPayload.optString("wishlist");
                                    String profile = joPayload.optString("profile");
                                    String guest_email_address = joPayload.optString("guest_email_address");
                                    String guest_user = joPayload.optString("guest_user");
                                    String no_order_available = joPayload.optString("no_order_available");
                                    String from_date = joPayload.optString("from_date");
                                    String to_date = joPayload.optString("to_date");
                                    String delivered = joPayload.optString("delivered");
                                    String cancelled_order = joPayload.optString("cancelled_order");
                                    String pending_order = joPayload.optString("pending_order");
                                    String confirm_order = joPayload.optString("confirm_order");
                                    String in_process = joPayload.optString("in_process");
                                    String out_for_delivery = joPayload.optString("out_for_delivery");
                                    String return_order = joPayload.optString("return_order");
                                    String refund_order = joPayload.optString("refund_order");
                                    String returned_order = joPayload.optString("returned_order");
                                    String order_id = joPayload.optString("order_id");
                                    String please_type_verification_code_and_send_to = joPayload.optString("please_type_verification_code_and_send_to");
                                    String verification_code = joPayload.optString("verification_code");
                                    String not_receive_the_otp = joPayload.optString("not_receive_the_otp");
                                    String strcontinue = joPayload.optString("continue");
                                    String resend_again = joPayload.optString("resend_again");
                                    String enter_your_otp = joPayload.optString("enter_your_otp");
                                    String verify = joPayload.optString("verify");
                                    String gender = joPayload.optString("gender");
                                    String male = joPayload.optString("male");
                                    String female = joPayload.optString("female");
                                    String date_of_birth = joPayload.optString("date_of_birth");
                                    String submit = joPayload.optString("submit");
                                    String mobile_no = joPayload.optString("mobile_no");
                                    String sign_up = joPayload.optString("sign_up");
                                    String no_review_available = joPayload.optString("no_review_available");
                                    String powered_by_bazarBit = joPayload.optString("powered_by_bazarBit");
                                    String shopping_cart = joPayload.optString("shopping_cart");
                                    String shopping_bag_is_empty = joPayload.optString("shopping_bag_is_empty");
                                    String start_shopping_now = joPayload.optString("start_shopping_now");
                                    String checkout = joPayload.optString("checkout");
                                    String remove_this_product_from_cart = joPayload.optString("remove_this_product_from_cart");
                                    String add = joPayload.optString("add");
                                    String change = joPayload.optString("change");
                                    String you_have_any_promocode = joPayload.optString("you_have_any_promocode");
                                    String promo_code = joPayload.optString("promo_code");
                                    String cash_on_delivery = joPayload.optString("cash_on_delivery");
                                    String pay_u_money = joPayload.optString("pay_u_money");
                                    String ccavenue = joPayload.optString("ccavenue");
                                    String paypal = joPayload.optString("paypal");
                                    String sub_total = joPayload.optString("sub_total");
                                    String Discount = joPayload.optString("Discount");
                                    String shipping_charges = joPayload.optString("shipping_charges");
                                    String cod_charges = joPayload.optString("cod_charges");
                                    String total = joPayload.optString("total");
                                    String next_payment_info = joPayload.optString("next_payment_info");
                                    String quantity = joPayload.optString("quantity");
                                    String confirm_purchase = joPayload.optString("confirm_purchase");
                                    String send_otp = joPayload.optString("send_otp");
                                    String resend = joPayload.optString("resend");
                                    String qty = joPayload.optString("qty");
                                    String choose_from_promocode_blow = joPayload.optString("choose_from_promocode_blow");
                                    String place_order = joPayload.optString("place_order");
                                    String shipping_address = joPayload.optString("shipping_address");
                                    String billing_address = joPayload.optString("billing_address");
                                    String same_as_shipping_address = joPayload.optString("same_as_shipping_address");
                                    String enter_promo_code = joPayload.optString("enter_promo_code");
                                    String apply_promocode = joPayload.optString("apply_promocode");
                                    String cancel_order = joPayload.optString("cancel_order");
                                    String contact_us = joPayload.optString("contact_us");
                                    String name = joPayload.optString("name");
                                    String subject = joPayload.optString("subject");
                                    String message = joPayload.optString("message");
                                    String feedback = joPayload.optString("feedback");
                                    String ans = joPayload.optString("ans");
                                    String q = joPayload.optString("q");
                                    String my_account = joPayload.optString("my_account");
                                    String order_history = joPayload.optString("order_history");
                                    String review = joPayload.optString("review");
                                    String order_detail = joPayload.optString("order_detail");
                                    String order_date = joPayload.optString("order_date");
                                    String order_total = joPayload.optString("order_total");
                                    String payment_information = joPayload.optString("payment_information");
                                    String item_total = joPayload.optString("item_total");
                                    String sure_cancel_this_order = joPayload.optString("sure_cancel_this_order");
                                    String add_your_review = joPayload.optString("add_your_review");
                                    String would_you_like_to_recommend_this_product = joPayload.optString("would_you_like_to_recommend_this_product");
                                    String user_name = joPayload.optString("user_name");
                                    String title = joPayload.optString("title");
                                    String description = joPayload.optString("description");
                                    String return_product = joPayload.optString("return_product");
                                    String return_detail = joPayload.optString("return_detail");
                                    String account_holder_name = joPayload.optString("account_holder_name");
                                    String bank_name = joPayload.optString("bank_name");
                                    String branch_name = joPayload.optString("branch_name");
                                    String account_number = joPayload.optString("account_number");
                                    String ifsc_code = joPayload.optString("ifsc_code");
                                    String reason = joPayload.optString("reason");
                                    String yes = joPayload.optString("yes");
                                    String no = joPayload.optString("no");
                                    String items = joPayload.optString("items");

                                    String referral_code = joPayload.optString("referral_code");
                                    String pre_booking = joPayload.optString("pre_booking");
                                    String commission = joPayload.optString("commission");
                                    String commission_percentage = joPayload.optString("commission_percentage");
                                    String commission_amount = joPayload.optString("commission_amount");
                                    String login_required = joPayload.optString("login_required");
                                    String read_less = joPayload.optString("read_less");

                                    String pending = joPayload.optString("pending");
                                    String rejected = joPayload.optString("rejected");
                                    String days = joPayload.optString("days");
                                    String check_pincode_availibility = joPayload.optString("check_pincode_availibility");
                                    String check_pincode_error = joPayload.optString("check_pincode_error");
                                    String select_share_type = joPayload.optString("select_share_type");
                                    String select_image = joPayload.optString("select_image");
                                    String take_photo = joPayload.optString("take_photo");
                                    String gallery = joPayload.optString("gallery");
                                    String facebook = joPayload.optString("facebook");
                                    String twitter = joPayload.optString("twitter");
                                    String google = joPayload.optString("google");
                                    String skype = joPayload.optString("skype");
                                    String linkdin = joPayload.optString("linkdin");
                                    String whatsapp = joPayload.optString("whatsapp");
                                    String printerest = joPayload.optString("printerest");
                                    String password_limit_valid = joPayload.optString("password_limit_valid");
                                    String password_check_empty = joPayload.optString("password_check_empty");
                                    String confirm_password_limit_valid = joPayload.optString("confirm_password_limit_valid");
                                    String confirm_password_check_empty = joPayload.optString("confirm_password_check_empty");
                                    String confirm_password_check_match = joPayload.optString("confirm_password_check_match");
                                    String old_password_check_empty = joPayload.optString("old_password_check_empty");
                                    String new_password_check_empty = joPayload.optString("new_password_check_empty");
                                    String email_check_valid = joPayload.optString("email_check_valid");
                                    String email_check_empty = joPayload.optString("email_check_empty");
                                    String firstname_check_empty = joPayload.optString("firstname_check_empty");
                                    String lastname_check_empty = joPayload.optString("lastname_check_empty");
                                    String address_check_empty = joPayload.optString("address_check_empty");
                                    String landmark_check_empty = joPayload.optString("landmark_check_empty");
                                    String pincode_check_empty = joPayload.optString("pincode_check_empty");
                                    String city_check_empty = joPayload.optString("city_check_empty");
                                    String phone_check_empty = joPayload.optString("phone_check_empty");
                                    String phone_check_valid = joPayload.optString("phone_check_valid");
                                    String country_check_empty = joPayload.optString("country_check_empty");
                                    String state_check_empty = joPayload.optString("state_check_empty");
                                    String dob_check_empty = joPayload.optString("dob_check_empty");
                                    String ans_check_empty = joPayload.optString("ans_check_empty");
                                    String rating_check_empty = joPayload.optString("rating_check_empty");
                                    String desc_check_empty = joPayload.optString("desc_check_empty");
                                    String title_check_empty = joPayload.optString("title_check_empty");
                                    String unm_check_empty = joPayload.optString("unm_check_empty");
                                    String reason_check_empty = joPayload.optString("reason_check_empty");
                                    String ifsc_check_empty = joPayload.optString("ifsc_check_empty");
                                    String accno_check_empty = joPayload.optString("accno_check_empty");
                                    String bankname_check_empty = joPayload.optString("bankname_check_empty");
                                    String branch_check_empty = joPayload.optString("branch_check_empty");
                                    String acc_hold_name_check_empty = joPayload.optString("acc_hold_name_check_empty");
                                    String subject_check_empty = joPayload.optString("subject_check_empty");
                                    String msg_check_empty = joPayload.optString("msg_check_empty");
                                    String pwd_newpwd_check_match = joPayload.optString("pwd_newpwd_check_match");
                                    String no_more_commission_found = joPayload.optString("no_more_commission_found");
                                    String please_enter_otp = joPayload.optString("please_enter_otp");
                                    String please_select_payment_gateway = joPayload.optString("please_select_payment_gateway");
                                    String bingage_wallet_error = joPayload.optString("please_accept_redeem_from_wallet");
                                    String redeem_from_wallet = joPayload.optString("redeem_from_wallet");
                                    String wallet_balance = joPayload.optString("redeem_balance");


                                    SharedPreference.CreatePreference(context, StaticUtility.MULTILANGUAGEPREFERENCE);
                                    SharedPreference.SavePreference(StaticUtility.sSUCCESS_ORDER, success_order);
                                    SharedPreference.SavePreference(StaticUtility.sTHANK_YOU_ORDER, thank_you_order);
                                    SharedPreference.SavePreference(StaticUtility.sTHANK_YOU_RESPONSE, thank_you_response);
                                    SharedPreference.SavePreference(StaticUtility.sCONTINUE_SHOPPING, continue_shopping);
                                    SharedPreference.SavePreference(StaticUtility.sEXPEXCTED_DELIVERY, expected_delivery);
                                    SharedPreference.SavePreference(StaticUtility.sDAYS_RETURN_AVAILABLE, days_return_available);
                                    SharedPreference.SavePreference(StaticUtility.sREAD_MORE, read_more);
                                    SharedPreference.SavePreference(StaticUtility.sNOTIFY_ME, notify_me);
                                    SharedPreference.SavePreference(StaticUtility.sSELECT, select);
                                    SharedPreference.SavePreference(StaticUtility.sOUT_OF_STOCK, out_of_stock);
                                    SharedPreference.SavePreference(StaticUtility.sGO_TO_CART, go_to_cart);
                                    SharedPreference.SavePreference(StaticUtility.sCHECK_DELIVERY_OPTION, check_delivery_option);
                                    SharedPreference.SavePreference(StaticUtility.sCHECK, check);
                                    SharedPreference.SavePreference(StaticUtility.sPRODUCT_DETAILS, product_details);
                                    SharedPreference.SavePreference(StaticUtility.sRELATED_PRODUCT, related_product);
                                    SharedPreference.SavePreference(StaticUtility.sENTER_PINCODE, enter_pincode);
                                    SharedPreference.SavePreference(StaticUtility.sPOPULAR_REVIEWS, popular_reviews);
                                    SharedPreference.SavePreference(StaticUtility.sADD_TO_WISHLIST, add_to_wishlist);
                                    SharedPreference.SavePreference(StaticUtility.sADD_TO_CART, add_to_cart);
                                    SharedPreference.SavePreference(StaticUtility.sCHECK_DELIVERY_OPTION_AVAILABLE_OR_NOT, check_delivery_option_available_or_not);
                                    SharedPreference.SavePreference(StaticUtility.sDELIVERY_AVAILABLE, delivery_available);
                                    SharedPreference.SavePreference(StaticUtility.sDELIVERY_NOT_AVAILABLE, delivery_not_available);
                                    SharedPreference.SavePreference(StaticUtility.sPINCODE_REQUIRED, pincode_required);
                                    SharedPreference.SavePreference(StaticUtility.sFILTER, filter);
                                    SharedPreference.SavePreference(StaticUtility.sAPPLY, apply);
                                    SharedPreference.SavePreference(StaticUtility.sCLEAR, clear);
                                    SharedPreference.SavePreference(StaticUtility.sNO_RECORD_FOUND, no_record_found);
                                    SharedPreference.SavePreference(StaticUtility.sARE_YOU_SURE_YOU_WANT_TO_EXIT, are_you_sure_you_want_to_exit);
                                    SharedPreference.SavePreference(StaticUtility.sARE_YOU_SURE_YOU_WANT_TO_CLEAR, are_you_sure_you_want_to_clear);
                                    SharedPreference.SavePreference(StaticUtility.sWISHLIST_EMPTY, wishlist_empty);
                                    SharedPreference.SavePreference(StaticUtility.sVIEW_WISHLIST_DETAIL, view_wishlist_detail);
                                    SharedPreference.SavePreference(StaticUtility.sREMOVE_FROM_WISHLIST, remove_from_wishlist);
                                    SharedPreference.SavePreference(StaticUtility.sDATE_OFF, data_off);
                                    SharedPreference.SavePreference(StaticUtility.sTURN_ON_DATA_WIFI, turn_on_data_wifi);
                                    SharedPreference.SavePreference(StaticUtility.sSETTINGS, settings);
                                    SharedPreference.SavePreference(StaticUtility.sEXIT, exit);
                                    SharedPreference.SavePreference(StaticUtility.sEDIT, edit);
                                    SharedPreference.SavePreference(StaticUtility.sDELETE, Delete);
                                    SharedPreference.SavePreference(StaticUtility.sUPDATE_LINKEDIN, update_linkedin);
                                    SharedPreference.SavePreference(StaticUtility.sCONNECT_LINKEDIN, connect_linkedin);
                                    SharedPreference.SavePreference(StaticUtility.sDOWNLOAD, download);
                                    SharedPreference.SavePreference(StaticUtility.sCANCEL, cancel);
                                    SharedPreference.SavePreference(StaticUtility.sPROCESS, processing);
                                    SharedPreference.SavePreference(StaticUtility.sPLEASE_WAIT, please_wait);
                                    SharedPreference.SavePreference(StaticUtility.sMY_ADDRESS, my_address);
                                    SharedPreference.SavePreference(StaticUtility.sFIREST_NAME, first_name);
                                    SharedPreference.SavePreference(StaticUtility.sLAST_NAME, last_name);
                                    SharedPreference.SavePreference(StaticUtility.sADDRESS, address);
                                    SharedPreference.SavePreference(StaticUtility.sLANDMARK, landmark);
                                    SharedPreference.SavePreference(StaticUtility.sPINCODE, pincode);
                                    SharedPreference.SavePreference(StaticUtility.sSELECT_COUNTRY, select_country);
                                    SharedPreference.SavePreference(StaticUtility.sSELECT_STATE, select_state);
                                    SharedPreference.SavePreference(StaticUtility.sCITY, city);
                                    SharedPreference.SavePreference(StaticUtility.sCODE, code);
                                    SharedPreference.SavePreference(StaticUtility.sPHONE_NUMBER, phone_number);
                                    SharedPreference.SavePreference(StaticUtility.sIS_SHIPPING, is_shipping);
                                    SharedPreference.SavePreference(StaticUtility.sADD_ADDRESS, add_address);
                                    SharedPreference.SavePreference(StaticUtility.sOLD_PASSWORD, old_password);
                                    SharedPreference.SavePreference(StaticUtility.sNEW_PASSWORD, new_password);
                                    SharedPreference.SavePreference(StaticUtility.sCONFIRM_PASSWORD, confirm_password);
                                    SharedPreference.SavePreference(StaticUtility.sCHANGE_PASSWORD, change_password);
                                    SharedPreference.SavePreference(StaticUtility.sPAYMENT, payment);
                                    SharedPreference.SavePreference(StaticUtility.sSUCCESS, success);
                                    SharedPreference.SavePreference(StaticUtility.sNO_ADDRESS_AVAILABLE, no_address_available);
                                    SharedPreference.SavePreference(StaticUtility.sSELECT_CURRENCY, select_currency);
                                    SharedPreference.SavePreference(StaticUtility.sCURRENCY, currency);
                                    SharedPreference.SavePreference(StaticUtility.sDONE, done);
                                    SharedPreference.SavePreference(StaticUtility.sSKIP, skip);
                                    SharedPreference.SavePreference(StaticUtility.sUPDATE, update);
                                    SharedPreference.SavePreference(StaticUtility.sFORGOT_PASSWORD, forgot_password);
                                    SharedPreference.SavePreference(StaticUtility.sLOG_IN_NOW, log_in_now);
                                    SharedPreference.SavePreference(StaticUtility.sEMAIL_ADDRESS, email_address);
                                    SharedPreference.SavePreference(StaticUtility.sPASSWORD, password);
                                    SharedPreference.SavePreference(StaticUtility.sFORGOT_YOUR_PASSWORD, forgot_your_password);
                                    SharedPreference.SavePreference(StaticUtility.sLOG_IN, log_in);
                                    SharedPreference.SavePreference(StaticUtility.sNOT_HAVE_AN_ACCOUNT, not_have_an_account);
                                    SharedPreference.SavePreference(StaticUtility.sSIGN_UP_NOW, sign_up_now);
                                    SharedPreference.SavePreference(StaticUtility.sLOGOUT, logout);
                                    SharedPreference.SavePreference(StaticUtility.sSEACRCH_BEST_PRODUCT, search_best_product);
                                    SharedPreference.SavePreference(StaticUtility.sOUR_BEST_SELLER, our_best_seller);
                                    SharedPreference.SavePreference(StaticUtility.sMOST_WANTED, most_wanted);
                                    SharedPreference.SavePreference(StaticUtility.sNEW_ARRIVALS, new_arrivals);
                                    SharedPreference.SavePreference(StaticUtility.sHOME, home);
                                    SharedPreference.SavePreference(StaticUtility.sCATEGORY, category);
                                    SharedPreference.SavePreference(StaticUtility.sNOTIFICATION, notification);
                                    SharedPreference.SavePreference(StaticUtility.sWISHLIST, wishlist);
                                    SharedPreference.SavePreference(StaticUtility.sPROFILE, profile);
                                    SharedPreference.SavePreference(StaticUtility.sGUEST_EMAIL_ADDRESS, guest_email_address);
                                    SharedPreference.SavePreference(StaticUtility.sGUEST_USER, guest_user);
                                    SharedPreference.SavePreference(StaticUtility.sNO_ORDER_AVAILABLE, no_order_available);
                                    SharedPreference.SavePreference(StaticUtility.sFROM_DATE, from_date);
                                    SharedPreference.SavePreference(StaticUtility.sTO_DATE, to_date);
                                    SharedPreference.SavePreference(StaticUtility.sDELIVERED, delivered);
                                    SharedPreference.SavePreference(StaticUtility.sCANCELLED_ORDER, cancelled_order);
                                    SharedPreference.SavePreference(StaticUtility.sPENDING_ORDER, pending_order);
                                    SharedPreference.SavePreference(StaticUtility.sCONFIRM_ORDER, confirm_order);
                                    SharedPreference.SavePreference(StaticUtility.sIN_PROCESS, in_process);
                                    SharedPreference.SavePreference(StaticUtility.sOUT_FOR_DELIVERY, out_for_delivery);
                                    SharedPreference.SavePreference(StaticUtility.sRETURN_ORDER, return_order);
                                    SharedPreference.SavePreference(StaticUtility.sREFUND_ORDER, refund_order);
                                    SharedPreference.SavePreference(StaticUtility.sRETURNED_ORDER, returned_order);
                                    SharedPreference.SavePreference(StaticUtility.sORDER_ID, order_id);
                                    SharedPreference.SavePreference(StaticUtility.sPLEASE_TYPE_VERIFICATION_CODE_AND_SEND_TO, please_type_verification_code_and_send_to);
                                    SharedPreference.SavePreference(StaticUtility.sVERIFICATION_CODE, verification_code);
                                    SharedPreference.SavePreference(StaticUtility.sNOT_RECEIVE_OTP, not_receive_the_otp);
                                    SharedPreference.SavePreference(StaticUtility.sCONTINUE, strcontinue);
                                    SharedPreference.SavePreference(StaticUtility.sRESEND_AGAIN, resend_again);
                                    SharedPreference.SavePreference(StaticUtility.sENTER_YOUR_OTP, enter_your_otp);
                                    SharedPreference.SavePreference(StaticUtility.sVERIFY, verify);
                                    SharedPreference.SavePreference(StaticUtility.sGENDER, gender);
                                    SharedPreference.SavePreference(StaticUtility.sMALE, male);
                                    SharedPreference.SavePreference(StaticUtility.sFEMALE, female);
                                    SharedPreference.SavePreference(StaticUtility.sDATE_OF_BIRTH, date_of_birth);
                                    SharedPreference.SavePreference(StaticUtility.sSUBMIT, submit);
                                    SharedPreference.SavePreference(StaticUtility.sMOBILE_NO, mobile_no);
                                    SharedPreference.SavePreference(StaticUtility.sSIGN_UP, sign_up);
                                    SharedPreference.SavePreference(StaticUtility.sNO_REVIEW_AVAILABLE, no_review_available);
                                    SharedPreference.SavePreference(StaticUtility.sPOWERED_BY_BAZRBIT, powered_by_bazarBit);
                                    SharedPreference.SavePreference(StaticUtility.sSHOPPING_CART, shopping_cart);
                                    SharedPreference.SavePreference(StaticUtility.sSHOPPING_BAG_IS_EMPTY, shopping_bag_is_empty);
                                    SharedPreference.SavePreference(StaticUtility.sSTART_SHOPPING_NOW, start_shopping_now);
                                    SharedPreference.SavePreference(StaticUtility.sCHECKOUT, checkout);
                                    SharedPreference.SavePreference(StaticUtility.sREMOVE_THIS_PRODUCT_FROM_CART, remove_this_product_from_cart);
                                    SharedPreference.SavePreference(StaticUtility.sADD, add);
                                    SharedPreference.SavePreference(StaticUtility.sCHANGE, change);
                                    SharedPreference.SavePreference(StaticUtility.sYOU_HAVE_ANY_PROMOCODE, you_have_any_promocode);
                                    SharedPreference.SavePreference(StaticUtility.sPROMO_CODE, promo_code);
                                    SharedPreference.SavePreference(StaticUtility.sCASH_ON_DELIVERY, cash_on_delivery);
                                    SharedPreference.SavePreference(StaticUtility.sPAY_U_MONEY, pay_u_money);
                                    SharedPreference.SavePreference(StaticUtility.sCCAVENUE, ccavenue);
                                    SharedPreference.SavePreference(StaticUtility.sPAYPAL, paypal);
                                    SharedPreference.SavePreference(StaticUtility.sSUB_TOTAL, sub_total);
                                    SharedPreference.SavePreference(StaticUtility.sDISCOUNT, Discount);
                                    SharedPreference.SavePreference(StaticUtility.sSHIPPING_CHARGES, shipping_charges);
                                    SharedPreference.SavePreference(StaticUtility.sCOD_CHARGES, cod_charges);
                                    SharedPreference.SavePreference(StaticUtility.sTOTAL, total);
                                    SharedPreference.SavePreference(StaticUtility.sNEXT_PAYMENT_INFO, next_payment_info);
                                    SharedPreference.SavePreference(StaticUtility.sQUANTITY, quantity);
                                    SharedPreference.SavePreference(StaticUtility.sCONFIRM_PURCHASE, confirm_purchase);
                                    SharedPreference.SavePreference(StaticUtility.sSEND_OTP, send_otp);
                                    SharedPreference.SavePreference(StaticUtility.sRESEND, resend);
                                    SharedPreference.SavePreference(StaticUtility.sQTY, qty);
                                    SharedPreference.SavePreference(StaticUtility.sCHOOSE_FROM_PROMOCODE_BELOW, choose_from_promocode_blow);
                                    SharedPreference.SavePreference(StaticUtility.sPLACE_ORDER, place_order);
                                    SharedPreference.SavePreference(StaticUtility.sSHIPPING_ADDRESS, shipping_address);
                                    SharedPreference.SavePreference(StaticUtility.sBILLING_ADDRESS, billing_address);
                                    SharedPreference.SavePreference(StaticUtility.sSAME_AS_SHIPPING_ADDRESS, same_as_shipping_address);
                                    SharedPreference.SavePreference(StaticUtility.sENTER_PROMO_CODE, enter_promo_code);
                                    SharedPreference.SavePreference(StaticUtility.sAPPLY_PROMOCODE, apply_promocode);
                                    SharedPreference.SavePreference(StaticUtility.sCANCEL_ORDER, cancel_order);
                                    SharedPreference.SavePreference(StaticUtility.sCONTACT_US, contact_us);
                                    SharedPreference.SavePreference(StaticUtility.sNAME, name);
                                    SharedPreference.SavePreference(StaticUtility.sSUBJECT, subject);
                                    SharedPreference.SavePreference(StaticUtility.sMESSAGE, message);
                                    SharedPreference.SavePreference(StaticUtility.sFEEDBACK, feedback);
                                    SharedPreference.SavePreference(StaticUtility.sANS, ans);
                                    SharedPreference.SavePreference(StaticUtility.sQ, q);
                                    SharedPreference.SavePreference(StaticUtility.sMY_ACCOUNT, my_account);
                                    SharedPreference.SavePreference(StaticUtility.sORDER_HISTORY, order_history);
                                    SharedPreference.SavePreference(StaticUtility.sREVIEW, review);
                                    SharedPreference.SavePreference(StaticUtility.sORDER_DETAIL, order_detail);
                                    SharedPreference.SavePreference(StaticUtility.sORDER_DATE, order_date);
                                    SharedPreference.SavePreference(StaticUtility.sORDER_TOTAL, order_total);
                                    SharedPreference.SavePreference(StaticUtility.sPAYMENT_INFORMATION, payment_information);
                                    SharedPreference.SavePreference(StaticUtility.sITEM_TOTAL, item_total);
                                    SharedPreference.SavePreference(StaticUtility.sSURE_CANCEL_THIS_ORDER, sure_cancel_this_order);
                                    SharedPreference.SavePreference(StaticUtility.sADD_YOUR_REVIEW, add_your_review);
                                    SharedPreference.SavePreference(StaticUtility.sWOULD_YOU_LIKE_TO_RECOMMEND_THIS_PRODUCT, would_you_like_to_recommend_this_product);
                                    SharedPreference.SavePreference(StaticUtility.sUSER_NAME, user_name);
                                    SharedPreference.SavePreference(StaticUtility.sTITLE, title);
                                    SharedPreference.SavePreference(StaticUtility.sDESCRIPTION, description);
                                    SharedPreference.SavePreference(StaticUtility.sRETURN_PRODUCT, return_product);
                                    SharedPreference.SavePreference(StaticUtility.sRETURN_DETAIL, return_detail);
                                    SharedPreference.SavePreference(StaticUtility.sACCOUNT_HOLDER_NAME, account_holder_name);
                                    SharedPreference.SavePreference(StaticUtility.sBANK_NAME, bank_name);
                                    SharedPreference.SavePreference(StaticUtility.sBRANCH_NAME, branch_name);
                                    SharedPreference.SavePreference(StaticUtility.sACCOUNT_NUMBER, account_number);
                                    SharedPreference.SavePreference(StaticUtility.sIFSC_CODE, ifsc_code);
                                    SharedPreference.SavePreference(StaticUtility.sREASON, reason);
                                    SharedPreference.SavePreference(StaticUtility.sYES, yes);
                                    SharedPreference.SavePreference(StaticUtility.sNO, no);
                                    SharedPreference.SavePreference(StaticUtility.sITEMS, items);
                                    SharedPreference.SavePreference(StaticUtility.sREFERRAL_CODE, referral_code);
                                    SharedPreference.SavePreference(StaticUtility.sPRE_BOOKING, pre_booking);
                                    SharedPreference.SavePreference(StaticUtility.sCOMMISSION, commission);
                                    SharedPreference.SavePreference(StaticUtility.sCOMMISSION_PERCENTAGE, commission_percentage);
                                    SharedPreference.SavePreference(StaticUtility.sCOMMISSION_AMOUNT, commission_amount);
                                    SharedPreference.SavePreference(StaticUtility.sLOGIN_REQUIRED, login_required);
                                    SharedPreference.SavePreference(StaticUtility.sREAD_LESS, read_less);
                                    SharedPreference.SavePreference(StaticUtility.sPENDING, pending);
                                    SharedPreference.SavePreference(StaticUtility.sREJECTED, rejected);
                                    SharedPreference.SavePreference(StaticUtility.sDAYS, days);
                                    SharedPreference.SavePreference(StaticUtility.sCHECK_PINCODE_AVAILABILITY, check_pincode_availibility);
                                    SharedPreference.SavePreference(StaticUtility.sCHECK_PINCODE_ERROR, check_pincode_error);
                                    SharedPreference.SavePreference(StaticUtility.sSELECT_SHARE_TYPE, select_share_type);
                                    SharedPreference.SavePreference(StaticUtility.sSELECT_IMAGE, select_image);
                                    SharedPreference.SavePreference(StaticUtility.sTAKE_PHOTO, take_photo);
                                    SharedPreference.SavePreference(StaticUtility.sGALLERY, gallery);
                                    SharedPreference.SavePreference(StaticUtility.sFACEBOOK, facebook);
                                    SharedPreference.SavePreference(StaticUtility.sTWITTER, twitter);
                                    SharedPreference.SavePreference(StaticUtility.sGOOGLE, google);
                                    SharedPreference.SavePreference(StaticUtility.sSKYPE, skype);
                                    SharedPreference.SavePreference(StaticUtility.sLINKDIN, linkdin);
                                    SharedPreference.SavePreference(StaticUtility.sWHATSAPP, whatsapp);
                                    SharedPreference.SavePreference(StaticUtility.sPRINTEREST, printerest);
                                    SharedPreference.SavePreference(StaticUtility.sPASSWORD_LIMIT_VALID, password_limit_valid);
                                    SharedPreference.SavePreference(StaticUtility.sPASSWORD_CHECK_EMPTY, password_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sCONFIRM_PASSWORD_LIMIT_VALID, confirm_password_limit_valid);
                                    SharedPreference.SavePreference(StaticUtility.sCONFIRM_PASSWORD_CHECK_EMPTY, confirm_password_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sCONFIRM_PASSWORD_CHECK_MATCH, confirm_password_check_match);
                                    SharedPreference.SavePreference(StaticUtility.sOLD_PASSWORD_CHECK_EMPTY, old_password_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sOLD_PASSWORD_CHECK_EMPTY, new_password_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sEMAIL_CHECK_VALID, email_check_valid);
                                    SharedPreference.SavePreference(StaticUtility.sEMAIL_CHECK_EMPTY, email_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sFIRSTNAME_CHECK_EMPTY, firstname_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sLASTNAME_CHECK_EMPTY, lastname_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sADDRESS_CHECK_EMPTY, address_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sLANDMARK_CHECK_EMPTY, landmark_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sPINCODE_CHECK_EMPTY, pincode_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sCITY_CHECK_EMPTY, city_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sPHONE_CHECK_EMPTY, phone_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sPHONE_CHECK_VALID, phone_check_valid);
                                    SharedPreference.SavePreference(StaticUtility.sCOUNTRY_CHECK_EMPTY, country_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sSTATE_CHECK_EMPTY, state_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sDOB_CHECK_EMPTY, dob_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sANS_CHECK_EMPTY, ans_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sRATING_CHECK_EMPTY, rating_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sDESC_CHECK_EMPTY, desc_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sTITLE_CHECK_EMPTY, title_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sUNM_CHECK_EMPTY, unm_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sREASON_CHECK_EMPTY, reason_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sIFSC_CHECK_EMPTY, ifsc_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sACCNO_CHECK_EMPTY, accno_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sBANKNAME_CHECK_EMPTY, bankname_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sBRANCH_CHECK_EMPTY, branch_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sACC_HOLD_NAME_CHECK_EMPTY, acc_hold_name_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sSUBJECT_CHECK_EMPTY, subject_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sMSG_CHECK_EMPTY, msg_check_empty);
                                    SharedPreference.SavePreference(StaticUtility.sPWD_NEW_PWD_CHECK_EMPTY, pwd_newpwd_check_match);
                                    SharedPreference.SavePreference(StaticUtility.sNO_MORE_COMMISSION_CHECK_EMPTY, no_more_commission_found);
                                    SharedPreference.SavePreference(StaticUtility.sPLEASE_ENTER_OTP, please_enter_otp);
                                    SharedPreference.SavePreference(StaticUtility.sPLEASE_SELECT_PAYMENT_GATEWAY, please_select_payment_gateway);
                                    SharedPreference.SavePreference(StaticUtility.sbingage_wallet_error, bingage_wallet_error);
                                    SharedPreference.SavePreference(StaticUtility.sredeem_from_wallet, redeem_from_wallet);
                                    SharedPreference.SavePreference(StaticUtility.wallet_balance, wallet_balance);
                                }
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strMessage.equalsIgnoreCase("app-id and app-secret is required")) {
//                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    Global.showSnackBar(coodinator, "Application is not active !");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            System.exit(0);
                                        }
                                    }, 5000);

                                }
//                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in LoginActivity.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR PaymentGateway API..
    private void PaymentGateway() {

        String strCurrencyCode = SharedPreference.GetPreference(context, Global.PREFERENCECURRENCY, StaticUtility.sCurrencyCode);
        String[] key = {"currencyCode"};
        String[] val = {strCurrencyCode};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetPaymentGateway);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                /*if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    JSONArray jaPG = jsonObjectPayload.optJSONArray("pg");
                                    SharedPreference.CreatePreference(context, StaticUtility.PREFERENCEPAYMENTGATETWAY);
                                    SharedPreference.SavePreference(StaticUtility.sPAYMENTGATEWAY, jaPG.toString());
                                    for (int i = 0; i < jaPG.length(); i++) {
                                        JSONObject joPG = (JSONObject) jaPG.get(i);
                                        String strPaymentgatewayKey = joPG.optString("paymentgateway_key");
                                        if (strPaymentgatewayKey.equalsIgnoreCase("cod_detail")) {
                                            JSONObject jsonObjectCodDetail = jsonObjectPayload.getJSONObject("cod_detail");
                                            String CODstatus = jsonObjectCodDetail.getString("status");
                                            SharedPreference.SavePreference(StaticUtility.sCODAVAILABLE, CODstatus);
                                        }

                                    }
                                }*/
                                if (strStatus.equals("ok")) {
                                    JSONObject jsonObjectPayload = response.getJSONObject("payload");
                                    JSONArray jaPG = jsonObjectPayload.optJSONArray("pg");
                                    SharedPreference.CreatePreference(context, StaticUtility.PREFERENCEPAYMENTGATETWAY);
                                    SharedPreference.SavePreference(StaticUtility.sPAYMENTGATEWAY, jaPG.toString());

                                    if (jsonObjectPayload.has("cod_detail")) {
                                        JSONObject jsonObjectCodDetail = jsonObjectPayload.getJSONObject("cod_detail");
                                        String CODstatus = jsonObjectCodDetail.getString("status");
                                        SharedPreference.SavePreference(StaticUtility.sCODAVAILABLE, CODstatus);
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in checkoutfragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion
}
