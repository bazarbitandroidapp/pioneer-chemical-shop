package com.pureweblopment.pioneerchemicalshop.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.pioneerchemicalshop.Adapter.AdapterCurrency;
import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.SendMail;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;
import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.pureweblopment.pioneerchemicalshop.Model.Currency;
import com.pureweblopment.pioneerchemicalshop.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class CurrencyScreen extends AppCompatActivity implements AdapterCurrency.CurrencySelection, View.OnClickListener {

    Context mContext = CurrencyScreen.this;

    //Internet Alert
    public EventBus eventBus = EventBus.getDefault();
    public static int i = 0;
    public static AlertDialog internetAlert;

    private TextView mTxtSkip, mTxtSelectCurrencyTitle;
    private RecyclerView mRvCurrency;
    private TextView mTxtDone;

    //Loader
    private RelativeLayout mRlProgress;
    private ProgressBar mProgress;

    private String strRedirectType = "", strCurrencyCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_currency_screen);
        eventBus.register(this);

        SharedPreference.CreatePreference(mContext, Global.PREFERENCECURRENCYSCREEN);
        SharedPreference.SavePreference(StaticUtility.sCURRENCYSCREENVISIBILITY, "yes");

        Initialization();
        TypeFace();
        OnClickListener();
        AppSetting();
        setDynamicString();

        strRedirectType = getIntent().getStringExtra("redirect");
        if (strRedirectType.equalsIgnoreCase("myaccount")) {
            mTxtSkip.setVisibility(View.GONE);
        } else {
            mTxtSkip.setVisibility(View.VISIBLE);
        }

        if (Global.isNetworkAvailable(mContext)) {
            GetCurrencyList("");
        } else {
            openInternetAlertDialog(mContext, "Not connected to Internet");
        }
    }

    private void setDynamicString() {
        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_CURRENCY) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_CURRENCY).equals("")) {
                mTxtSelectCurrencyTitle.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSELECT_CURRENCY));
            } else {
                mTxtSelectCurrencyTitle.setText(getString(R.string.select_currency));
            }
        } else {
            mTxtSelectCurrencyTitle.setText(getString(R.string.select_currency));
        }
        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDONE) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDONE).equals("")) {
                mTxtDone.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDONE));
            } else {
                mTxtDone.setText(getString(R.string.done));
            }
        } else {
            mTxtDone.setText(getString(R.string.done));
        }
        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSKIP) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSKIP).equals("")) {
                mTxtSkip.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSKIP) + " >>");
            } else {
                mTxtSkip.setText(getString(R.string.skip));
            }
        } else {
            mTxtSkip.setText(getString(R.string.skip));
        }
    }

    //region Initialization
    private void Initialization() {
        mTxtSkip = findViewById(R.id.txtSkip);
        mTxtSelectCurrencyTitle = findViewById(R.id.txtSelectCurrencyTitle);
        mRvCurrency = findViewById(R.id.rvCurrency);
        mRlProgress = findViewById(R.id.rlProgress);
        mProgress = findViewById(R.id.progress);
        mTxtDone = findViewById(R.id.txtDone);
    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        mTxtSkip.setTypeface(Typefaces.TypefaceCalibri_bold(mContext));
        mTxtSelectCurrencyTitle.setTypeface(Typefaces.TypefaceCalibri_bold(mContext));
        mTxtDone.setTypeface(Typefaces.TypefaceCalibri_bold(mContext));
    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        mTxtSkip.setOnClickListener(this);
        mTxtDone.setOnClickListener(this);
    }//endregion

    //region AppSetting..
    @SuppressLint("NewApi")
    private void AppSetting() {
        mTxtSkip.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        mTxtSelectCurrencyTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mProgress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(
                    SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE,
                            StaticUtility.ThemePrimaryColor))));
        }
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(mContext, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = (TextView) row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = (TextView) row.findViewById(R.id.tvTitle);
        final Button btnSettings = (Button) row.findViewById(R.id.btnSettings);
        final Button btnExit = (Button) row.findViewById(R.id.btnExit);

        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF).equals("")) {
                tvTitle.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF));
            } else {
                tvTitle.setText(getText(R.string.your_data_is_off));
            }
        } else {
            tvTitle.setText(getText(R.string.your_data_is_off));
        }

        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI).equals("")) {
                tvAlertText.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI));
            } else {
                tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
            }
        } else {
            tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
        }

        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS).equals("")) {
                btnSettings.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS));
            } else {
                btnSettings.setText(getText(R.string.settings));
            }
        } else {
            btnSettings.setText(getText(R.string.settings));
        }

        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT) != null) {
            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT).equals("")) {
                btnExit.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT));
            } else {
                btnExit.setText(getText(R.string.exit));
            }
        } else {
            btnExit.setText(getText(R.string.exit));
        }

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(mContext)) {
                openInternetAlertDialog(mContext, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion

    //region FOR Get Currency List...
    private void GetCurrencyList(final String strCurrencyCode) {
        mRlProgress.setVisibility(View.VISIBLE);
        String[] key = {"currencyCode"};
        String[] val = {strCurrencyCode};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(
                StaticUtility.URL + StaticUtility.GetCurrencyList);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        mRlProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.optString("status");
                                String strMessage = response.optString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    JSONObject joPayload = response.optJSONObject("payload");
                                    JSONArray jsonArrayPayload = joPayload.optJSONArray("multi_currency");
                                    if (jsonArrayPayload.length() > 0) {
                                        ArrayList<Currency> currencies = new ArrayList<>();
                                        for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                            JSONObject jsonObjectPayload = (JSONObject) jsonArrayPayload.get(i);
                                            String strZcurrencyidPk = jsonObjectPayload.optString("z_currencyid_pk");
                                            String strCurrencyId = jsonObjectPayload.optString("currency_id");
                                            String strCurrency = jsonObjectPayload.optString("currency");
                                            String strCode = jsonObjectPayload.optString("code");
                                            String strFlag = jsonObjectPayload.optString("flag");
                                            String strSymbol = jsonObjectPayload.optString("symbol");
                                            String strPostOrPreText = jsonObjectPayload.optString("post_or_pre_text");
                                            String strIsBasecurrency = jsonObjectPayload.optString("is_basecurrency");
                                            String strIsActive = jsonObjectPayload.optString("is_active");
                                            String strZcurrencyrateidPk = jsonObjectPayload.optString("z_currencyrateid_pk");
                                            String strCurrencyRateId = jsonObjectPayload.optString("currency_rate_id");
                                            String strZcurrencyidFk = jsonObjectPayload.optString("z_currencyid_fk");
                                            String strRate = jsonObjectPayload.optString("rate");
                                            if (!strCurrencyCode.equalsIgnoreCase("")) {
                                                MainActivity.manageBackPress(true);
                                                SharedPreference.CreatePreference(mContext, Global.PREFERENCECURRENCY);
                                                SharedPreference.SavePreference(StaticUtility.sCurrencyName, strCurrency);
                                                SharedPreference.SavePreference(StaticUtility.sCurrencySign, strSymbol);
                                                SharedPreference.SavePreference(StaticUtility.sCurrencySignPosition, strPostOrPreText);
                                                SharedPreference.SavePreference(StaticUtility.sCurrencyCode, strCode);
                                                Intent intent = new Intent(CurrencyScreen.this, MainActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                                finish();
                                            }
                                            currencies.add(new Currency(strCurrencyId, strCurrency, strCode, strSymbol, strPostOrPreText));
                                        }
                                        if (strCurrencyCode.equalsIgnoreCase("")) {
                                            if (currencies.size() > 0) {
                                                String strCurrencyCode = SharedPreference.GetPreference(mContext, Global.PREFERENCECURRENCY, StaticUtility.sCurrencyCode);
                                                AdapterCurrency adapterCurrency = new AdapterCurrency(mContext, currencies, strRedirectType, strCurrencyCode);
                                                mRvCurrency.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                                                mRvCurrency.setAdapter(adapterCurrency);
                                            }
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                mRlProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(mContext, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(mContext, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(mContext, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(mContext, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(mContext, Global.Billing_Preference);
                                    Intent intent = new Intent(mContext, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(mContext, Global.TOEMAIL, Global.SUBJECT, "Getting error in CurrencyScreen.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
//                Toast.makeText(mContext, anError.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    //endregion

    @Override
    public void SelectedCurrency(String CurrencyCode, String Currency) {
        strCurrencyCode = CurrencyCode;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSkip:
                Intent intent = new Intent(CurrencyScreen.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.txtDone:
                if (strCurrencyCode.equalsIgnoreCase("")) {
                    MainActivity.manageBackPress(true);
                    Intent intent1 = new Intent(CurrencyScreen.this, MainActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent1);
                    finish();
                } else {
                    GetCurrencyList(strCurrencyCode);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (strRedirectType.equalsIgnoreCase("myaccount")) {
            super.onBackPressed();
        } else {
            Intent intent = new Intent(CurrencyScreen.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
