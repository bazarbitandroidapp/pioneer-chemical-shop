package com.pureweblopment.pioneerchemicalshop.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.pioneerchemicalshop.Fragment.CheckoutFragment;
import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.SendMail;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;
import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.pureweblopment.pioneerchemicalshop.Model.AddressList;
import com.pureweblopment.pioneerchemicalshop.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class CheckoutAddressListActivity extends AppCompatActivity implements
        View.OnClickListener {

    Context context = CheckoutAddressListActivity.this;

    RelativeLayout relativeProgress;
    TextView txtAddressNameTitle;
    static RecyclerView recyclerviewAddress;
    Button btnAddAddress;
    String addressType = "", addressTypeName = "";

    Bundle bundle;
    LinearLayout llPaymentHeader, llBottomNavigation;
    ImageView imageCartBack;
    String ActivityType;
    static ArrayList<AddressList> addressLists = new ArrayList<>();

    FrameLayout framLayoutAddressList;

    boolean isRefresh = false;

    boolean isSelectAddress = false;
    TextView txtAddressName;
    TextView txtAddress, txtPayment, txtSuccess;

    TextView txtNoAddressAvailable;
    LinearLayout llToolbar;

    //Internet Alert
    public EventBus eventBus = EventBus.getDefault();
    public static int i = 0;
    public static AlertDialog internetAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_address_list);

        eventBus.register(this);

        CheckoutFragment.isBillingAddress = false;
        ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        Initialization();
        TypeFace();
        OnClickListener();
        setDynamicString();

        chanageButton(btnAddAddress);
        llPaymentHeader.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

        Bundle bundle = getIntent().getBundleExtra("BundleAddress");
        addressType = bundle.getString("AddressType");
        addressTypeName = bundle.getString("AddressTypeName");
        ActivityType = bundle.getString("ActivityType");
        txtAddressNameTitle.setText(addressTypeName);
       /* if(addressTypeName.equalsIgnoreCase("Billing Address")) {
            txtAddressNameTitle.setText("Billing Address");
        }else
        {
            txtAddressNameTitle.setText("Shipping Address");
        }*/
        addressLists = (ArrayList<AddressList>) bundle.getSerializable("AddressList");

        if (addressLists.size() > 0) {
            txtNoAddressAvailable.setVisibility(View.GONE);
            AdapterAddress adapterAddress = new AdapterAddress(context, addressLists);
            recyclerviewAddress.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            recyclerviewAddress.setAdapter(adapterAddress);
        } else {
            getAddressList(addressType);
        }
    }

    //region Initialization
    private void Initialization() {
        txtAddressNameTitle = findViewById(R.id.txtAddressNameTitle);
        recyclerviewAddress = findViewById(R.id.recyclerviewAddress);
        btnAddAddress = findViewById(R.id.btnAddAddress);
        relativeProgress = findViewById(R.id.relativeProgress);
        llPaymentHeader = findViewById(R.id.llPaymentHeader);
        framLayoutAddressList = findViewById(R.id.framLayoutAddressList);
        imageCartBack = findViewById(R.id.imageCartBack);
        txtAddressName = findViewById(R.id.txtAddressName);
        txtNoAddressAvailable = findViewById(R.id.txtNoAddressAvailable);
        txtAddress = findViewById(R.id.txtAddress);
        txtPayment = findViewById(R.id.txtPayment);
        txtSuccess = findViewById(R.id.txtSuccess);
        txtAddressName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtNoAddressAvailable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

        llToolbar = findViewById(R.id.llToolbar);
        llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        imageCartBack.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtAddressName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtPayment.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtSuccess.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        txtAddressNameTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtAddressName.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtAddress.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtPayment.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtSuccess.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        txtNoAddressAvailable.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        btnAddAddress.setOnClickListener(this);
        imageCartBack.setOnClickListener(this);
    }
    //endregion

    //region For set dynamic string
    private void setDynamicString() {
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS).equals("")) {
                txtAddressName.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS));
            } else {
                txtAddressName.setText(getString(R.string.myaddress));
            }
        } else {
            txtAddressName.setText(getString(R.string.myaddress));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS).equals("")) {
                txtAddress.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS));
            } else {
                txtAddress.setText(getString(R.string.address));
            }
        } else {
            txtAddress.setText(getString(R.string.address));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT).equals("")) {
                txtPayment.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT));
            } else {
                txtPayment.setText(getString(R.string.payment));
            }
        } else {
            txtPayment.setText(getString(R.string.payment));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS).equals("")) {
                txtSuccess.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS));
            } else {
                txtSuccess.setText(getString(R.string.success));
            }
        } else {
            txtSuccess.setText(getString(R.string.success));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_ADDRESS) != null) {
            btnAddAddress.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_ADDRESS));
        } else {
            btnAddAddress.setText(getString(R.string.add_address));
        }
    }
    //endregion


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddAddress:
                isRefresh = true;
                Intent intent = new Intent(context, CheckoutAddAddressActivity.class);
                intent.putExtra("ActivityType", ActivityType);
                intent.putExtra("AddressType", addressType);
                intent.putExtra("Type", "AddressListing");
                startActivity(intent);
                break;

            case R.id.imageCartBack:
                onBackPressed();
                finish();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            getAddressList(addressType);
        }
    }

    //region FOR getAddressList API..
    private void getAddressList(String type) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key;
        String[] val;

        if (ActivityType.equals("MainActivity")) {
            key = new String[]{};
            val = new String[]{};
        } else if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
            key = new String[]{"address_type"};
            val = new String[]{type};
        } else {
            key = new String[]{};
            val = new String[]{};
        }

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.AddressList);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONArray jsonArrayPayload = response.getJSONArray("payload");
                                    addressLists = new ArrayList<>();
                                    if (jsonArrayPayload.length() > 0) {
                                        txtNoAddressAvailable.setVisibility(View.GONE);
                                        for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                            JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(i);
                                            String Fname = jsonObjectPayload.getString("first_name");
                                            String Lname = jsonObjectPayload.getString("last_name");
                                            String address = jsonObjectPayload.getString("address");
                                            String pincode = jsonObjectPayload.getString("pincode");
                                            String city = jsonObjectPayload.getString("city");
                                            String state = jsonObjectPayload.getString("state");
                                            String phone_number = jsonObjectPayload.getString("phone_number");
                                            String useradress_id = jsonObjectPayload.getString("useradress_id");
                                            String landmark = jsonObjectPayload.getString("landmark");
                                            String country = jsonObjectPayload.getString("country");
                                            String address_type = jsonObjectPayload.getString("address_type");
                                            addressLists.add(new AddressList(Fname, Lname, address, city, pincode, state, phone_number, useradress_id, landmark, country, address_type));
                                        }
                                        if (addressLists.size() > 0) {
                                            txtNoAddressAvailable.setVisibility(View.GONE);
                                            AdapterAddress adapterAddress = new AdapterAddress(context, addressLists);
                                            recyclerviewAddress.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                                            recyclerviewAddress.setAdapter(adapterAddress);
                                        } else {
                                            txtNoAddressAvailable.setVisibility(View.VISIBLE);
                                        }
                                    } else {
                                        txtNoAddressAvailable.setVisibility(View.VISIBLE);
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    /*finish();*/
                                } else if (strMessage.equals("app-id and app-secret is required")) {
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in AddressListFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ADAPTER
    public class AdapterAddress extends RecyclerView.Adapter<AdapterAddress.Viewholder> {

        Context context;
        JSONArray jsonArray;
        ArrayList<AddressList> addressLists = new ArrayList<>();

        public AdapterAddress(Context context, ArrayList<AddressList> addressLists) {
            this.context = context;
            this.addressLists = addressLists;
        }

        @Override
        public AdapterAddress.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_address_items, viewGroup, false);
            return new AdapterAddress.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder holder, final int position) {
            final AddressList addressList = addressLists.get(position);

            holder.UserName.setText(Catpital(addressList.getFirstName()) + " " + Catpital(addressList.getLastName()));
            holder.txtAddress.setText(addressList.getAddress() + "," + "\n" +
                    addressList.getCity() + " - " + addressList.getPincode() + "," + "\n" + addressList.getState());
            holder.txtPhoneNo.setText(addressList.getPhone_number());

            holder.txtEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String FName = addressList.getFirstName();
                    String LName = addressList.getLastName();
                    String Address = addressList.getAddress();
                    String Landmark = addressList.getLandmark();
                    String Pincode = addressList.getPincode();
                    String Country = addressList.getCountry();
                    String State = addressList.getState();
                    String City = addressList.getCity();
                    String Phoneno = addressList.getPhone_number();
                    String useradress_id = addressList.getUserAddressID();

                    Intent intent = new Intent(context, EditAddressesActivity.class);
                    intent.putExtra("ActivityType", ActivityType);
                    intent.putExtra("Fname", FName);
                    intent.putExtra("Lname", LName);
                    intent.putExtra("Address", Address);
                    intent.putExtra("Landmark", Landmark);
                    intent.putExtra("Pincode", Pincode);
                    intent.putExtra("Country", Country);
                    intent.putExtra("State", State);
                    intent.putExtra("City", City);
                    intent.putExtra("Phoneno", Phoneno);
                    intent.putExtra("addressType", addressType);
                    intent.putExtra("useradress_id", useradress_id);
                    intent.putExtra("Position", String.valueOf(position));
                    startActivity(intent);

                    /*if (ActivityType.equals("MainActivity")) {
                        mListener = (OnFragmentInteractionListener) context;
                        mListener.goToEditAddress(FName, LName, Address, Landmark, Pincode, Country, State, City,
                                Phoneno, addressType, useradress_id, String.valueOf(position));
                    } else if(ActivityType.equalsIgnoreCase("MyAddressActivity")){
                        mListener = (OnFragmentInteractionListener) context;
                        mListener.goToEditCheckoutAddress(FName, LName, Address, Landmark, Pincode, Country, State, City,
                                Phoneno, addressType, useradress_id, String.valueOf(position));
                    }else {
                        mListener = (OnFragmentInteractionListener) context;
                        mListener.goToEditAddress(FName, LName, Address, Landmark, Pincode, Country, State, City,
                                Phoneno, addressType, useradress_id, String.valueOf(position));
                    }*/

                }
            });


            holder.txtDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String useradress_id = addressList.getUserAddressID();
                    String address_type = addressList.getAddress_type();
                    DeleteAddresses(useradress_id, address_type, position);
                }
            });

            if (ActivityType.equals("MainActivity")) {
                holder.cartAddress.setEnabled(false);
            } else if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
                holder.cartAddress.setEnabled(true);
            } else {
                holder.cartAddress.setEnabled(false);
            }

            holder.cartAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String FName = addressList.getFirstName();
                    String LName = addressList.getLastName();
                    String Address = addressList.getAddress();
                    String Landmark = addressList.getLandmark();
                    String Pincode = addressList.getPincode();
                    String Country = addressList.getCountry();
                    String State = addressList.getState();
                    String City = addressList.getCity();
                    String Phoneno = addressList.getPhone_number();
                    String useradress_id = addressList.getUserAddressID();
                    /*MainActivity.isCheckoutBack = "AddressList";*/
                    if (addressType.equals("0")) {
                        CheckoutFragment.isShippingAddressChange = true;
                        String username = holder.UserName.getText().toString();
                        String address = holder.txtAddress.getText().toString();
                        String phoneno = holder.txtPhoneNo.getText().toString();
                        SharedPreference.CreatePreference(context, Global.Shipping_Preference);
                        SharedPreference.SavePreference(StaticUtility.IsSelectAddress, "2");
                        SharedPreference.SavePreference(StaticUtility.strShippingUserName, username);
                        SharedPreference.SavePreference(StaticUtility.strShippingAddress, address);
                        SharedPreference.SavePreference(StaticUtility.strShippingPhoneno, phoneno);
                        SharedPreference.SavePreference(StaticUtility.strShippingFname, FName);
                        SharedPreference.SavePreference(StaticUtility.strShippingLname, LName);
                        SharedPreference.SavePreference(StaticUtility.strShippingAddress1, Address);
                        SharedPreference.SavePreference(StaticUtility.strShippingLandmark, Landmark);
                        SharedPreference.SavePreference(StaticUtility.strPincode, Pincode);
                        SharedPreference.SavePreference(StaticUtility.strShippingCountry, Country);
                        SharedPreference.SavePreference(StaticUtility.strShippingState, State);
                        SharedPreference.SavePreference(StaticUtility.strShippingCity, City);
                        onBackPressed();
                        finish();
                       /* mListener = (AddressListFragment.OnFragmentInteractionListener) context;
                        mListener.gotoCheckout();*/
                    } else {
                        CheckoutFragment.isBillingAddressChange = true;
                        String username = holder.UserName.getText().toString();
                        String address = holder.txtAddress.getText().toString();
                        String phoneno = holder.txtPhoneNo.getText().toString();
                        SharedPreference.CreatePreference(context, Global.Billing_Preference);
                        SharedPreference.SavePreference(StaticUtility.IsSelectAddress, "2");
                        SharedPreference.SavePreference(StaticUtility.strBillingUserName, username);
                        SharedPreference.SavePreference(StaticUtility.strBillingAddress, address);
                        SharedPreference.SavePreference(StaticUtility.strBillingPhoneno, phoneno);
                        SharedPreference.SavePreference(StaticUtility.strBillingFname, FName);
                        SharedPreference.SavePreference(StaticUtility.strBillingLname, LName);
                        SharedPreference.SavePreference(StaticUtility.strBillingAddress1, Address);
                        SharedPreference.SavePreference(StaticUtility.strBillingLandmark, Landmark);
                        SharedPreference.SavePreference(StaticUtility.strBillingPincode, Pincode);
                        SharedPreference.SavePreference(StaticUtility.strBillingCountry, Country);
                        SharedPreference.SavePreference(StaticUtility.strBillingState, State);
                        SharedPreference.SavePreference(StaticUtility.strBillingCity, City);
                        onBackPressed();
                        finish();
                        /*mListener = (AddressListFragment.OnFragmentInteractionListener) context;
                        mListener.gotoCheckout();*/
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return addressLists.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            TextView UserName, txtEdit, txtDelete, txtAddress, txtPhoneNo;
            CardView cartAddress;

            public Viewholder(View itemView) {
                super(itemView);
                txtPhoneNo = itemView.findViewById(R.id.txtPhoneNo);
                UserName = itemView.findViewById(R.id.UserName);
                txtEdit = itemView.findViewById(R.id.txtEdit);
                txtDelete = itemView.findViewById(R.id.txtDelete);
                txtAddress = itemView.findViewById(R.id.txtAddress);
                cartAddress = (CardView) itemView.findViewById(R.id.cartAddress);

                UserName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtEdit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtDelete.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEDIT) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEDIT).equals("")) {
                        txtEdit.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEDIT));
                    } else {
                        txtEdit.setText(context.getString(R.string.edit));
                    }
                } else {
                    txtEdit.setText(context.getString(R.string.edit));
                }

                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELETE) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELETE).equals("")) {
                        txtDelete.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELETE));
                    } else {
                        txtDelete.setText(context.getString(R.string.delete));
                    }
                } else {
                    txtDelete.setText(context.getString(R.string.delete));
                }

                UserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtEdit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtDelete.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

            }
        }
    }
    //endregion

    //region FOR DeleteAddresses API..
    private void DeleteAddresses(String useradress_id, final String address_type, final int position) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"useradress_id", "address_type"};
        String[] val = {useradress_id, address_type};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.DeleteAddress);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    String strFname = "", strLname = "", strLandmark = "", strPincode = "",
                                            strCountry = "", strState = "", strCity = "", strPhoneno = "",
                                            strAddress = "";
                                    if (addressType.equalsIgnoreCase("0")) {
                                        CheckoutFragment.isShippingAddressChange = true;
                                        strPincode = SharedPreference.GetPreference(context, Global.Shipping_Preference, StaticUtility.strPincode);
                                        strPhoneno = SharedPreference.GetPreference(context, Global.Shipping_Preference, StaticUtility.strShippingPhoneno);
                                        strFname = SharedPreference.GetPreference(context, Global.Shipping_Preference, StaticUtility.strShippingFname);
                                        strLname = SharedPreference.GetPreference(context, Global.Shipping_Preference, StaticUtility.strShippingLname);
                                        strAddress = SharedPreference.GetPreference(context, Global.Shipping_Preference, StaticUtility.strShippingAddress1);
                                        strLandmark = SharedPreference.GetPreference(context, Global.Shipping_Preference, StaticUtility.strShippingLandmark);
                                        strCountry = SharedPreference.GetPreference(context, Global.Shipping_Preference, StaticUtility.strShippingCountry);
                                        strState = SharedPreference.GetPreference(context, Global.Shipping_Preference, StaticUtility.strShippingState);
                                        strCity = SharedPreference.GetPreference(context, Global.Shipping_Preference, StaticUtility.strShippingCity);
                                    } else {
                                        CheckoutFragment.isBillingAddressChange = true;
                                        strPincode = SharedPreference.GetPreference(context, Global.Billing_Preference, StaticUtility.strBillingPincode);
                                        strFname = SharedPreference.GetPreference(context, Global.Billing_Preference, StaticUtility.strBillingFname);
                                        strLname = SharedPreference.GetPreference(context, Global.Billing_Preference, StaticUtility.strBillingLname);
                                        strAddress = SharedPreference.GetPreference(context, Global.Billing_Preference, StaticUtility.strBillingAddress1);
                                        strLandmark = SharedPreference.GetPreference(context, Global.Billing_Preference, StaticUtility.strBillingLandmark);
                                        strCountry = SharedPreference.GetPreference(context, Global.Billing_Preference, StaticUtility.strBillingCountry);
                                        strState = SharedPreference.GetPreference(context, Global.Billing_Preference, StaticUtility.strBillingState);
                                        strCity = SharedPreference.GetPreference(context, Global.Billing_Preference, StaticUtility.strBillingCity);
                                        strPhoneno = SharedPreference.GetPreference(context, Global.Billing_Preference, StaticUtility.strBillingPhoneno);
                                    }
                                    AddressList addressList = addressLists.get(position);
                                    if (addressList.getFirstName().equalsIgnoreCase(strFname) && addressList.getLastName().equalsIgnoreCase(strLname) &&
                                            addressList.getAddress().equalsIgnoreCase(strAddress) && addressList.getLandmark().equalsIgnoreCase(strLandmark) &&
                                            addressList.getPincode().equalsIgnoreCase(strPincode) && addressList.getCountry().equalsIgnoreCase(strCountry) &&
                                            addressList.getState().equalsIgnoreCase(strState) && addressList.getCity().equalsIgnoreCase(strCity) &&
                                            addressList.getPhone_number().equalsIgnoreCase(strPhoneno)) {
                                        if (addressType.equalsIgnoreCase("0")) {
                                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                        } else {
                                            CheckoutFragment.isBillingAddress = true;
                                            CheckoutFragment.isBillingAddressChange = false;
                                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                        }
                                    }
                                    addressLists.remove(position);
                                    recyclerviewAddress.getAdapter().notifyDataSetChanged();
                                    if (addressLists.size() <= 0) {
                                        txtNoAddressAvailable.setVisibility(View.VISIBLE);
                                        if (addressType.equalsIgnoreCase("0")) {
                                            SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                        } else {
                                            CheckoutFragment.isBillingAddress = true;
                                            SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                        }
                                        isRefresh = true;
                                        Intent intent = new Intent(context, CheckoutAddAddressActivity.class);
                                        intent.putExtra("ActivityType", ActivityType);
                                        intent.putExtra("AddressType", addressType);
                                        intent.putExtra("Type", "AddressListing");
                                        startActivity(intent);
                                    } else {
                                        txtNoAddressAvailable.setVisibility(View.GONE);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in AddressListFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    public static void EditAddresses(String Fname, String Lname, String Address, String Landmark, String Pincode, String Country,
                                     String State, String City, String Phoneno, String addressType, String useradress_id, String Position) {
        AddressList addressList = addressLists.get(Integer.parseInt(Position));
        addressList.setFirstName(Fname);
        addressList.setLastName(Lname);
        addressList.setAddress(Address);
        addressList.setLandmark(Landmark);
        addressList.setPincode(Pincode);
        addressList.setCountry(Country);
        addressList.setState(State);
        addressList.setCity(City);
        addressList.setPhone_number(Phoneno);
        recyclerviewAddress.getAdapter().notifyDataSetChanged();
    }

    public String Catpital(String strText) {
        String cap = "";
        String[] strArray = strText.split(" ");
        for (String s : strArray) {
            cap = s.substring(0, 1).toUpperCase() + s.substring(1);
        }
        return cap;
    }

    //region chanageButton
    public void chanageButton(Button button) {
        button.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(70);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = row.findViewById(R.id.tvTitle);
        final Button btnSettings = row.findViewById(R.id.btnSettings);
        final Button btnExit = row.findViewById(R.id.btnExit);

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF).equals("")) {
                tvTitle.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF));
            } else {
                tvTitle.setText(getText(R.string.your_data_is_off));
            }
        } else {
            tvTitle.setText(getText(R.string.your_data_is_off));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI).equals("")) {
                tvAlertText.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI));
            } else {
                tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
            }
        } else {
            tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS).equals("")) {
                btnSettings.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS));
            } else {
                btnSettings.setText(getText(R.string.settings));
            }
        } else {
            btnSettings.setText(getText(R.string.settings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT).equals("")) {
                btnExit.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT));
            } else {
                btnExit.setText(getText(R.string.exit));
            }
        } else {
            btnExit.setText(getText(R.string.exit));
        }


        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion
}
