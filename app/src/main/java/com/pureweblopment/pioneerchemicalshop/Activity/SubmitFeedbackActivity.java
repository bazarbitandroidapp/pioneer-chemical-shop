package com.pureweblopment.pioneerchemicalshop.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.SendMail;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;
import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.pureweblopment.pioneerchemicalshop.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class SubmitFeedbackActivity extends AppCompatActivity implements View.OnClickListener {

    Context context = SubmitFeedbackActivity.this;

    ImageView imageCartBack;
    TextView txtCatName;

    RecyclerView recyclerviewFeedbackform;
    RelativeLayout relativeProgress;
    Button btn_feedback_submit;
    String question[];
    String answer[];
    Bundle bundle = new Bundle();
    String orderitem_id = "", product_id = "";
    int i = 0;

    //Internet Alert
    public EventBus eventBus = EventBus.getDefault();
    public static int j = 0;
    public static AlertDialog internetAlert;

    FrameLayout flToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_submit_feedback);

        eventBus.register(this);

        orderitem_id = getIntent().getStringExtra("orderitem_id");
        product_id = getIntent().getStringExtra("product_id");

        ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        Initialization();
        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onBackPressed();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        getFeedbackForm();

        changeButton(btn_feedback_submit);
    }

    //region Initialization
    private void Initialization() {
        imageCartBack = findViewById(R.id.imageCartBack);
        txtCatName = findViewById(R.id.txtCatName);
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFEEDBACK) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFEEDBACK).equals("")) {
                txtCatName.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFEEDBACK));
            } else {
                txtCatName.setText(R.string.feedback);
            }
        } else {
            txtCatName.setText(R.string.feedback);
        }
        recyclerviewFeedbackform = findViewById(R.id.recyclerviewFeedbackform);
        relativeProgress = findViewById(R.id.relativeProgress);
        btn_feedback_submit = findViewById(R.id.btn_feedback_submit);
        flToolbar = findViewById(R.id.flToolbar);
        btn_feedback_submit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtCatName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btn_feedback_submit.setOnClickListener(this);
        flToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_feedback_submit:
                int length = 0;
                for (int i = 0; i < answer.length; i++) {
                    if (answer[i] != null && !answer[i].equalsIgnoreCase("")) {
                        length++;
                    }
                }
                if (question.length == length) {
                    SubmitFeedback();
                } else {
                    if(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sANS_CHECK_EMPTY)!=null){
                        if(!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sANS_CHECK_EMPTY).equals("")) {
                            Toast.makeText(context, SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sANS_CHECK_EMPTY), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, getString(R.string.feedback_error_message), Toast.LENGTH_LONG).show();
                        }
                    }else {
                        Toast.makeText(context, getString(R.string.feedback_error_message), Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    //region FOR getFeedbackForm...
    private void getFeedbackForm() {
        relativeProgress.setVisibility(View.VISIBLE);
        String[] key = {};
        String[] val = {};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetFeedbackForm);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    JSONArray jsonArray = response.getJSONArray("payload");
                                    if (jsonArray.length() > 0) {
                                        recyclerviewFeedbackform.setVisibility(View.VISIBLE);
                                        btn_feedback_submit.setVisibility(View.VISIBLE);
                                        recyclerviewFeedbackform.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                                        AdapterFeedback adapterFeedback = new AdapterFeedback(context, jsonArray);
                                        recyclerviewFeedbackform.setAdapter(adapterFeedback);
                                        recyclerviewFeedbackform.setHasFixedSize(true);
                                    } else {
                                        recyclerviewFeedbackform.setVisibility(View.GONE);
                                        btn_feedback_submit.setVisibility(View.GONE);
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                if (strMessage.equalsIgnoreCase("Invalid Credentials")) {
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in SubmitFeedbackActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ADAPTER
    public class AdapterFeedback extends RecyclerView.Adapter<AdapterFeedback.Viewholder> {

        Context context;
        JSONArray jsonArray;

        public AdapterFeedback(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
            question = new String[this.jsonArray.length()];
            answer = new String[this.jsonArray.length()];
        }

        @Override
        public AdapterFeedback.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_feedback_form, viewGroup, false);
            return new AdapterFeedback.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final AdapterFeedback.Viewholder holder, final int position) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(position);
//                feedbackid = jsonObject.getString("feedbackformid_id");
                question[position] = jsonObject.getString("question");
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sQ) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sQ).equals("")) {
                        holder.txtquestion.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sQ)
                                + (position + 1) + " " + jsonObject.getString("question"));
                    }else {
                        holder.txtquestion.setText(getString(R.string.q) + (position + 1) + " " + jsonObject.getString("question"));
                    }
                } else {
                    holder.txtquestion.setText(getString(R.string.q) + (position + 1) + " " + jsonObject.getString("question"));
                }

                if (jsonObject.getString("ans_type").equals("radio")) {
                    holder.radioGroupfeedback.setVisibility(View.VISIBLE);
                    JSONObject ans = jsonObject.getJSONObject("ans_values");
                    //ans = ans.replace("\"","");

                    holder.option_1.setText(ans.getString("option_1"));
                    holder.option_2.setText(ans.getString("option_2"));
                    holder.option_3.setText(ans.getString("option_3"));
                    holder.option_4.setText(ans.getString("option_4"));
                    holder.radioGroupfeedback.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @SuppressLint("ResourceType")
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            RadioButton rb = (RadioButton) group.findViewById(checkedId);
                            if (null != rb && checkedId > -1) {
                                answer[position] = rb.getText().toString();

                            }
                        }
                    });
                }
                if (jsonObject.getString("ans_type").equals("textarea")) {
                    holder.llAnsTextarea.setVisibility(View.VISIBLE);
                    i++;
                }
                if (jsonObject.getString("ans_type").equals("text")) {
                    holder.llAnsText.setVisibility(View.VISIBLE);
                    i++;
                }


                holder.edtAnsText.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        answer[position] = holder.edtAnsText.getText().toString();
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
                holder.edtTextArea.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        answer[position] = holder.edtTextArea.getText().toString();
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });


            } catch (JSONException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in AdapterFeedback.java When parsing response.\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();

            }
        }


        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            TextView txtquestion, txtAns;
            RadioGroup radioGroupfeedback;
            EditText edtAnsText, edtTextArea;
            LinearLayout llAnsText, llAnsTextarea;
            RadioButton option_1, option_2, option_3, option_4;

            public Viewholder(View itemView) {
                super(itemView);
                txtquestion = itemView.findViewById(R.id.txtquestion);
                txtAns = itemView.findViewById(R.id.txtAns);
                radioGroupfeedback = itemView.findViewById(R.id.radioGroupfeedback);
                edtAnsText = itemView.findViewById(R.id.edtAnsText);
                edtTextArea = itemView.findViewById(R.id.edtTextArea);
                llAnsText = itemView.findViewById(R.id.llAnsText);
                llAnsTextarea = itemView.findViewById(R.id.llAnsTextarea);
                option_1 = itemView.findViewById(R.id.option_1);
                option_2 = itemView.findViewById(R.id.option_2);
                option_3 = itemView.findViewById(R.id.option_3);
                option_4 = itemView.findViewById(R.id.option_4);

                txtquestion.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtAns.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sANS) != null) {
                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sANS).equals("")) {
                    txtAns.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sANS));
                }else {
                    txtAns.setText(context.getString(R.string.ans));
                }
                } else {
                    txtAns.setText(context.getString(R.string.ans));
                }

                /*chanageEditTextBorder(edtAnsText);
                chanageEditTextBorder(edtTextArea);*/

                /*edtAnsText.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                edtTextArea.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));*/
            }
        }
    }
    //endregion

    //region FOR SubmitFeedback...
    private void SubmitFeedback() {
        relativeProgress.setVisibility(View.VISIBLE);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < question.length; i++) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("question", question[i]);
                obj.put("answer", answer[i]);

                jsonArray.put(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        try {
            jsonObject.put("z_productid_fk", product_id);
            jsonObject.put("z_orderitemid_fk", orderitem_id);
            jsonObject.put("questions", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.SubmitFeedback);
        postRequestBuilder.addJSONObjectBody(jsonObject)
                .addHeaders(Global.headers1(context));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    MainActivity.manageBackPress(true);
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in SubmitFeedbackActivity.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }

    //endregion

    //region chanageButton
    public void changeButton(Button button) {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            button.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region chanageEditTextBorder
    public void changeEditTextBorder(EditText editText) {
        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
            editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = row.findViewById(R.id.tvTitle);
        final Button btnSettings = row.findViewById(R.id.btnSettings);
        final Button btnExit = row.findViewById(R.id.btnExit);

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF).equals("")) {
                tvTitle.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF));
            } else {
                tvTitle.setText(getText(R.string.your_data_is_off));
            }
        } else {
            tvTitle.setText(getText(R.string.your_data_is_off));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI).equals("")) {
                tvAlertText.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI));
            } else {
                tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
            }
        } else {
            tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS).equals("")) {
                btnSettings.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS));
            } else {
                btnSettings.setText(getText(R.string.settings));
            }
        } else {
            btnSettings.setText(getText(R.string.settings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT).equals("")) {
                btnExit.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT));
            } else {
                btnExit.setText(getText(R.string.exit));
            }
        } else {
            btnExit.setText(getText(R.string.exit));
        }

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (j == 0) {
                    j = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                j = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion
}
