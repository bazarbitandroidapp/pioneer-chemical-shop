package com.pureweblopment.pioneerchemicalshop.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.pureweblopment.pioneerchemicalshop.Activity.LoginActivity;
import com.pureweblopment.pioneerchemicalshop.Activity.MainActivity;
import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.Executors;

import com.pureweblopment.pioneerchemicalshop.Global.SendMail;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;

import com.pureweblopment.pioneerchemicalshop.R;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrderDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderDetailFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    Bundle bundle;

    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    LinearLayout llBottomNavigation, ll_wallet_amount;

    RelativeLayout relativeProgress;

    TextView txtOrderDateTitle, txtOrderDate, txtOrderIDTitle, txtOrderID, txtOrderTotalTitle, txtOrderTotal;
    TextView txtShippingAddressTitle, txtShippingUserName, txtShippingAddress, txtShippingPhoneNo,
            txtBillingAddressTitle, txtBillingUserName, txtBillingAddress, txtBillingPhoneNo;
    TextView txtDiscountTitle, txtDiscount, txtShippingChargesTitle, txtShippingCharges,
            txtCODChargesTitle, txtCODCharges, txtTotalTitle, txtTotal, txtItemTotal, txtItemTotalTitle,
            txtPaymentInfoTitle, txtPaymentMethod, txtwalletTitle, txtwalletTotal;
    Boolean clickable = true;
    String itemorderid, product_id;
    int paymentType, intOrderStatus, intDeliveryDays;
    private AlertDialog ReturnCOD, ReturnOnline, Reviews;

    CardView cardviewBottomNavigation;

    RecyclerView recyclerviewOrderHistoryProduct;
    String selectradio;

    String order_datetime, redeem_wallet_amount;
    String slug;
    LinearLayout llToolbar, llDiscount, llShippingCharges, llCODCharges;
    int intOrderPaymentStatus = 0;

    public OrderDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrderDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrderDetailFragment newInstance(String param1, String param2) {
        OrderDetailFragment fragment = new OrderDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_detail, container, false);

        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        Initialization(view);
        TypeFace();
        AppSettings();
        setDynamicString();

        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_DETAIL) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_DETAIL).equals("")) {
                txtCatName.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_DETAIL));
            } else {
                txtCatName.setText(R.string.orderDetail);
            }
        }
        txtCatName.setText(R.string.orderDetail);

        imageCartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

        bundle = getArguments();
        String slug = bundle.getString("Order_Id");
        redeem_wallet_amount = bundle.getString("redeem_wallet_amount");



        SingleOrderDetail(slug);

        return view;
    }

    //region Initialization....
    private void Initialization(View view) {
        relativeProgress = (RelativeLayout) view.findViewById(R.id.relativeProgress);
        txtOrderDateTitle = view.findViewById(R.id.txtOrderDateTitle);
        txtOrderDate = view.findViewById(R.id.txtOrderDate);
        txtOrderIDTitle = view.findViewById(R.id.txtOrderIDTitle);
        txtOrderID = view.findViewById(R.id.txtOrderID);
        txtOrderTotalTitle = view.findViewById(R.id.txtOrderTotalTitle);
        txtOrderTotal = view.findViewById(R.id.txtOrderTotal);
        txtShippingAddressTitle = view.findViewById(R.id.txtShippingAddressTitle);
        txtShippingUserName = view.findViewById(R.id.txtShippingUserName);
        txtShippingAddress = view.findViewById(R.id.txtShippingAddress);
        txtShippingPhoneNo = view.findViewById(R.id.txtShippingPhoneNo);
        txtBillingAddressTitle = view.findViewById(R.id.txtBillingAddressTitle);
        txtBillingUserName = view.findViewById(R.id.txtBillingUserName);
        txtBillingAddress = view.findViewById(R.id.txtBillingAddress);
        txtBillingPhoneNo = view.findViewById(R.id.txtBillingPhoneNo);

        txtDiscountTitle = view.findViewById(R.id.txtDiscountTitle);
        txtDiscount = view.findViewById(R.id.txtDiscount);
        txtShippingChargesTitle = view.findViewById(R.id.txtShippingChargesTitle);
        txtShippingCharges = view.findViewById(R.id.txtShippingCharges);
        txtCODChargesTitle = view.findViewById(R.id.txtCODChargesTitle);
        txtCODCharges = view.findViewById(R.id.txtCODCharges);
        txtTotalTitle = view.findViewById(R.id.txtTotalTitle);
        txtTotal = view.findViewById(R.id.txtTotal);
        txtItemTotalTitle = view.findViewById(R.id.txtItemTotalTitle);
        txtItemTotal = view.findViewById(R.id.txtItemTotal);
        txtPaymentInfoTitle = view.findViewById(R.id.txtPaymentInfoTitle);
        txtPaymentMethod = view.findViewById(R.id.txtPaymentMethod);

        llToolbar = view.findViewById(R.id.llToolbar);

        txtCatName = view.findViewById(R.id.txtCatName);
        imageCartBack = view.findViewById(R.id.imageCartBack);

        recyclerviewOrderHistoryProduct = view.findViewById(R.id.recyclerviewOrderHistoryProduct);
        llDiscount = view.findViewById(R.id.llDiscount);
        llShippingCharges = view.findViewById(R.id.llShippingCharges);
        llCODCharges = view.findViewById(R.id.llCODCharges);

        ll_wallet_amount = view.findViewById(R.id.ll_wallet_amount);
        txtwalletTitle = view.findViewById(R.id.txtwalletTitle);
        txtwalletTotal = view.findViewById(R.id.txtwalletTotal);
    }
    //endregion

    //region TypeFace...
    private void TypeFace() {
        txtOrderTotal.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderTotalTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderID.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderIDTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderDate.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtOrderDateTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));

        txtBillingPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtBillingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtBillingUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtBillingAddressTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingAddressTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));

        txtTotal.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtTotalTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtCODCharges.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtCODChargesTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingCharges.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtShippingChargesTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtDiscount.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtDiscountTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtItemTotal.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtItemTotalTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtPaymentInfoTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtPaymentMethod.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtwalletTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtwalletTotal.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
    }
    //endregions

    //region AppSettings....
    private void AppSettings() {
        txtwalletTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtOrderTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtOrderID.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtOrderDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtOrderDateTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtOrderIDTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtOrderTotalTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtwalletTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtShippingAddressTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtBillingAddressTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtBillingUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtShippingUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtShippingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtShippingPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtBillingAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtBillingPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));

        txtTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtTotalTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtCODCharges.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtCODChargesTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtShippingCharges.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtShippingChargesTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtDiscount.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtDiscountTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtItemTotalTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
        txtItemTotal.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtPaymentInfoTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtPaymentMethod.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
    }
    //endregion

    //region setDynamicString
    private void setDynamicString() {
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_DATE) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_DATE).equals("")) {
                txtOrderDateTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_DATE));
            } else {
                txtOrderDateTitle.setText(R.string.order_date);
            }
        } else {
            txtOrderDateTitle.setText(R.string.order_date);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID).equals("")) {
                txtOrderIDTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID));
            } else {
                txtOrderIDTitle.setText(R.string.order_id);
            }
        } else {
            txtOrderIDTitle.setText(R.string.order_id);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sredeem_from_wallet) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sredeem_from_wallet).equals("")) {
                txtwalletTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sredeem_from_wallet));
            } else {
                txtwalletTitle.setText(R.string.redeem_from_wallet);
            }
        } else {
            txtwalletTitle.setText(R.string.redeem_from_wallet);
        }

        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_TOTAL) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_TOTAL).equals("")) {
                txtOrderTotalTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_TOTAL));
            } else {
                txtOrderTotalTitle.setText(R.string.order_total);
            }
        } else {
            txtOrderTotalTitle.setText(R.string.order_total);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSHIPPING_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSHIPPING_ADDRESS).equals("")) {
                txtShippingAddressTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSHIPPING_ADDRESS));
            } else {
                txtShippingAddressTitle.setText(R.string.shipping_address);
            }
        } else {
            txtShippingAddressTitle.setText(R.string.shipping_address);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBILLING_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBILLING_ADDRESS).equals("")) {
                txtBillingAddressTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBILLING_ADDRESS));
            } else {
                txtBillingAddressTitle.setText(R.string.billing_address);
            }
        } else {
            txtBillingAddressTitle.setText(R.string.billing_address);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT_INFORMATION) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT_INFORMATION).equals("")) {
                txtPaymentInfoTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT_INFORMATION));
            } else {
                txtPaymentInfoTitle.setText(R.string.payment_information);
            }
        } else {
            txtPaymentInfoTitle.setText(R.string.payment_information);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sITEM_TOTAL) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sITEM_TOTAL).equals("")) {
                txtItemTotalTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sITEM_TOTAL));
            } else {
                txtItemTotalTitle.setText(R.string.item_total);
            }
        } else {
            txtItemTotalTitle.setText(R.string.item_total);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDISCOUNT) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDISCOUNT).equals("")) {
                txtDiscountTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDISCOUNT));
            } else {
                txtDiscountTitle.setText(R.string.discount);
            }
        } else {
            txtDiscountTitle.setText(R.string.discount);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSHIPPING_CHARGES) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSHIPPING_CHARGES).equals("")) {
                txtShippingChargesTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSHIPPING_CHARGES));
            } else {
                txtShippingChargesTitle.setText(R.string.shipping_charges);
            }
        } else {
            txtShippingChargesTitle.setText(R.string.shipping_charges);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOD_CHARGES) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOD_CHARGES).equals("")) {
                txtCODChargesTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOD_CHARGES));
            } else {
                txtCODChargesTitle.setText(R.string.cod_charges);
            }
        } else {
            txtCODChargesTitle.setText(R.string.cod_charges);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTOTAL) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTOTAL).equals("")) {
                txtTotalTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTOTAL));
            } else {
                txtTotalTitle.setText(R.string.total);
            }
        } else {
            txtTotalTitle.setText(R.string.total);
        }
    }//endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //region FOR SingleOrderDetail...
    private void SingleOrderDetail(String slug) {
        relativeProgress.setVisibility(View.VISIBLE);
        String strCurrencyCode = SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY, StaticUtility.sCurrencyCode);
        String[] key = {"slug", "currencyCode"};
        String[] val = {slug, strCurrencyCode};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.SingleOrderDetail);
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(getContext()));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(getContext()));
        }
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    JSONArray jsonArrayPayload = response.getJSONArray("payload");
                                    JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(0);
                                    txtOrderDate.setText(Global.changeDateFormate(jsonObjectPayload.getString("order_datetime"),
                                            "yyyy-MM-dd hh:mm:ss", "dd-MMM-yyyy"));
                                    txtOrderID.setText(jsonObjectPayload.getString("order_id"));
                                    order_datetime = jsonObjectPayload.getString("order_datetime");
                                    intOrderPaymentStatus = Integer.parseInt(jsonObjectPayload.getString("order_payment_status"));

                                    String strPrice = jsonObjectPayload.getString("order_product_amount");
                                    if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                            StaticUtility.sCurrencySignPosition) != null) {
                                        if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                StaticUtility.sCurrencySignPosition).equals("1")) {

                                        } else if (SharedPreference.GetPreference(getContext(), Global.PREFERENCECURRENCY,
                                                StaticUtility.sCurrencySignPosition).equals("0")) {

                                        }
                                    } else {
                                        txtOrderTotal.setText(strPrice);
                                    }
                                    String product_discount = jsonObjectPayload.getString("order_total_discount");
                                    redeem_wallet_amount = jsonObjectPayload.getString("redeem_wallet_amount");
                                    String shipping_tax = jsonObjectPayload.getString("order_shipping_amount");
                                    String shipping_price_cod = jsonObjectPayload.getString("order_cod_amount");
                                    String total_price = jsonObjectPayload.getString("order_amount");
                                    String item_price = jsonObjectPayload.getString("order_product_amount");
                                    String order_payment_type_display = jsonObjectPayload.getString("order_payment_type_display");
                                    String currency_symbol = jsonObjectPayload.getString("currency_symbol");
                                    String currency_place = jsonObjectPayload.getString("currency_place");
                                    txtPaymentMethod.setText("(" + order_payment_type_display + ")");

                                    if (product_discount.equalsIgnoreCase("0.00")) {
                                        llDiscount.setVisibility(View.GONE);
                                    } else {
                                        llDiscount.setVisibility(View.VISIBLE);
                                    }
                                    if (shipping_tax.equalsIgnoreCase("0.00")) {
                                        llShippingCharges.setVisibility(View.GONE);
                                    } else {
                                        llShippingCharges.setVisibility(View.VISIBLE);
                                    }
                                    if (shipping_price_cod.equalsIgnoreCase("0.00")) {
                                        llCODCharges.setVisibility(View.GONE);
                                    } else {
                                        llCODCharges.setVisibility(View.VISIBLE);
                                    }

                                    if (currency_place.equals("1")) {
                                        if (redeem_wallet_amount != null && Double.valueOf(redeem_wallet_amount) > 0) {
                                            txtwalletTotal.setText(String.format("%s %s", currency_symbol, redeem_wallet_amount));
                                            ll_wallet_amount.setVisibility(View.VISIBLE);
                                        }
                                        txtOrderTotal.setText(String.format("%s %s", currency_symbol, strPrice));
                                        txtDiscount.setText(String.format("%s %s", currency_symbol, product_discount));
                                        txtShippingCharges.setText(String.format("%s %s", currency_symbol, shipping_tax));
                                        txtCODCharges.setText(String.format("%s %s", currency_symbol, shipping_price_cod));
                                        txtTotal.setText(String.format("%s %s", currency_symbol, total_price));
                                        txtItemTotal.setText(String.format("%s %s", currency_symbol, item_price));
                                    } else if(currency_place.equals("0")){
                                        if (redeem_wallet_amount != null && Double.valueOf(redeem_wallet_amount) > 0) {
                                            txtwalletTotal.setText(String.format("%s %s", redeem_wallet_amount, currency_symbol));
                                            ll_wallet_amount.setVisibility(View.VISIBLE);
                                        }
                                        txtOrderTotal.setText(String.format("%s %s", strPrice, currency_symbol));
                                        txtDiscount.setText(String.format("%s %s", product_discount, currency_symbol));
                                        txtShippingCharges.setText(String.format("%s %s", shipping_tax, currency_symbol));
                                        txtCODCharges.setText(String.format("%s %s", shipping_price_cod, currency_symbol));
                                        txtTotal.setText(String.format("%s %s", total_price, currency_symbol));
                                        txtItemTotal.setText(String.format("%s %s", item_price, currency_symbol));
                                    }else {
                                        txtOrderTotal.setText(strPrice);
                                        txtDiscount.setText(product_discount);
                                        txtShippingCharges.setText(shipping_tax);
                                        txtCODCharges.setText(shipping_price_cod);
                                        txtTotal.setText(total_price);
                                        txtItemTotal.setText(item_price);
                                    }

                                    paymentType = Integer.parseInt(jsonObjectPayload.getString("order_payment_type"));

                                    JSONArray jsonArrayOrderDetail = jsonObjectPayload.getJSONArray("orderitems");
                                    for (int i = 0; i < jsonArrayOrderDetail.length(); i++) {
                                        JSONObject jsonObjectOrderDetail = jsonArrayOrderDetail.getJSONObject(i);
                                        String fullname = jsonObjectOrderDetail.getString("fullname");
                                        String phone_number = jsonObjectOrderDetail.getString("phone_number");
                                        String address = jsonObjectOrderDetail.getString("address");
                                        String state = jsonObjectOrderDetail.getString("state");
                                        String city = jsonObjectOrderDetail.getString("city");
                                        String pincode = jsonObjectOrderDetail.getString("pincode");
                                        String billing_fullname = jsonObjectOrderDetail.getString("billing_fullname");
                                        String billing_phonenumber = jsonObjectOrderDetail.getString("billing_phonenumber");
                                        String billing_address = jsonObjectOrderDetail.getString("billing_address");
                                        String billing_state = jsonObjectOrderDetail.getString("billing_state");
                                        String billing_city = jsonObjectOrderDetail.getString("billing_city");
                                        String billing_pincode = jsonObjectOrderDetail.getString("billing_pincode");

                                        txtShippingUserName.setText(fullname);
                                        txtShippingAddress.setText(address + "\n" + city + " - " + pincode + "," + "\n" + state + ",");
                                        txtShippingPhoneNo.setText(phone_number);

                                        if (!billing_fullname.equals("")) {
                                            txtBillingUserName.setText(billing_fullname);
                                        }
                                        if (!billing_phonenumber.equals("")) {
                                            txtBillingPhoneNo.setText(billing_phonenumber);
                                        }
                                        if (!billing_address.equals("")) {
                                            txtBillingAddress.setText(billing_address + "\n" + billing_city + " - " + billing_pincode + "," + "\n" + billing_state + ",");
                                        }
                                    }
                                    AdapterOrderHistoryProduct adapterOrderHistoryProduct = new AdapterOrderHistoryProduct(getContext(), jsonArrayOrderDetail);
                                    recyclerviewOrderHistoryProduct.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                    recyclerviewOrderHistoryProduct.setAdapter(adapterOrderHistoryProduct);
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region AdapterOrderHistoryProduct
    public class AdapterOrderHistoryProduct extends RecyclerView.Adapter<AdapterOrderHistoryProduct.Viewholder> {

        Context context;
        JSONArray jsonArray;

        public AdapterOrderHistoryProduct(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        @Override
        public AdapterOrderHistoryProduct.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_orderhistory_product, viewGroup, false);
            return new AdapterOrderHistoryProduct.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(final Viewholder holder, final int position) {
            try {
                if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                    }
                }
                final JSONObject jsonObjectOrderDetail = jsonArray.getJSONObject(position);
                String name = jsonObjectOrderDetail.getString("name");
                String quantity = jsonObjectOrderDetail.getString("quantity");
                slug = jsonObjectOrderDetail.getString("slug");
                holder.txtOrderDetailProductName.setText(name);
                holder.txtProducDate.setText(Global.changeDateFormate(order_datetime, "yyyy-MM-dd hh:mm:ss", "dd-MMM-yyyy"));
                holder.txtOrderDetailQty.setText(quantity);
                String reviewdetail_id = jsonObjectOrderDetail.getString("reviewdetail_id");

                float floatOrderPrice = Float.parseFloat(jsonObjectOrderDetail.getString("price"));
                int intOrderPrice = Math.round(floatOrderPrice);
                if (jsonObjectOrderDetail.optString("currency_place").equals("1")) {
                    holder.txtProductPrice.setText(jsonObjectOrderDetail.optString("currency_symbol") + " " + jsonObjectOrderDetail.getString("price"));
                } else if (jsonObjectOrderDetail.optString("currency_place").equals("0")) {
                    holder.txtProductPrice.setText(jsonObjectOrderDetail.getString("price") + " " + jsonObjectOrderDetail.optString("currency_symbol"));
                }

                int intReturn_days = Integer.parseInt(jsonObjectOrderDetail.getString("return_days"));
                String order_item_status = jsonObjectOrderDetail.getString("order_item_status");
                intOrderStatus = Integer.parseInt(jsonObjectOrderDetail.getString("order_item_status"));
                String DeliveryDays = jsonObjectOrderDetail.getString("DeliveryDays");
                if (!DeliveryDays.equals("")) {
                    intDeliveryDays = Integer.parseInt(DeliveryDays);
                } else {
                    intDeliveryDays = 0;
                }

                if (reviewdetail_id.equalsIgnoreCase("")) {
                    holder.txtAddReviews.setVisibility(View.VISIBLE);
                    holder.llOrderStatus.setVisibility(View.VISIBLE);
                } else {
                    holder.txtAddReviews.setVisibility(View.GONE);
                    holder.llOrderStatus.setVisibility(View.GONE);
                }

                if (intOrderStatus >= 0) {
                    if (paymentType > 1 && intOrderStatus != 1) {
                        if (intOrderStatus == 0 && intOrderPaymentStatus == 0) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER).equals("")) {
                                    holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER));
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.cancelled_order));
                                }
                            } else {
                                holder.txtProductReturn.setText(getString(R.string.cancelled_order));
                            }
                            holder.txtProductReturn.setEnabled(false);
                            clickable = false;
                        } else if (intOrderStatus == 5) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER).equals("")) {
                                    holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER));
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.cancelled_order));
                                }
                            } else {
                                holder.txtProductReturn.setText(getString(R.string.cancelled_order));
                            }
                            holder.txtProductReturn.setEnabled(false);
                            clickable = false;
                        } else if (intOrderStatus == 4) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (intReturn_days > 0 && intDeliveryDays <= intReturn_days) {
                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER) != null) {
                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER).equals("")) {
                                        holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER));
                                    } else {
                                        holder.txtProductReturn.setText(getString(R.string.return_order));
                                    }
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.return_order));
                                }
                                holder.txtProductReturn.setClickable(true);
                            } else {
                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERED) != null) {
                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERED).equals("")) {
                                        holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERED));
                                    } else {
                                        holder.txtProductReturn.setText(getString(R.string.delivered));
                                    }
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.delivered));
                                }
                                holder.txtProductReturn.setEnabled(false);
                            }
                        } else if (intOrderStatus == 6) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER).equals("")) {
                                    holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER));
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.returned_order));
                                }
                            } else {
                                holder.txtProductReturn.setText(getString(R.string.returned_order));
                            }
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 7) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREFUND_ORDER) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREFUND_ORDER).equals("")) {
                                    holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREFUND_ORDER));
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.refund_order));
                                }
                            } else {
                                holder.txtProductReturn.setText(getString(R.string.refund_order));
                            }
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 3) {
                            holder.txtProductReturn.setVisibility(View.GONE);
                        } else {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL).equals("")) {
                                    holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL));
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.cancel));
                                }
                            } else {
                                holder.txtProductReturn.setText(getString(R.string.cancel));
                            }
                            holder.txtProductReturn.setEnabled(true);
                            clickable = true;
                        }
                    } else {
                        if (intOrderStatus == 5) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER).equals("")) {
                                    holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER));
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.cancelled_order));
                                }
                            } else {
                                holder.txtProductReturn.setText(getString(R.string.cancelled_order));
                            }
                            holder.txtProductReturn.setEnabled(false);
                            clickable = false;
                        } else if (intOrderStatus == 4) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (intReturn_days > 0 && intDeliveryDays <= intReturn_days) {
                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER) != null) {
                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER).equals("")) {
                                        holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER));
                                    } else {
                                        holder.txtProductReturn.setText(getString(R.string.return_order));
                                    }
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.return_order));
                                }
                                holder.txtProductReturn.setClickable(true);
                            } else {
                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERED) != null) {
                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERED).equals("")) {
                                        holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERED));
                                    } else {
                                        holder.txtProductReturn.setText(getString(R.string.delivered));
                                    }
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.delivered));
                                }
                                holder.txtProductReturn.setEnabled(false);
                            }
                        } else if (intOrderStatus == 6) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER).equals("")) {
                                    holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_ORDER));
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.returned_order));
                                }
                            } else {
                                holder.txtProductReturn.setText(getString(R.string.returned_order));
                            }
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 7) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREFUND_ORDER) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREFUND_ORDER).equals("")) {
                                    holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREFUND_ORDER));
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.refund_order));
                                }
                            } else {
                                holder.txtProductReturn.setText(getString(R.string.refund_order));
                            }
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 3) {
                            holder.txtProductReturn.setVisibility(View.GONE);
                        } else {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL).equals("")) {
                                    holder.txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL));
                                } else {
                                    holder.txtProductReturn.setText(getString(R.string.cancel));
                                }
                            } else {
                                holder.txtProductReturn.setText(getString(R.string.cancel));
                            }
                            holder.txtProductReturn.setEnabled(true);
                            clickable = true;
                        }
                    }

                    /*if (paymentType > 1 && intOrderStatus != 1) {
                        if (intOrderStatus == 5) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText(getString(R.string.cancelled_order));
                            holder.txtProductReturn.setEnabled(false);
                            clickable = false;
                        } else if (intOrderStatus == 4) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (intReturn_days > 0 && intDeliveryDays <= intReturn_days) {
                                holder.txtProductReturn.setText(getString);
                                holder.txtProductReturn.setClickable(true);
                            } else {
                                holder.txtProductReturn.setText("order delivered");
                                holder.txtProductReturn.setEnabled(false);
                            }
                        } else if (intOrderStatus == 6) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order returned");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 7) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order refund");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 3) {
                            holder.txtProductReturn.setVisibility(View.GONE);
                        } else {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("cancel");
                            holder.txtProductReturn.setEnabled(true);
                            clickable = true;
                        }
                    } else {
                        if (intOrderStatus == 5) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText(getString(R.string.cancelled_order));
                            holder.txtProductReturn.setEnabled(false);
                            clickable = false;
                        } else if (intOrderStatus == 4) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            if (intReturn_days > 0 && intDeliveryDays <= intReturn_days) {
                                holder.txtProductReturn.setText(getString);
                                holder.txtProductReturn.setClickable(true);
                            } else {
                                holder.txtProductReturn.setText("order delivered");
                                holder.txtProductReturn.setEnabled(false);
                            }
                        } else if (intOrderStatus == 6) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order returned");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 7) {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("order refund");
                            holder.txtProductReturn.setEnabled(false);
                        } else if (intOrderStatus == 3) {
                            holder.txtProductReturn.setVisibility(View.GONE);
                        } else {
                            holder.llOrderStatus.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setVisibility(View.VISIBLE);
                            holder.txtProductReturn.setText("cancel");
                            holder.txtProductReturn.setEnabled(true);
                            clickable = true;
                        }
                    }*/
                }

                itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                product_id = jsonObjectOrderDetail.getString("product_id");

                holder.txtProductReturn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (holder.txtProductReturn.getText().toString().
                                equalsIgnoreCase("cancel")) {
                            if (clickable) {
                                String strYes = "", strNo = "";
                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sYES) != null) {
                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sYES).equals("")) {
                                        strYes = SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sYES);
                                    } else {
                                        strYes = getString(R.string.yes);
                                    }
                                } else {
                                    strYes = getString(R.string.yes);
                                }
                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO) != null) {
                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO).equals("")) {
                                        strNo = SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO);
                                    } else {
                                        strNo = getString(R.string.no);
                                    }
                                } else {
                                    strNo = getString(R.string.no);
                                }
                                try {
                                    final String itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSURE_CANCEL_THIS_ORDER) != null) {
                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSURE_CANCEL_THIS_ORDER).equals("")) {
                                            builder1.setMessage(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSURE_CANCEL_THIS_ORDER));
                                        } else {
                                            builder1.setMessage(getString(R.string.cancel_order_alert));
                                        }
                                    } else {
                                        builder1.setMessage(getString(R.string.cancel_order_alert));
                                    }
                                    builder1.setCancelable(true);

                                    builder1.setPositiveButton(strYes,
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    if (paymentType > 1) {
                                                        try {
                                                            String itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                                                            String product_id = jsonObjectOrderDetail.getString("product_id");
                                                            OnlineOrderCancelDialog(holder.txtProductReturn,
                                                                    itemorderid, product_id, position);
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    } else {
                                                        CancelOrder(holder.txtProductReturn,
                                                                itemorderid, position);
                                                    }
                                                }
                                            });

                                    builder1.setNegativeButton(strNo,
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        } else {
                            if (paymentType > 1) {
                                try {
                                    String itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                                    String product_id = jsonObjectOrderDetail.getString("product_id");
                                    OnlinePaymentDialog(holder.txtProductReturn, itemorderid, product_id, position);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    String itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                                    String product_id = jsonObjectOrderDetail.getString("product_id");
                                    CODDialog(holder.txtProductReturn, itemorderid, product_id, position);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
                holder.txtAddReviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            String itemorderid = jsonObjectOrderDetail.getString("orderitem_id");
                            String product_id = jsonObjectOrderDetail.getString("product_id");
                            ADDReviewsDialog(holder.txtAddReviews, itemorderid, product_id);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

              /*  JSONArray jsonArrayImage = jsonObjectOrderDetail.getJSONArray("main_image");
                String image = jsonArrayImage.get(0).toString();*/

                JSONObject jsonObjectImage = jsonObjectOrderDetail.getJSONObject("main_image");
                String image = jsonObjectImage.getString("main_image");

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(image.replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.get()
                            .load(picUrl)
                            .into(holder.imageOrderDetail, new Callback() {
                                @Override
                                public void onSuccess() {
                                    holder.rlImgHolder.setVisibility(View.GONE);
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onError(Exception e) {
                                    //holder.pbHome.setVisibility(View.INVISIBLE);
                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                    SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
                //endregion

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            ImageView imageOrderDetail;
            TextView txtOrderDetailProductName, txtQtyTitle, txtOrderDetailQty, txtProducDate, txtProductPrice,
                    txtProductReturn, txtAddReviews;
            LinearLayout llOrderStatus;
            RelativeLayout rlImgHolder;
            ProgressBar pbImgHolder;

            public Viewholder(View itemView) {
                super(itemView);
                txtOrderDetailProductName = itemView.findViewById(R.id.txtOrderDetailProductName);
                txtQtyTitle = itemView.findViewById(R.id.txtQtyTitle);
                txtOrderDetailQty = itemView.findViewById(R.id.txtOrderDetailQty);
                txtProducDate = itemView.findViewById(R.id.txtProducDate);
                txtProductPrice = itemView.findViewById(R.id.txtProductPrice);
                txtProductReturn = itemView.findViewById(R.id.txtProductReturn);
                txtAddReviews = itemView.findViewById(R.id.txtAddReviews);
                imageOrderDetail = itemView.findViewById(R.id.imageOrderDetail);
                llOrderStatus = itemView.findViewById(R.id.llOrderStatus);
                rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
                pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);

                txtOrderDetailProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtQtyTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtOrderDetailQty.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtProducDate.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
                txtProductPrice.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
                txtProductReturn.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
                txtAddReviews.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));

                txtQtyTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                txtProducDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
                txtOrderDetailProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtOrderDetailQty.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtProductPrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtProductReturn.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtAddReviews.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sQTY) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sQTY).equals("")) {
                        txtQtyTitle.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sQTY));
                    } else {
                        txtQtyTitle.setText(context.getString(R.string.qty));
                    }
                } else {
                    txtQtyTitle.setText(context.getString(R.string.qty));
                }

            }
        }
    }
    //endregion

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void RefreshListing(int itemPositon, String ItemStatus);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    //region FOR CancelOrder API..
    private void CancelOrder(final TextView txtProductReturn, String itemorderid,
                             final int position) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"itemorderid", "status"};
        String[] val = {itemorderid, "5"};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.CancelOrder);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER) != null) {
                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER).equals("")) {
                                            txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER));
                                        } else {
                                            txtProductReturn.setText(getString(R.string.cancelled_order));
                                        }
                                    } else {
                                        txtProductReturn.setText(getString(R.string.cancelled_order));
                                    }
                                    txtProductReturn.setEnabled(false);
                                    /*recyclerviewOrderHistoryProduct.getAdapter().notifyDataSetChanged();*/
                                    mListener = (OnFragmentInteractionListener) getContext();
                                    mListener.RefreshListing(position, "5");
//                                    txtProductReturn.setClickable(false);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR Cancel Online Order API..
    private void CancelOnlineOrder(String holderName, String BankName, String BranchName, String ACNo, String IFSCode,
                                   String reason, final TextView txtProductReturn, String itemorderid, String product_id, final int position) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"itemorderid", "order_id", "product_id", "account_holder_name", "bank_name", "branch_name",
                "account_no", "ifsc_code", "reason", "status", "operation_type"};
        String[] val = {itemorderid, txtOrderID.getText().toString(), product_id, holderName, BankName, BranchName, ACNo,
                IFSCode, reason, "5", "online"};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.CancelOnlineOrder);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER) != null) {
                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER).equals("")) {
                                            txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER));
                                        } else {
                                            txtProductReturn.setText(getString(R.string.cancelled_order));
                                        }
                                    } else {
                                        txtProductReturn.setText(getString(R.string.cancelled_order));
                                    }
                                    txtProductReturn.setEnabled(false);
                                    mListener = (OnFragmentInteractionListener) getContext();
                                    mListener.RefreshListing(position, "5");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR OnlinePaymentCancelDialog...
    public void OnlineOrderCancelDialog(final TextView txtProductReturn,
                                        final String itemorderid, final String product_id,
                                        final int position) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.row_online_order_cancel_dialog, null, false);
        // find the Textview in the popup layout
        final EditText editAccountHolderName, editBankName, editBranchName, editAccountNo, editIFSCCode, editReason;
        final Button btnSubmit, btnCancel;
        final TextView txtCODTitle;
        txtCODTitle = inflatedView.findViewById(R.id.txtCODTitle);
        editAccountHolderName = inflatedView.findViewById(R.id.editAccountHolderName);
        editBankName = inflatedView.findViewById(R.id.editBankName);
        editBranchName = inflatedView.findViewById(R.id.editBranchName);
        editAccountNo = inflatedView.findViewById(R.id.editAccountNo);
        editIFSCCode = inflatedView.findViewById(R.id.editIFSCCode);
        editReason = inflatedView.findViewById(R.id.editReason);
        btnSubmit = inflatedView.findViewById(R.id.btnSubmit);
        btnCancel = inflatedView.findViewById(R.id.btnCancel);
        editAccountHolderName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editBankName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editBranchName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editAccountNo.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editIFSCCode.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editReason.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnCancel.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        chanageButton(btnSubmit);
        chanageButton(btnCancel);
        txtCODTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        //region SetDynamicStringLabel
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL_ORDER) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL_ORDER).equals("")) {
                txtCODTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL_ORDER));
            } else {
                txtCODTitle.setText(getString(R.string.cancel_order));
            }
        } else {
            txtCODTitle.setText(getString(R.string.cancel_order));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_HOLDER_NAME) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_HOLDER_NAME).equals("")) {
                editAccountHolderName.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_HOLDER_NAME));
            } else {
                editAccountHolderName.setHint(getString(R.string.account_holder_name));
            }
        } else {
            editAccountHolderName.setHint(getString(R.string.account_holder_name));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANK_NAME) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANK_NAME).equals("")) {
                editBankName.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANK_NAME));
            } else {
                editBankName.setHint(getString(R.string.bank_name));
            }
        } else {
            editBankName.setHint(getString(R.string.bank_name));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_NAME) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_NAME).equals("")) {
                editBranchName.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_NAME));
            } else {
                editBranchName.setHint(getString(R.string.branch_name));
            }
        } else {
            editBranchName.setHint(getString(R.string.branch_name));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_NUMBER) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_NUMBER).equals("")) {
                editAccountNo.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_NUMBER));
            } else {
                editAccountNo.setHint(getString(R.string.account_number));
            }
        } else {
            editAccountNo.setHint(getString(R.string.account_number));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CODE) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CODE).equals("")) {
                editIFSCCode.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CODE));
            } else {
                editIFSCCode.setHint(getString(R.string.ifsc_code));
            }
        } else {
            editIFSCCode.setHint(getString(R.string.ifsc_code));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON).equals("")) {
                editReason.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON));
            } else {
                editReason.setHint(getString(R.string.reason));
            }
        } else {
            editReason.setHint(getString(R.string.reason));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT).equals("")) {
                btnSubmit.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT));
            } else {
                btnSubmit.setText(getString(R.string.submit));
            }
        } else {
            btnSubmit.setText(getString(R.string.submit));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL).equals("")) {
                btnCancel.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCEL));
            } else {
                btnCancel.setText(getString(R.string.cancel));
            }
        } else {
            btnCancel.setText(getString(R.string.cancel));
        }//endregion
        /*editAccountHolderName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBankName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBranchName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editAccountNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editIFSCCode.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editReason.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));*/

        chanageEditTextBorder(editAccountHolderName);
        chanageEditTextBorder(editBankName);
        chanageEditTextBorder(editBranchName);
        chanageEditTextBorder(editAccountNo);
        chanageEditTextBorder(editIFSCCode);
        chanageEditTextBorder(editReason);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        ReturnCOD = builder.create();
        ReturnCOD.setCancelable(false);
        ReturnCOD.setView(inflatedView);
        ReturnCOD.setCanceledOnTouchOutside(true);
        if (ReturnCOD.isShowing()) {
            ReturnCOD.dismiss();
        } else {
            ReturnCOD.show();
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editAccountHolderName.setText("");
                editBankName.setText("");
                editBranchName.setText("");
                editAccountNo.setText("");
                editIFSCCode.setText("");
                editReason.setText("");
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(editAccountHolderName.getText().toString())) {
                    editAccountHolderName.setError(null);
                    if (!TextUtils.isEmpty(editBankName.getText().toString())) {
                        editBankName.setError(null);
                        if (!TextUtils.isEmpty(editBranchName.getText().toString())) {
                            editBranchName.setError(null);
                            if (!TextUtils.isEmpty(editAccountNo.getText().toString())) {
                                editAccountNo.setError(null);
                                if (!TextUtils.isEmpty(editIFSCCode.getText().toString())) {
                                    editIFSCCode.setError(null);
                                    if (!TextUtils.isEmpty(editReason.getText().toString())) {
                                        editReason.setError(null);
                                        ReturnCOD.dismiss();
                                        CancelOnlineOrder(editAccountHolderName.getText().toString(), editBankName.getText().toString(), editBranchName.getText().toString(),
                                                editAccountNo.getText().toString(), editIFSCCode.getText().toString(),
                                                editReason.getText().toString(), txtProductReturn
                                                , itemorderid, product_id, position);
                                    } else {
                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON_CHECK_EMPTY) != null) {
                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON_CHECK_EMPTY).equals("")) {
                                                editReason.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON_CHECK_EMPTY));
                                            } else {
                                                editReason.setError(getString(R.string.null_reason_error_message));
                                            }
                                        } else {
                                            editReason.setError(getString(R.string.null_reason_error_message));
                                        }
                                    }
                                } else {
                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CHECK_EMPTY) != null) {
                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CHECK_EMPTY).equals("")) {
                                            editIFSCCode.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CHECK_EMPTY));
                                        } else {
                                            editIFSCCode.setError(getString(R.string.null_ifsc_error_message));
                                        }
                                    } else {
                                        editIFSCCode.setError(getString(R.string.null_ifsc_error_message));
                                    }
                                }
                            } else {
                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCNO_CHECK_EMPTY) != null) {
                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCNO_CHECK_EMPTY).equals("")) {
                                        editAccountNo.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCNO_CHECK_EMPTY));
                                    } else {
                                        editAccountNo.setError(getString(R.string.null_account_number_error_message));
                                    }
                                } else {
                                    editAccountNo.setError(getString(R.string.null_account_number_error_message));
                                }
                            }
                        } else {
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_CHECK_EMPTY) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_CHECK_EMPTY).equals("")) {
                                    editBranchName.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_CHECK_EMPTY));
                                } else {
                                    editBranchName.setError(getString(R.string.null_branch_name_error_message));
                                }
                            } else {
                                editBranchName.setError(getString(R.string.null_branch_name_error_message));
                            }
                        }
                    } else {
                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANKNAME_CHECK_EMPTY) != null) {
                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANKNAME_CHECK_EMPTY).equals("")) {
                                editBankName.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANKNAME_CHECK_EMPTY));
                            } else {
                                editBankName.setError(getString(R.string.null_bank_name_error_message));
                            }
                        } else {
                            editBankName.setError(getString(R.string.null_bank_name_error_message));
                        }
                    }
                } else {
                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACC_HOLD_NAME_CHECK_EMPTY) != null) {
                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACC_HOLD_NAME_CHECK_EMPTY).equals("")) {
                            editAccountHolderName.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACC_HOLD_NAME_CHECK_EMPTY));
                        } else {
                            editAccountHolderName.setError(getString(R.string.null_account_holder_name_error_message));
                        }
                    } else {
                        editAccountHolderName.setError(getString(R.string.null_account_holder_name_error_message));
                    }
                }

            }
        });
        ReturnCOD.show();
    }
    //endregion

    //region FOR CODDialog...
    public void CODDialog(final TextView txtProductReturn, final String itemorderid, final String product_id, final int position) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.row_return_cod_dialog, null, false);
        // find the Textview in the popup layout
        final EditText editAccountHolderName, editBankName, editBranchName, editAccountNo, editIFSCCode, editReason;
        final Button btnSubmit;
        final TextView txtCODTitle;
        txtCODTitle = inflatedView.findViewById(R.id.txtCODTitle);
        editAccountHolderName = inflatedView.findViewById(R.id.editAccountHolderName);
        editBankName = inflatedView.findViewById(R.id.editBankName);
        editBranchName = inflatedView.findViewById(R.id.editBranchName);
        editAccountNo = inflatedView.findViewById(R.id.editAccountNo);
        editIFSCCode = inflatedView.findViewById(R.id.editIFSCCode);
        editReason = inflatedView.findViewById(R.id.editReason);
        btnSubmit = inflatedView.findViewById(R.id.btnSubmit);
        editAccountHolderName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editBankName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editBranchName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editAccountNo.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editIFSCCode.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editReason.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setOnClickListener(this);
        chanageButton(btnSubmit);
        txtCODTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

        //region SetDynamicStringLabel
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_DETAIL) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_DETAIL).equals("")) {
                txtCODTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_DETAIL));
            } else {
                txtCODTitle.setText(getString(R.string.return_detail));
            }
        } else {
            txtCODTitle.setText(getString(R.string.return_detail));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_HOLDER_NAME) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_HOLDER_NAME).equals("")) {
                editAccountHolderName.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_HOLDER_NAME));
            } else {
                editAccountHolderName.setHint(getString(R.string.account_holder_name));
            }
        } else {
            editAccountHolderName.setHint(getString(R.string.account_holder_name));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANK_NAME) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANK_NAME).equals("")) {
                editBankName.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANK_NAME));
            } else {
                editBankName.setHint(getString(R.string.bank_name));
            }
        } else {
            editBankName.setHint(getString(R.string.bank_name));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_NAME) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_NAME).equals("")) {
                editBranchName.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_NAME));
            } else {
                editBranchName.setHint(getString(R.string.branch_name));
            }
        } else {
            editBranchName.setHint(getString(R.string.branch_name));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_NUMBER) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_NUMBER).equals("")) {
                editAccountNo.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCOUNT_NUMBER));
            } else {
                editAccountNo.setHint(getString(R.string.account_number));
            }
        } else {
            editAccountNo.setHint(getString(R.string.account_number));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CODE) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CODE).equals("")) {
                editIFSCCode.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CODE));
            } else {
                editIFSCCode.setHint(getString(R.string.ifsc_code));
            }
        } else {
            editIFSCCode.setHint(getString(R.string.ifsc_code));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON).equals("")) {
                editReason.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON));
            } else {
                editReason.setHint(getString(R.string.reason));
            }
        } else {
            editReason.setHint(getString(R.string.reason));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT).equals("")) {
                btnSubmit.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT));
            } else {
                btnSubmit.setText(getString(R.string.submit));
            }
        } else {
            btnSubmit.setText(getString(R.string.submit));
        }
        //endregion

        /*editAccountHolderName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBankName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editBranchName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editAccountNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editIFSCCode.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editReason.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));*/

        chanageEditTextBorder(editAccountHolderName);
        chanageEditTextBorder(editBankName);
        chanageEditTextBorder(editBranchName);
        chanageEditTextBorder(editAccountNo);
        chanageEditTextBorder(editIFSCCode);
        chanageEditTextBorder(editReason);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        ReturnCOD = builder.create();
        ReturnCOD.setCancelable(false);
        ReturnCOD.setView(inflatedView);
        ReturnCOD.setCanceledOnTouchOutside(true);
        if (ReturnCOD.isShowing()) {
            ReturnCOD.dismiss();
        } else {
            ReturnCOD.show();
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(editAccountHolderName.getText().toString())) {
                    editAccountHolderName.setError(null);
                    if (!TextUtils.isEmpty(editBankName.getText().toString())) {
                        editBankName.setError(null);
                        if (!TextUtils.isEmpty(editBranchName.getText().toString())) {
                            editBranchName.setError(null);
                            if (!TextUtils.isEmpty(editAccountNo.getText().toString())) {
                                editAccountNo.setError(null);
                                if (!TextUtils.isEmpty(editIFSCCode.getText().toString())) {
                                    editIFSCCode.setError(null);
                                    if (!TextUtils.isEmpty(editReason.getText().toString())) {
                                        editReason.setError(null);
                                        ReturnCOD.dismiss();
                                        ReturnCOD(editAccountHolderName.getText().toString(), editBankName.getText().toString(), editBranchName.getText().toString(),
                                                editAccountNo.getText().toString(), editIFSCCode.getText().toString(),
                                                editReason.getText().toString(), txtProductReturn
                                                , itemorderid, product_id, position);
                                    } else {
                                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON_CHECK_EMPTY) != null) {
                                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON_CHECK_EMPTY).equals("")) {
                                                editReason.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON_CHECK_EMPTY));
                                            } else {
                                                editReason.setError(getString(R.string.null_reason_error_message));
                                            }
                                        } else {
                                            editReason.setError(getString(R.string.null_reason_error_message));
                                        }
                                    }
                                } else {
                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CHECK_EMPTY) != null) {
                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CHECK_EMPTY).equals("")) {
                                            editIFSCCode.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIFSC_CHECK_EMPTY));
                                        } else {
                                            editIFSCCode.setError(getString(R.string.null_ifsc_error_message));
                                        }
                                    } else {
                                        editIFSCCode.setError(getString(R.string.null_ifsc_error_message));
                                    }
                                }
                            } else {
                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCNO_CHECK_EMPTY) != null) {
                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCNO_CHECK_EMPTY).equals("")) {
                                        editAccountNo.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACCNO_CHECK_EMPTY));
                                    } else {
                                        editAccountNo.setError(getString(R.string.null_account_number_error_message));
                                    }
                                } else {
                                    editAccountNo.setError(getString(R.string.null_account_number_error_message));
                                }
                            }
                        } else {
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_CHECK_EMPTY) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_CHECK_EMPTY).equals("")) {
                                    editBranchName.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBRANCH_CHECK_EMPTY));
                                } else {
                                    editBranchName.setError(getString(R.string.null_branch_name_error_message));
                                }
                            } else {
                                editBranchName.setError(getString(R.string.null_branch_name_error_message));
                            }
                        }
                    } else {
                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANKNAME_CHECK_EMPTY) != null) {
                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANKNAME_CHECK_EMPTY).equals("")) {
                                editBankName.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sBANKNAME_CHECK_EMPTY));
                            } else {
                                editBankName.setError(getString(R.string.null_bank_name_error_message));
                            }
                        } else {
                            editBankName.setError(getString(R.string.null_bank_name_error_message));
                        }
                    }
                } else {
                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACC_HOLD_NAME_CHECK_EMPTY) != null) {
                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACC_HOLD_NAME_CHECK_EMPTY).equals("")) {
                            editAccountHolderName.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sACC_HOLD_NAME_CHECK_EMPTY));
                        } else {
                            editAccountHolderName.setError(getString(R.string.null_account_holder_name_error_message));
                        }
                    } else {
                        editAccountHolderName.setError(getString(R.string.null_account_holder_name_error_message));
                    }

                }

            }
        });
        ReturnCOD.show();
    }
    //endregion

    //region FOR ReturnCOD API..
    private void ReturnCOD(String holderName, String BankName, String BranchName, String ACNo, String IFSCode,
                           String reason, final TextView txtProductReturn, String itemorderid, String product_id, final int position) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"itemorder_id", "order_id", "product_id", "account_holder_name", "bank_name", "branch_name",
                "account_no", "ifsc_code", "reason"};
        String[] val = {itemorderid, txtOrderID.getText().toString(), product_id, holderName, BankName, BranchName, ACNo,
                IFSCode, reason};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.ReturnCODOrder);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURNED_ORDER) != null) {
                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURNED_ORDER).equals("")) {
                                            txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURNED_ORDER));
                                        } else {
                                            txtProductReturn.setText(getString(R.string.returned_order));
                                        }
                                    } else {
                                        txtProductReturn.setText(getString(R.string.returned_order));
                                    }
                                    txtProductReturn.setClickable(false);
                                    mListener = (OnFragmentInteractionListener) getContext();
                                    mListener.RefreshListing(position, "6");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR OnlinePaymentDialog...
    public void OnlinePaymentDialog(final TextView txtProductReturn, final String itemorderid, final String product_id, final int position) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.row_return_online_payment_dialog, null, false);
        // find the Textview in the popup layout
        final EditText editReason;
        final Button btnSubmit;
        TextView txtCODTitle;
        txtCODTitle = inflatedView.findViewById(R.id.txtCODTitle);
        editReason = inflatedView.findViewById(R.id.editReason);
        btnSubmit = inflatedView.findViewById(R.id.btnSubmit);
        editReason.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtCODTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editReason.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSubmit.setOnClickListener(this);
        chanageButton(btnSubmit);
        chanageEditTextBorder(editReason);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        //region SetDynamicStringLabel
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_DETAIL) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_DETAIL).equals("")) {
                txtCODTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURN_DETAIL));
            } else {
                txtCODTitle.setText(getString(R.string.return_detail));
            }
        } else {
            txtCODTitle.setText(getString(R.string.return_detail));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON).equals("")) {
                editReason.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON));
            } else {
                editReason.setHint(getString(R.string.reason));
            }
        } else {
            editReason.setHint(getString(R.string.reason));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT).equals("")) {
                btnSubmit.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT));
            } else {
                btnSubmit.setText(getString(R.string.submit));
            }
        } else {
            btnSubmit.setText(getString(R.string.submit));
        }
        //endregion

        ReturnOnline = builder.create();
        ReturnOnline.setCancelable(false);
        ReturnOnline.setView(inflatedView);
        ReturnOnline.setCanceledOnTouchOutside(true);
        if (ReturnOnline.isShowing()) {
            ReturnOnline.dismiss();
        } else {
            ReturnOnline.show();
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(editReason.getText().toString())) {
                    editReason.setError(null);
                    ReturnOnline.dismiss();
                    ReturnOnlinePayment(editReason.getText().toString(), txtProductReturn, itemorderid, product_id, position);
                } else {
                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON_CHECK_EMPTY) != null) {
                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON_CHECK_EMPTY).equals("")) {
                            editReason.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREASON_CHECK_EMPTY));
                        } else {
                            editReason.setError(getString(R.string.null_reason_error_message));
                        }
                    } else {
                        editReason.setError(getString(R.string.null_reason_error_message));
                    }
                }

            }
        });
        ReturnOnline.show();
    }
    //endregion

    //region FOR ReturnOnlinePayment API..
    private void ReturnOnlinePayment(String reason, final TextView txtProductReturn, String itemorderid, String product_id, final int position) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"itemorder_id", "order_id", "product_id", "reason"};
        String[] val = {itemorderid, txtOrderID.getText().toString(), product_id, reason};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.ReturnNetOrder);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURNED_ORDER) != null) {
                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURNED_ORDER).equals("")) {
                                            txtProductReturn.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURNED_ORDER));
                                        } else {
                                            txtProductReturn.setText(getString(R.string.returned_order));
                                        }
                                    } else {
                                        txtProductReturn.setText(getString(R.string.returned_order));
                                    }
                                    txtProductReturn.setClickable(false);
                                    mListener = (OnFragmentInteractionListener) getContext();
                                    mListener.RefreshListing(position, "6");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in OrderDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region FOR ADDReviewsDialog...
    public void ADDReviewsDialog(final TextView txtAddReviews, final String itemId, final String ProductID) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View inflatedView = layoutInflater.inflate(R.layout.row_add_review, null, false);
        final EditText editUserName, editUserEmail, editUserTitle, editUserDes;
        final TextView txtRecomment;
        final Button btnSubmit;
        final RatingBar reviewRatingbar;
        final RadioGroup radioGroupRecomme;
        final RadioButton radioYes, radioNo;
        final TextView txtCODTitle;
        txtCODTitle = inflatedView.findViewById(R.id.txtCODTitle);
        reviewRatingbar = inflatedView.findViewById(R.id.reviewRatingbar1);
        editUserName = inflatedView.findViewById(R.id.editUserName);
        editUserEmail = inflatedView.findViewById(R.id.editUserEmail);
        editUserTitle = inflatedView.findViewById(R.id.editUserTitle);
        editUserDes = inflatedView.findViewById(R.id.editUserDes);
        txtRecomment = inflatedView.findViewById(R.id.txtRecomment);
        btnSubmit = inflatedView.findViewById(R.id.btnSubmit);
        radioGroupRecomme = inflatedView.findViewById(R.id.radioGroupRecomme);
        radioYes = inflatedView.findViewById(R.id.radioYes);
        radioNo = inflatedView.findViewById(R.id.radioNo);
        editUserName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editUserEmail.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editUserTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editUserDes.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        txtRecomment.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setOnClickListener(this);
        editUserEmail.setEnabled(false);
        chanageButton(btnSubmit);

        chanageEditTextBorder(editUserDes);
        chanageEditTextBorder(editUserName);
        chanageEditTextBorder(editUserEmail);
        chanageEditTextBorder(editUserTitle);
        changeRadioButtonColor(radioYes);
        changeRadioButtonColor(radioNo);

        txtCODTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editUserEmail.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editUserTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editUserDes.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtRecomment.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

        //region SetDynamicStringLabel
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_YOUR_REVIEW) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_YOUR_REVIEW).equals("")) {
                txtCODTitle.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_YOUR_REVIEW));
            } else {
                txtCODTitle.setText(getString(R.string.add_your_review));
            }
        } else {
            txtCODTitle.setText(getString(R.string.add_your_review));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUSER_NAME) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUSER_NAME).equals("")) {
                editUserName.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUSER_NAME));
            } else {
                editUserName.setHint(getString(R.string.user_name));
            }
        } else {
            editUserName.setHint(getString(R.string.user_name));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_ADDRESS).equals("")) {
                editUserEmail.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_ADDRESS));
            } else {
                editUserEmail.setHint(getString(R.string.email));
            }
        } else {
            editUserEmail.setHint(getString(R.string.email));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTITLE) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTITLE).equals("")) {
                editUserTitle.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTITLE));
            } else {
                editUserTitle.setHint(getString(R.string.tilte));
            }
        } else {
            editUserTitle.setHint(getString(R.string.tilte));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDESCRIPTION) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDESCRIPTION).equals("")) {
                editUserDes.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDESCRIPTION));
            } else {
                editUserDes.setHint(getString(R.string.description));
            }
        } else {
            editUserDes.setHint(getString(R.string.description));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sWOULD_YOU_LIKE_TO_RECOMMEND_THIS_PRODUCT) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sWOULD_YOU_LIKE_TO_RECOMMEND_THIS_PRODUCT).equals("")) {
                txtRecomment.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sWOULD_YOU_LIKE_TO_RECOMMEND_THIS_PRODUCT));
            } else {
                txtRecomment.setText(getString(R.string.would_you_like_to_recommend_this_product));
            }
        } else {
            txtRecomment.setText(getString(R.string.would_you_like_to_recommend_this_product));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sYES) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sYES).equals("")) {
                radioYes.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sYES));
            } else {
                radioYes.setText(getString(R.string.yes));
            }
        } else {
            radioYes.setText(getString(R.string.yes));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO).equals("")) {
                radioNo.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO));
            } else {
                radioNo.setText(getString(R.string.no));
            }
        } else {
            radioNo.setText(getString(R.string.no));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT).equals("")) {
                btnSubmit.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT));
            } else {
                btnSubmit.setText(getString(R.string.submit));
            }
        } else {
            btnSubmit.setText(getString(R.string.submit));
        }//endregion

        LayerDrawable stars = (LayerDrawable) reviewRatingbar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);

        String name = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Name);
        String email = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_EMAIL);
        editUserName.setText(name);
        editUserEmail.setText(email);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        Reviews = builder.create();
        Reviews.setCancelable(false);
        Reviews.setView(inflatedView);
        Reviews.setCanceledOnTouchOutside(true);
        if (Reviews.isShowing()) {
            Reviews.dismiss();
        } else {
            Reviews.show();
        }

        radioGroupRecomme.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (radioGroupRecomme.getCheckedRadioButtonId()) {
                    case R.id.radioYes:
                        if (radioYes.isChecked()) {
                            selectradio = "1";
                            radioNo.setChecked(false);

                        }
                        break;
                    case R.id.radioNo:
                        if (radioNo.isChecked()) {
                            selectradio = "0";
                            radioYes.setChecked(false);
                        }
                        break;
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(editUserName.getText().toString())) {
                    editUserName.setError(null);
                    if (!TextUtils.isEmpty(editUserEmail.getText().toString())) {
                        editUserEmail.setError(null);
                        if (!TextUtils.isEmpty(editUserTitle.getText().toString())) {
                            editUserTitle.setError(null);
                            if (!TextUtils.isEmpty(editUserDes.getText().toString())) {
                                editUserDes.setError(null);
                                if (reviewRatingbar.getRating() != 0.0) {
                                    Reviews.dismiss();
                                    SubmitReviews(editUserDes.getText().toString(), editUserEmail.getText().toString(),
                                            editUserName.getText().toString(), editUserTitle.getText().toString(),
                                            String.valueOf(reviewRatingbar.getRating()), selectradio, txtAddReviews, itemId, ProductID);
                                } else {
                                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRATING_CHECK_EMPTY) != null) {
                                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRATING_CHECK_EMPTY).equals("")) {
                                            Toast.makeText(getContext(), SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRATING_CHECK_EMPTY), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getContext(), getText(R.string.null_rating_error_message), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(getContext(), getText(R.string.null_rating_error_message), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else {
                                if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDESC_CHECK_EMPTY) != null) {
                                    if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDESC_CHECK_EMPTY).equals("")) {
                                        editUserDes.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDESC_CHECK_EMPTY));
                                    } else {
                                        editUserDes.setError(getString(R.string.null_des_error_message));
                                    }
                                } else {
                                    editUserDes.setError(getString(R.string.null_des_error_message));
                                }
                            }
                        } else {
                            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTITLE_CHECK_EMPTY) != null) {
                                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTITLE_CHECK_EMPTY).equals("")) {
                                    editUserTitle.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTITLE_CHECK_EMPTY));
                                } else {
                                    editUserTitle.setError(getString(R.string.null_title_error_message));
                                }
                            } else {
                                editUserTitle.setError(getString(R.string.null_title_error_message));
                            }
                        }
                    } else {
                        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_EMPTY) != null) {
                            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_EMPTY).equals("")) {
                                editUserEmail.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_EMPTY));
                            } else {
                                editUserEmail.setError(getString(R.string.null_email_error_message));
                            }
                        } else {
                            editUserEmail.setError(getString(R.string.null_email_error_message));
                        }
                    }
                } else {
                    if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUNM_CHECK_EMPTY) != null) {
                        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUNM_CHECK_EMPTY).equals("")) {
                            editUserName.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUNM_CHECK_EMPTY));
                        } else {
                            editUserName.setError(getString(R.string.null_user_name_error_message));
                        }
                    } else {
                        editUserName.setError(getString(R.string.null_user_name_error_message));
                    }
                }

            }
        });
        Reviews.show();
    }
    //endregion

    //region FOR SubmitReviews API..
    private void SubmitReviews(String des, String email, String name, String title, String rating, String recomment,
                               final TextView txtAddReviews, String itemId, String ProductId) {
        relativeProgress.setVisibility(View.VISIBLE);

        String userid = SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USERID);

        String[] key = {"description", "email", "itemorder_id", "name", "product_id", "rating", "title", "user_id", "recommendthisproject"};
        String[] val = {des, email, itemId, name, ProductId, rating, title, userid, recomment};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.SubmitReview);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    txtAddReviews.setVisibility(View.GONE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ProductDetailFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            button.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(50);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

    }
    //endregion

    // region changeRadioButtonColor
    @SuppressLint("NewApi")
    public void changeRadioButtonColor(RadioButton radioButton) {
        radioButton.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        radioButton.setButtonTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor))));
    }
    //endregion
}
