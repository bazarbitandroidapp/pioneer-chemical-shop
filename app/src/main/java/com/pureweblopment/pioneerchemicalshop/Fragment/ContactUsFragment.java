package com.pureweblopment.pioneerchemicalshop.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;

import com.pureweblopment.pioneerchemicalshop.Activity.LoginActivity;
import com.pureweblopment.pioneerchemicalshop.Activity.MainActivity;
import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.SendMail;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;
import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;
import com.pureweblopment.pioneerchemicalshop.R;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContactUsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContactUsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactUsFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RelativeLayout relativeProgress;
    EditText editName, editEmailAddress, editSubject, editMessage;
    Button btnSubmit;

    LinearLayout llBottomNavigation;
    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    CardView cardviewBottomNavigation;
    TextView txtCatName;

    private OnFragmentInteractionListener mListener;

    public ContactUsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ContactUsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactUsFragment newInstance(String param1, String param2) {
        ContactUsFragment fragment = new ContactUsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);

        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = (ImageView) getActivity().findViewById(R.id.imageNavigation);
        imageLogo = (ImageView) getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = (FrameLayout) getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = (TextView) getActivity().findViewById(R.id.txtCatName);
        llBottomNavigation = (LinearLayout) getActivity().findViewById(R.id.llBottomNavigation);
        cardviewBottomNavigation = (CardView) getActivity().findViewById(R.id.cardviewBottomNavigation);

        imageCartBack.setVisibility(View.GONE);
        txtCatName.setVisibility(View.VISIBLE);
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        imageNavigation.setVisibility(View.VISIBLE);
        imageLogo.setVisibility(View.GONE);
        frameLayoutCart.setVisibility(View.GONE);
        cardviewBottomNavigation.setVisibility(View.GONE);

        Initialization(view);
        TypeFaces();
        onClickLisnear();
        setDynamicString();

        chanageEditTextBorder(editName);
        chanageEditTextBorder(editEmailAddress);
        chanageEditTextBorder(editSubject);
        chanageEditTextBorder(editMessage);
        chanageButton(btnSubmit);

        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Name) != null) {
            editName.setText(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_Name));
        }
        if (SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_EMAIL) != null) {
            editEmailAddress.setText(SharedPreference.GetPreference(getContext(), Global.LOGIN_PREFERENCE, Global.USER_EMAIL));
        }

        return view;
    }

    private void setDynamicString() {
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCONTACT_US) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCONTACT_US).equals("")) {
                txtCatName.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCONTACT_US));
            } else {
                txtCatName.setText(R.string.contactus);
            }
        } else {
            txtCatName.setText(R.string.contactus);
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNAME) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNAME).equals("")) {
                editName.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNAME));
            } else {
                editName.setHint(getString(R.string.name));
            }
        } else {
            editName.setHint(getString(R.string.name));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_ADDRESS).equals("")) {
                editEmailAddress.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_ADDRESS));
            } else {
                editEmailAddress.setHint(getString(R.string.email));
            }
        } else {
            editEmailAddress.setHint(getString(R.string.email));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBJECT) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBJECT).equals("")) {
                editSubject.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBJECT));
            } else {
                editSubject.setHint(getString(R.string.subject));
            }
        } else {
            editSubject.setHint(getString(R.string.subject));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMESSAGE) != null) {
        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMESSAGE).equals("")) {
            editMessage.setHint(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMESSAGE));
        }else {
            editMessage.setHint(getString(R.string.message));
        }
        } else {
            editMessage.setHint(getString(R.string.message));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT) != null) {
            if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT).equals("")) {
                btnSubmit.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBMIT));
            } else {
                btnSubmit.setText(getString(R.string.submit));
            }
        } else {
            btnSubmit.setText(getString(R.string.submit));
        }
    }

    //region Initialization
    private void Initialization(View view) {
        relativeProgress = view.findViewById(R.id.relativeProgress);
        editMessage = view.findViewById(R.id.editMessage);
        editSubject = view.findViewById(R.id.editSubject);
        editEmailAddress = view.findViewById(R.id.editEmailAddress);
        editName = view.findViewById(R.id.editName);
        btnSubmit = view.findViewById(R.id.btnSubmit);
    }
    //endregion

    //region TypeFaces
    private void TypeFaces() {
        editMessage.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editSubject.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editEmailAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        editName.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
        btnSubmit.setTypeface(Typefaces.TypefaceCalibri_Regular(getContext()));
    }
    //endregion

    //region onClickLisnear
    private void onClickLisnear() {
        btnSubmit.setOnClickListener(this);
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                if (Validation()) {
                    SubmitContactUs();
                }
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
            editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        }
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

    }
    //endregion

    //region chanageButton
    public void chanageButton(Button button) {
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor) != "") {
            button.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        }
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            gd.setColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(60);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion

    //region Validation
    public Boolean Validation() {
        Boolean valid = false;
        if (!TextUtils.isEmpty(editName.getText().toString())) {
            editName.setError(null);
            if (!TextUtils.isEmpty(editEmailAddress.getText().toString())) {
                editEmailAddress.setError(null);
                if (Global.isValidEmail(editEmailAddress.getText())) {
                    if (!TextUtils.isEmpty(editSubject.getText().toString())) {
                        editSubject.setError(null);
                        if (!TextUtils.isEmpty(editMessage.getText().toString())) {
                            editMessage.setError(null);
                            valid = true;
                        } else {
                            if(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMSG_CHECK_EMPTY)!=null){
                                if(!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMSG_CHECK_EMPTY).equals("")) {
                                    editMessage.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMSG_CHECK_EMPTY));
                                }else {
                                    editMessage.setError(getString(R.string.null_message_error_message));
                                }
                            }else {
                                editMessage.setError(getString(R.string.null_message_error_message));
                            }
                        }
                    } else {
                        if(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBJECT_CHECK_EMPTY)!=null){
                            if(!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBJECT_CHECK_EMPTY).equals("")) {
                                editSubject.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUBJECT_CHECK_EMPTY));
                            }else {
                                editSubject.setError(getString(R.string.null_subject_error_message));
                            }
                        }else {
                            editSubject.setError(getString(R.string.null_subject_error_message));
                        }
                    }
                } else {
                    if(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_VALID)!=null){
                        if(!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_VALID).equals("")) {
                            editEmailAddress.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_VALID));
                        }else {
                            editEmailAddress.setError(getString(R.string.valid_email_error_message));
                        }
                    }else {
                        editEmailAddress.setError(getString(R.string.valid_email_error_message));
                    }
                }
            } else {
                if(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_EMPTY)!=null){
                    if(!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_EMPTY).equals("")) {
                        editEmailAddress.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEMAIL_CHECK_EMPTY));
                    }else {
                        editEmailAddress.setError(getString(R.string.null_email_error_message));
                    }
                }else {
                    editEmailAddress.setError(getString(R.string.null_email_error_message));
                }
            }
        } else {
            if(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUNM_CHECK_EMPTY)!=null){
                if(!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUNM_CHECK_EMPTY).equals("")) {
                    editName.setError(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sUNM_CHECK_EMPTY));
                }else {
                    editName.setError(getString(R.string.null_user_name_error_message));
                }
            }else {
                editName.setError(getString(R.string.null_user_name_error_message));
            }
        }
        return valid;
    }
    //endregion

    //region FOR SubmitContactUs...
    private void SubmitContactUs() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"name", "email", "subject", "message"};
        String[] val = {editName.getText().toString(), editEmailAddress.getText().toString(), editSubject.getText().toString(), editMessage.getText().toString()};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL + StaticUtility.SubmitContactUs);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(getContext()));
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();


        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    editSubject.setText("");
                                    editMessage.setText("");
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                    MainActivity.manageBackPress(true);
                                    startActivity(new Intent(getActivity(), MainActivity.class));
                                    getActivity().finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(getContext(), getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(getContext(), Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(getContext(), Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(getContext(), Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(getContext(), Global.Billing_Preference);
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    /*getActivity().finish();*/
                                } else {
                                    Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in ContactUsFragment.java When parsing Error response.\n" + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }

    //endregion
}
