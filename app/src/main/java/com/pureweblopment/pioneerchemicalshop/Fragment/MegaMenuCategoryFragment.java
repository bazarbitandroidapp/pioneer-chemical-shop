package com.pureweblopment.pioneerchemicalshop.Fragment;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.pureweblopment.pioneerchemicalshop.Activity.MainActivity;
import com.pureweblopment.pioneerchemicalshop.Adapter.MegamenuThirdLevelAdapter;
import com.pureweblopment.pioneerchemicalshop.Global.Global;
import com.pureweblopment.pioneerchemicalshop.Global.SharedPreference;
import com.pureweblopment.pioneerchemicalshop.Global.Typefaces;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.Executors;

import com.pureweblopment.pioneerchemicalshop.Global.SendMail;
import com.pureweblopment.pioneerchemicalshop.Global.StaticUtility;

import com.pureweblopment.pioneerchemicalshop.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MegaMenuCategoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MegaMenuCategoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MegaMenuCategoryFragment extends Fragment implements MegamenuThirdLevelAdapter.ThirdLevelClick {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ExpandableListView expantableListview;
    RelativeLayout relativeProgress;

    LinearLayout llBottomNavigation;
    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;

    String menuname;
    String[] mParents;
    String[] mChildNames, mChildSlug, mChildImages;
    String[] mSubChildNames, mSubChildSlugs, mSubChildImages;

    //First Level
    ArrayList<LinkedHashMap<String, String[]>> mFirstLevelData = new ArrayList<>();
    ArrayList<LinkedHashMap<String, String[]>> mFirstLevelSlugData = new ArrayList<>();
    ArrayList<LinkedHashMap<String, String[]>> mFirstLevelImageData = new ArrayList<>();

    //Second Level
    ArrayList<String[]> mSecondLevelName = new ArrayList<>();
    ArrayList<String[]> mSecondLevelSlug = new ArrayList<>();
    ArrayList<String[]> mSecondLevelImages = new ArrayList<>();

    //Third Level
    LinkedHashMap<String, String[]> mThirdLevelName = new LinkedHashMap<>();
    LinkedHashMap<String, String[]> mThirdLevelSlug = new LinkedHashMap<>();
    LinkedHashMap<String, String[]> mThirdLevelImage = new LinkedHashMap<>();

    private OnFragmentInteractionListener mListener;

    int count = 0;
    int width;


    public MegaMenuCategoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MegaMenuCategoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MegaMenuCategoryFragment newInstance(String param1, String param2) {
        MegaMenuCategoryFragment fragment = new MegaMenuCategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        View view = inflater.inflate(R.layout.fragment_mega_menu_category, container, false);

        mSecondLevelName.clear();
        mFirstLevelData.clear();
        mSecondLevelSlug.clear();
        mSecondLevelImages.clear();
        mFirstLevelSlugData.clear();
        mFirstLevelImageData.clear();
        count = 0;

        ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        imageCartBack = (ImageView) getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = (ImageView) getActivity().findViewById(R.id.imageNavigation);
        imageLogo = (ImageView) getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = (FrameLayout) getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = (TextView) getActivity().findViewById(R.id.txtCatName);
        llBottomNavigation = (LinearLayout) getActivity().findViewById(R.id.llBottomNavigation);

        imageCartBack.setVisibility(View.GONE);
        txtCatName.setVisibility(View.GONE);

        llBottomNavigation.setVisibility(View.VISIBLE);
        imageNavigation.setVisibility(View.VISIBLE);
        imageLogo.setVisibility(View.VISIBLE);
        frameLayoutCart.setVisibility(View.VISIBLE);

        Initialization(view);

        getMegaMenu();
        return view;
    }

    //region Initialization
    private void Initialization(View view) {
        expantableListview = view.findViewById(R.id.expantableListview);
        relativeProgress = view.findViewById(R.id.relativeProgress);
//        expantableListview.setGroupIndicator(null);
//        expantableListview.setIndicatorBounds(expantableListview.getRight()-GetDipsFromPixel(35), width-GetDipsFromPixel(5));
    }
    //endregion

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void selectedSubCategory(String Value, String Slug) {
        mListener = (OnFragmentInteractionListener) getContext();
        mListener.gotoProductListing(Slug, Value);
        mSecondLevelName.clear();
        mFirstLevelData.clear();
        mSecondLevelSlug.clear();
        mSecondLevelImages.clear();
        mFirstLevelSlugData.clear();
        mFirstLevelImageData.clear();
        count = 0;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void gotoProductListing(String category_slug, String CatName);
    }

    //region FOR getMegaMenu API..
    private void getMegaMenu() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {};
        String[] val = {};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.GetMegaMenu);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers());
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    int a = 0;
                                    JSONArray jsonArrayPayload = response.getJSONArray("payload");
                                    mParents = new String[jsonArrayPayload.length()];
                                    for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                        JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(i);
                                        int megamenu_fk = Integer.parseInt(jsonObjectPayload.getString("z_megamenuid_fk"));
                                        if (megamenu_fk > 0) {
                                            menuname = jsonObjectPayload.getString("menuname");
                                            if (jsonObjectPayload.has("child")) {
                                                JSONArray jsonArrayChild = jsonObjectPayload.getJSONArray("child");
                                                mChildNames = new String[jsonArrayChild.length()];
                                                mChildSlug = new String[jsonArrayChild.length()];
                                                mChildImages = new String[jsonArrayChild.length()];
                                                mThirdLevelName = new LinkedHashMap<>();
                                                mThirdLevelSlug = new LinkedHashMap<>();
                                                mThirdLevelImage = new LinkedHashMap<>();
                                                for (int j = 0; j < jsonArrayChild.length(); j++) {
                                                    JSONObject jsonObjectChild = jsonArrayChild.getJSONObject(j);
                                                    String childcategoryname = jsonObjectChild.getString("categoryname");
                                                    String childslug = jsonObjectChild.getString("slug");
                                                    JSONObject joChildImage = jsonObjectChild.optJSONObject("image_url");
                                                    String strChildImage = joChildImage.optString("418x418");
                                                    mChildNames[j] = childcategoryname;
                                                    mChildSlug[j] = childslug;
                                                    mChildImages[j] = strChildImage;
                                                    if (jsonArrayChild.length() - 1 == j) {
                                                        mSecondLevelName.add(mChildNames);
                                                        mSecondLevelSlug.add(mChildSlug);
                                                        mSecondLevelImages.add(mChildImages);
                                                    }
                                                    JSONArray jsonArraySubCategory = jsonObjectChild.getJSONArray("subcat");
                                                    mSubChildNames = new String[jsonArraySubCategory.length()];
                                                    mSubChildSlugs = new String[jsonArraySubCategory.length()];
                                                    mSubChildImages = new String[jsonArraySubCategory.length()];
                                                    if (jsonArraySubCategory.length() > 0) {
                                                        for (int k = 0; k < jsonArraySubCategory.length(); k++) {
                                                            JSONObject jsonObjectSubCategory = jsonArraySubCategory.getJSONObject(k);
                                                            String name = jsonObjectSubCategory.getString("name");
                                                            String slug = jsonObjectSubCategory.getString("slug");
                                                            String category_id = jsonObjectSubCategory.getString("category_id");
                                                            JSONObject joSubChildImage = jsonObjectSubCategory.optJSONObject("image_url");
                                                            String strSubChildImage = "";
                                                            if(joSubChildImage != null) {
                                                                strSubChildImage = joSubChildImage.optString("418x418");
                                                            }
                                                            mSubChildNames[k] = name;
                                                            mSubChildSlugs[k] = slug;
                                                            mSubChildImages[k] = strSubChildImage;
                                                        }
                                                    }
                                                    mThirdLevelName.put(mChildNames[j] + j, mSubChildNames);
                                                    mThirdLevelSlug.put(mChildSlug[j] + j, mSubChildSlugs);
                                                    mThirdLevelImage.put(mChildImages[j] + j, mSubChildImages);
                                                }
                                            }
                                            mParents[a] = menuname;
                                            mFirstLevelData.add(a, mThirdLevelName);
                                            mFirstLevelSlugData.add(a, mThirdLevelSlug);
                                            mFirstLevelImageData.add(a, mThirdLevelImage);
                                            a++;
                                            ThreeLevelListAdapter threeLevelListAdapterAdapter = new
                                                    ThreeLevelListAdapter(getContext(), mParents,
                                                    mSecondLevelName, mSecondLevelSlug, mSecondLevelImages,
                                                    mFirstLevelData, mFirstLevelSlugData, mFirstLevelImageData);
                                            expantableListview.setAdapter(threeLevelListAdapterAdapter);
                                        } else {
                                            count++;
                                        }

                                    }
                                    /*if (megaMenus.size() > 0) {
                                        expantableListview.setVisibility(View.VISIBLE);
                                        ExpandableListAdapter expandableListAdapter = new ExpandableListAdapter(getContext(), megaMenus, listHashMap);
                                        expantableListview.setAdapter(expandableListAdapter);
                                    }*/
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                Toast.makeText(getContext(), strMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(getContext(), Global.TOEMAIL, Global.SUBJECT, "Getting error in MegaMenuCategoryFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ThreeLevelListAdapter
    public class ThreeLevelListAdapter extends BaseExpandableListAdapter {

        private Context context;

        String[] parentHeaders;

        ArrayList<String[]> mSecondLevel;
        ArrayList<String[]> mSecondLevelSlug;
        ArrayList<String[]> mSecondLevelImages;

        ArrayList<LinkedHashMap<String, String[]>> mThirdLevel;
        ArrayList<LinkedHashMap<String, String[]>> mThirdLevelSlug;
        ArrayList<LinkedHashMap<String, String[]>> mThirdLevelImage;

        public ThreeLevelListAdapter(Context context, String[] parentHeader,
                                     ArrayList<String[]> secondLevel,
                                     ArrayList<String[]> mSecondSlugLevel,
                                     ArrayList<String[]> mSecondLevelImages,
                                     ArrayList<LinkedHashMap<String, String[]>> data,
                                     ArrayList<LinkedHashMap<String, String[]>> Slugdata,
                                     ArrayList<LinkedHashMap<String, String[]>> ImageData) {
            this.context = context;

            this.parentHeaders = parentHeader;

            //Second level data
            this.mSecondLevel = secondLevel;
            this.mSecondLevelSlug = mSecondSlugLevel;
            this.mSecondLevelImages = mSecondLevelImages;

            //Third level data
            this.mThirdLevel = data;
            this.mThirdLevelSlug = Slugdata;
            this.mThirdLevelImage = ImageData;
        }

        @Override
        public int getGroupCount() {
            return parentHeaders.length - count;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return 1;

        }

        @Override
        public Object getGroup(int groupPosition) {

            return groupPosition;
        }

        @Override
        public Object getChild(int group, int child) {
            return child;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_first, null);
            TextView text = convertView.findViewById(R.id.rowParentText);
            LinearLayout mLlRowParentText = convertView.findViewById(R.id.llRowParentText);
            ImageView mImgdown = convertView.findViewById(R.id.imgdown);
            text.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                text.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
            text.setText(this.parentHeaders[groupPosition]);
            if (groupPosition % 2 == 0) {
                mLlRowParentText.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            } else {
                mLlRowParentText.setBackgroundColor(getContext().getResources().getColor(R.color.colorMegamenu));
            }

            if (isExpanded) {
                mImgdown.setImageResource(R.drawable.ic_arrow_up1);
            } else {
                mImgdown.setImageResource(R.drawable.ic_arrow_down1);
            }

            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            final SecondLevelExpandableListView secondLevelELV = new SecondLevelExpandableListView(context);
            String[] mChildNames = mSecondLevel.get(groupPosition);
            String[] mChildSlugs = mSecondLevelSlug.get(groupPosition);
            String[] mChildImages = mSecondLevelImages.get(groupPosition);
            ArrayList<String[]> mSubChildData = new ArrayList<>();
            ArrayList<String[]> mSubChildDataImages = new ArrayList<>();
            ArrayList<String[]> mSubChildDataSlug = new ArrayList<>();
            HashMap<String, String[]> mThirdLevelData = mThirdLevel.get(groupPosition);
            HashMap<String, String[]> mThirdLevelDataSlug = mThirdLevelSlug.get(groupPosition);
            HashMap<String, String[]> mThirdLevelDataImage = mThirdLevelImage.get(groupPosition);

            for (String key : mThirdLevelData.keySet()) {
                mSubChildData.add(mThirdLevelData.get(key));
            }

            for (String key : mThirdLevelDataSlug.keySet()) {
                mSubChildDataSlug.add(mThirdLevelDataSlug.get(key));
            }

            for (String key : mThirdLevelDataImage.keySet()) {
                mSubChildDataImages.add(mThirdLevelDataImage.get(key));
            }

            secondLevelELV.setAdapter(new SecondLevelAdapter(context, mChildNames, mChildSlugs, mChildImages, mSubChildData, mSubChildDataSlug, mSubChildDataImages));
            secondLevelELV.setGroupIndicator(null);
            secondLevelELV.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                int previousGroup = -1;

                @Override
                public void onGroupExpand(int groupPosition) {
                    if (groupPosition != previousGroup)
                        secondLevelELV.collapseGroup(previousGroup);
                    previousGroup = groupPosition;
                }
            });

            return secondLevelELV;
        }


        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }
    //endregion

    //region SecondLevelAdapter
    public class SecondLevelAdapter extends BaseExpandableListAdapter {

        private Context context;

        List<String[]> mSubChildName;
        List<String[]> mSubChildSlug;
        List<String[]> mSubChildImage;

        String[] mChildNames;
        String[] mChildSlugs;
        String[] mChildImages;

        public SecondLevelAdapter(Context context, String[] mChildNames, String[] mChildSlugs, String[] mChildImages,
                                  List<String[]> mSubChildName, List<String[]> mSubChildSlug, List<String[]> mSubChildImage) {
            this.context = context;
            this.mChildNames = mChildNames;
            this.mChildSlugs = mChildSlugs;
            this.mChildImages = mChildImages;

            this.mSubChildName = mSubChildName;
            this.mSubChildSlug = mSubChildSlug;
            this.mSubChildImage = mSubChildImage;
        }

        @Override
        public Object getGroup(int groupPosition) {

            return mChildNames[groupPosition];
        }

        public Object getGroupSlug(int groupPosition) {

            return mChildSlugs[groupPosition];
        }

        public Object getGroupImage(int groupPosition) {

            return mChildImages[groupPosition];
        }

        @Override
        public int getGroupCount() {
            return mChildNames.length;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_second, null);
            TextView text = (TextView) convertView.findViewById(R.id.rowSecondText);
            final ImageView imagedown = convertView.findViewById(R.id.imgdown);
            final ImageView mImgSecondLevel = convertView.findViewById(R.id.imgSecondLevel);
            text.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor) != "") {
                text.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            }
            final String groupText = getGroup(groupPosition).toString();
            final String groupTextSlug = getGroupSlug(groupPosition).toString();
            final String groupImage = getGroupImage(groupPosition).toString();
//            Toast.makeText(getContext(), groupTextSlug, Toast.LENGTH_SHORT).show();
            text.setText(groupText);

            //region Image
            String picUrl = null;
            try {
                URL urla = null;
                /*images = images.replace("[", "");
                image = image.replace("]", "");*/
                urla = new URL(groupImage.replaceAll("%20", " "));
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.get()
                        .load(picUrl)
                        .into(mImgSecondLevel, new Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError(Exception e) {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion

            try {
                if (mSubChildName.get(groupPosition).length > 0) {
                    if (isExpanded) {
                        imagedown.setImageResource(R.drawable.ic_arrow_up1);
                    } else {
                        imagedown.setVisibility(View.VISIBLE);
                    }
                } else {
                    imagedown.setVisibility(View.GONE);
                    text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mListener = (OnFragmentInteractionListener) getContext();
                            mListener.gotoProductListing(groupTextSlug, groupText);
//                        Toast.makeText(getContext(), groupText , Toast.LENGTH_SHORT).show();
                            mSecondLevelName.clear();
                            mFirstLevelData.clear();
                            mSecondLevelSlug.clear();
                            mSecondLevelImages.clear();
                            mFirstLevelSlugData.clear();
                            mFirstLevelImageData.clear();
                            count = 0;
                        }
                    });
                }
            } catch (IndexOutOfBoundsException exception) {
                exception.printStackTrace();
            }

            return convertView;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            String[] childData;
            childData = mSubChildName.get(groupPosition);
            return childData[childPosition];
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_third, null);
            final RecyclerView mRvThirdLevel = convertView.findViewById(R.id.rvThirdLevel);
            String[] childArray = mSubChildName.get(groupPosition);
            String[] childArraySlug = mSubChildSlug.get(groupPosition);
            String[] childArrayImage = mSubChildImage.get(groupPosition);

            MegamenuThirdLevelAdapter megamenuThirdLevelAdapter = new MegamenuThirdLevelAdapter(getContext(), MegaMenuCategoryFragment.this, childArray, childArraySlug, childArrayImage);
            mRvThirdLevel.setLayoutManager(new GridLayoutManager(getContext(), 2));
            mRvThirdLevel.setAdapter(megamenuThirdLevelAdapter);
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            String[] children = mSubChildName.get(groupPosition);
            /*return children.length;*/
            return 1;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }
    //endregion

    //region SecondLevelExpandableListView
    public class SecondLevelExpandableListView extends ExpandableListView {

        public SecondLevelExpandableListView(Context context) {
            super(context);
        }

        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            //999999 is a size in pixels. ExpandableListView requires a maximum height in order to do measurement calculations.
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(999999, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    //endregion

    public int GetDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

}
