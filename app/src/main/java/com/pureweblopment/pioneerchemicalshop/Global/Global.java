package com.pureweblopment.pioneerchemicalshop.Global;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.pureweblopment.pioneerchemicalshop.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


public class Global {
    public static String Content_Type = "application/json";

    //local BazarBit Front APP Details
    public static String App_Id = "8ec181e8967390c800d25df09474302b";
    public static String app_secret = "230c47b445dbd52db4cf200e81c44ad3 ";

   /* //local BazarBit Clothes APP Details
    public static String App_Id = "3e231498db9ae07057ea86a414ac6428";
    public static String app_secret = "332081b2c63361c8ada7833865407660";*/

    //local BazarBit Fancy shop APP Details
    /*public static String App_Id = "8852ea67051111df4747f1311cdfc2ed";
    public static String app_secret = "6967e49910b3b3aeac3b61a6ad8f378d";*/

    /*//Live APP Details
    public static String App_Id = "6159cd3d72b73199ef63bbd51aac3e0a";
    public static String app_secret = "bad8dd8af65cb7ce803a1f948d567bc6";*/

    //Live APP Details
   /* public static String App_Id = "c7af2adbd4ec4aec8e8e6bda9670126a";
    public static String app_secret = "8dfa2d6bed5eb54013d94c2a692f683f";*/

    public static String USERTOKEN = "usertoken";
    public static String USERID = "user_id";
    public static String USER_EMAIL = "useremail";
    public static String USER_Name = "username";
    public static String USER_Profile_Picture = "userprofileimage";

    public static String LOGIN_PREFERENCE = "LoginPreference";
    public static String APPSetting_PREFERENCE = "APPSettingPreference";
    public static String preferenceNameGuestUSer = "GuestPreference";

    public static String PREFERENCECURRENCYSCREEN = "preferencecurrencyscreen";
    public static String PREFERENCECURRENCY = "preferencecurrency";

    public static String Shipping_Preference = "ShippingPreference";
    public static String Billing_Preference = "BillingPreference";
    public static String CheckoutTotalPrice = "CheckoutTotalPricePreference";
    public static String ISCheck = "ischeckPreference";
    public static String ISPromocode = "isPromocodePreference";
    public static String WishlistCountPreference = "wishlistcountpreference";
    public static String IsClickPreference = "isclickpreference";

    public static String SessionId = "SessionId";

    public static final String DEVICEINFOPreference = "deviceinfoPreference";
    public static String DeviceName = "DeviceName";
    public static String DeviceOs = "DeviceOs";
    public static String App_Version = "App_Version";

    public static String devicename = "";
    public static String deviceos = "";
    public static String app_version = "";

    public static Date date = null;
    public static SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
    public static SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
    public static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private static SecureRandom random = new SecureRandom();

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //region for send mail to developer..
    public static final String EMAIL = "appcrashreportppc@gmail.com";
    public static final String PASSWORD = "ppckart@2017";
    public static final String TOEMAIL = "atul@parghiinfotech.com";
    public static final String SUBJECT = "Pioneer Chemical Shop Exception Report";
    //endregion

    public static void Toast(Context context, String string) {
        Toast.makeText(context, string, Toast.LENGTH_LONG).show();
    }

    //region For make static url...
    public static String queryStringUrl(Context context) {
        try {
            devicename = SharedPreference.GetPreference(context, Global.DEVICEINFOPreference, Global.DeviceName);
            deviceos = SharedPreference.GetPreference(context, Global.DEVICEINFOPreference, Global.DeviceOs);
            app_version = SharedPreference.GetPreference(context, Global.DEVICEINFOPreference, Global.App_Version);
            devicename = devicename.replace(" ", "");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return "?app_type=Android&app_version=" + app_version + "&device_name=" + devicename + "&system_version=" +
                deviceos;
    }
    //endregion

    //region FOR GENERATE JSON OBJECT...
    public static JSONObject bodyParameter(String[] key, String[] val) {
        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < key.length; i++) {
            try {
                jsonObject.put(key[i], val[i]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }
    //endregion

    //region FOR CHANGE DATE FORMATE...
    public static String changeDateFormate(String dateString, String yourFormate, String requiredFormate) {
        SimpleDateFormat sdf = new SimpleDateFormat(yourFormate);//set format of date you receiving from db
        Date date = null;
        SimpleDateFormat newDate = null;
        try {
            date = (Date) sdf.parse(dateString);
            newDate = new SimpleDateFormat(requiredFormate, Locale.US);//set format of new date

        } catch (ParseException | NullPointerException e) {
            e.printStackTrace();
        }
        return newDate.format(date);
    }
    //endregion

    //region For Generate Header..
    public static HashMap<String, String> headers() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", Content_Type);
        headers.put("app-id", App_Id);
        headers.put("app-secret", app_secret);
        return headers;
    }
    //endregion

    //region For Generate Header
    public static HashMap<String, String> headers2(Context mContext) {
        String strAuthToken = SharedPreference.GetPreference(mContext, Global.LOGIN_PREFERENCE, Global.USERTOKEN);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", Content_Type);
        headers.put("app-id", App_Id);
        headers.put("app-secret", app_secret);
        headers.put("auth-token", "");
        return headers;
    }
    //endregion

    //region For Generate Header
    public static HashMap<String, String> headers1(Context mContext) {
        String strAuthToken = SharedPreference.GetPreference(mContext, Global.LOGIN_PREFERENCE, Global.USERTOKEN);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", Content_Type);
        headers.put("app-id", App_Id);
        headers.put("app-secret", app_secret);
        headers.put("auth-token", strAuthToken);
        return headers;
    }
    //endregion

    //region For random string in 32 bit
    public static String SessionId() {
        return new BigInteger(130, random).toString(32);
    }
    //endregion

    //region Check Internet Connection
    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo activeNetworkInfo = null;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    //endregion

    //region For Snackbar internet connection
    public static void internetcon(CoordinatorLayout coordinatorLayout) {
        Snackbar.make(coordinatorLayout, "Kindly check your internet connection...!", 5000).show();
    }
    //endregion

    //region FOR DISPLAY SNACKBAR GLOBALY...
    public static void showSnackBar(CoordinatorLayout coordinatorLayout, String message) {
        Snackbar.make(coordinatorLayout, message, 5000).show();
    }
    //endregion

    //region DateDialog...
    @SuppressLint("ValidFragment")
    public static class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        Calendar myCalendar;
        TextView textView;
        boolean fromBooking;

        public DateDialog(View v, boolean fromBooking) {
            textView = (TextView) v;
            this.fromBooking = fromBooking;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceSateate) {

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            myCalendar = Calendar.getInstance();

            if (textView.getText().toString().equals("")) {
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
            } else {
                String textDate = textView.getText().toString();
                try {
                    if (!textDate.equals("")) {
                        date = dateFormat.parse(textDate);
                        day = Integer.parseInt(dayFormat.format(date));
                        month = Integer.parseInt(monthFormat.format(date)) - 1;
                        year = Integer.parseInt(yearFormat.format(date));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            datePickerDialog.setTitle("");
            datePickerDialog.show();
            if (fromBooking)
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
            datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "", datePickerDialog);

            return datePickerDialog;

        }

        public void onDateSet(DatePicker view, int year, int month, int day) {

            textView.setText(populateSetDate(year, month + 1, day));

        }

        private String populateSetDate(int year, int i, int day) {

            String y = String.valueOf(year);
            String m = String.valueOf(i);
            String d = String.valueOf(day);

            String _Date = y + "-" + m + "-" + d;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat fmt2 = new SimpleDateFormat("dd MMMM yyyy");
            SimpleDateFormat fmt3 = new SimpleDateFormat("dd-MM-yyyy");
            try {
                Date date = fmt.parse(_Date);
                return fmt3.format(date);
            } catch (ParseException pe) {

                return "Date";
            }
        }

    }
    //endregion

    //region DateDialogFromTo...
    @SuppressLint("ValidFragment")
    public static class DateDialogFromTo extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        Calendar myCalendar;
        TextView textView;
        boolean fromBooking;
        Context context;

        public DateDialogFromTo(Context context, View v, boolean fromBooking) {
            textView = (TextView) v;
            this.fromBooking = fromBooking;
            this.context = context;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceSateate) {

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            myCalendar = Calendar.getInstance();

            if (textView.getText().toString().equals("")) {
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
            } else {
                String textDate = textView.getText().toString();
                try {
                    if (!textDate.equals("")) {
                        date = dateFormat.parse(textDate);
                        day = Integer.parseInt(dayFormat.format(date));
                        month = Integer.parseInt(monthFormat.format(date)) - 1;
                        year = Integer.parseInt(yearFormat.format(date));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            DatePickerDialog datePickerDialog = new DatePickerDialog(context, this, year, month, day);
            String openning_date = SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, StaticUtility.Fromdate);
            SimpleDateFormat f1 = new SimpleDateFormat("yyyy-MM-dd");
            long milliseconds = 0;
            SimpleDateFormat f = new SimpleDateFormat("dd-MMM-yyyy");
            try {
                Date d = f1.parse(openning_date);
                openning_date = f.format(d);
                d = f.parse(openning_date);
                milliseconds = d.getTime();
            } catch (ParseException | NullPointerException e) {
                e.printStackTrace();
            }

            if (fromBooking)
                datePickerDialog.getDatePicker().setMinDate(milliseconds);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
            datePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "", datePickerDialog);

            return datePickerDialog;

        }

        public void onDateSet(DatePicker view, int year, int month, int day) {

            textView.setText(populateSetDate1(year, month + 1, day));

            if (!fromBooking) {
                SharedPreference.CreatePreference(getActivity(), Global.LOGIN_PREFERENCE);
                SharedPreference.SavePreference(StaticUtility.Fromdate, populateSetDate(year, month + 1, day));
            }

        }

        private String populateSetDate(int year, int i, int day) {

            String y = String.valueOf(year);
            String m = String.valueOf(i);
            String d = String.valueOf(day);

            String _Date = y + "-" + m + "-" + d;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat fmt2 = new SimpleDateFormat("dd MMMM yyyy");
            SimpleDateFormat fmt3 = new SimpleDateFormat("dd-MM-yyyy");
            try {
                Date date = fmt.parse(_Date);
                return fmt.format(date);
            } catch (ParseException pe) {

                return "Date";
            }
        }

        private String populateSetDate1(int year, int i, int day) {

            String y = String.valueOf(year);
            String m = String.valueOf(i);
            String d = String.valueOf(day);

            String _Date = y + "-" + m + "-" + d;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat fmt2 = new SimpleDateFormat("dd MMMM yyyy");
            SimpleDateFormat fmt3 = new SimpleDateFormat("dd-MM-yyyy");
            try {
                Date date = fmt.parse(_Date);
                return fmt3.format(date);
            } catch (ParseException pe) {

                return "Date";
            }
        }

    }
    //endregion

    //region decodeFile
    public static Bitmap decodeFile(File f, int WIDTH, int HIGHT) {
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }
    //endregion

    //region OpenSystemKeyBoard
    public static void OpenSystemKeyBoard(View view, Activity activity) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }
    //endregion

    //region Keyboard Hide
    public static void HideSystemKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    //endregion

    //region DeviceInfo
    public static void DeviceInfo(Context mContext) {
        String model = Build.MODEL;
        String os = Build.VERSION.RELEASE;
        PackageManager manager = mContext.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    mContext.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            ///Creating SendMail object
            SendMail sm = new SendMail(mContext, Global.TOEMAIL, Global.SUBJECT, "Getting error in LoginFragment.java When parsing Error response.\n" + e.toString());
            //Executing sendmail to send email
            sm.execute();
        }
        assert info != null;
        String version = info.versionName;
        SharedPreference.CreatePreference(mContext, Global.DEVICEINFOPreference);
        SharedPreference.SavePreference(Global.DeviceName, model);
        SharedPreference.SavePreference(Global.DeviceOs, os);
        SharedPreference.SavePreference(Global.App_Version, version);
    }//endregion

    public static void MakeTextViewResizable(final Context mContext, final TextView tv,
                                             final int maxLine, final String expandText,
                                             final boolean viewMore) {
        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        String string = tv.getText().toString();
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                String text;
                int lineEndIndex;
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    lineEndIndex = tv.getLayout().getLineEnd(0);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else if (tv.getLineCount() < maxLine) {
                    lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    text = String.valueOf(tv.getText().subSequence(0, lineEndIndex));
                } else {
                    lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                }
                tv.setText(text);
                tv.setMovementMethod(LinkMovementMethod.getInstance());
                tv.setText(addClickablePartTextViewResizable(mContext, Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                        viewMore), TextView.BufferType.SPANNABLE);
            }
        });
    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Context mContext, final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new MySpannable(false, mContext) {
                @Override
                public void onClick(View widget) {
                    tv.setLayoutParams(tv.getLayoutParams());
                    String string = tv.getTag().toString();
                    tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                    if (viewMore) {
                        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREAD_LESS) != null) {
                            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREAD_LESS).equals("")) {
                                MakeTextViewResizable(mContext, tv, -1, SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREAD_LESS), false);
                            } else {
                                MakeTextViewResizable(mContext, tv, -1, mContext.getString(R.string.read_less), false);
                            }
                        } else {
                            MakeTextViewResizable(mContext, tv, -1, mContext.getString(R.string.read_less), false);
                        }
                    } else {
                        if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREAD_MORE) != null) {
                            if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREAD_MORE).equals("")) {
                                MakeTextViewResizable(mContext, tv, 3, SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE,
                                        StaticUtility.sREAD_MORE), true);
                            } else {
                                MakeTextViewResizable(mContext, tv, 3, mContext.getString(R.string.read_more), true);
                            }
                        } else {
                            MakeTextViewResizable(mContext, tv, 3, mContext.getString(R.string.read_more), true);
                        }
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;
    }

}
