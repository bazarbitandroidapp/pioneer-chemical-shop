package com.pureweblopment.pioneerchemicalshop.Global;

import android.content.Context;
import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

public class MySpannable extends ClickableSpan {

    private boolean isUnderline = false;
    private Context mContext;

    /**
     * Constructor
     */
    public MySpannable(boolean isUnderline, Context mContext) {
        this.isUnderline = isUnderline;
        this.mContext = mContext;
    }

    @Override
    public void updateDrawState(TextPaint ds) {

        ds.setUnderlineText(isUnderline);
        ds.setColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        /*ds.setColor(mContext.getResources().getColor(R.color.colorPrimary));*/
        ds.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
        float scaledSizeInPixels = 16 * mContext.getResources().getDisplayMetrics().scaledDensity;
        ds.setTextSize(scaledSizeInPixels);

    }

    @Override
    public void onClick(View widget) {

    }
}
