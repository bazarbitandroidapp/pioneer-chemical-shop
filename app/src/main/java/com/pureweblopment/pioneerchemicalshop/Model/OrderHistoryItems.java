package com.pureweblopment.pioneerchemicalshop.Model;

/**
 * Created by divya on 16/11/17.
 */

public class OrderHistoryItems {
    String order_datetime;
    String order_product_amount;
    String order_item_payment_status;
    String currency_symbol;
    String currency_position;

    public void setOrder_datetime(String order_datetime) {
        this.order_datetime = order_datetime;
    }

    public void setOrder_product_amount(String order_product_amount) {
        this.order_product_amount = order_product_amount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrder_item_status(String order_item_status) {
        this.order_item_status = order_item_status;
    }

    public void setImage(String image) {
        this.image = image;
    }

    String name;
    String order_item_status;
    String image;

    public String getOrder_datetime() {
        return order_datetime;
    }

    public String getOrder_product_amount() {
        return order_product_amount;
    }

    public String getName() {
        return name;
    }

    public String getOrder_item_status() {
        return order_item_status;
    }

    public String getImage() {
        return image;
    }

    public OrderHistoryItems(String order_datetime, String order_product_amount, String name,
                             String order_item_status, String image,
                             String order_item_payment_status, String currency_symbol, String currency_position) {
        this.order_datetime = order_datetime;
        this.order_product_amount = order_product_amount;
        this.name = name;
        this.order_item_status = order_item_status;
        this.image = image;
        this.order_item_payment_status = order_item_payment_status;
        this.currency_symbol = currency_symbol;
        this.currency_position = currency_position;
    }

    public String getOrder_item_payment_status() {
        return order_item_payment_status;
    }

    public String getCurrency_symbol() {
        return currency_symbol;
    }

    public String getCurrency_position() {
        return currency_position;
    }
}
