package com.pureweblopment.pioneerchemicalshop.Model;

/**
 * Created by divya on 16/9/17.
 */

public class Reviews {
    String UserName;
    String ReviewsDate;
    String Rating;
    String Description;

    public Reviews(String userName, String reviewsDate, String rating, String description) {
        UserName = userName;
        ReviewsDate = reviewsDate;
        Rating = rating;
        Description = description;
    }

    public String getUserName() {
        return UserName;
    }

    public String getReviewsDate() {
        return ReviewsDate;
    }

    public String getRating() {
        return Rating;
    }

    public String getDescription() {
        return Description;
    }
}
