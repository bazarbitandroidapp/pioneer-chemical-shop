package com.pureweblopment.pioneerchemicalshop.Model;

/**
 * Created by divya on 27/10/17.
 */

public class Feedback extends Item {
    String ordername;
    String orderId;
    String orderDate;
    String Price;
    String feedback_id;
    String image;
    String orderStatus;
    String product_id;
    String orderitem_id;
    String currency_symbol;
    String currency_position;

    public Feedback(String ordername, String orderId, String orderDate, String price, String feedback_id, String image,
                    String orderStatus, String product_id, String orderitem_id, String currency_symbol, String currency_position) {
        this.ordername = ordername;
        this.orderId = orderId;
        this.orderDate = orderDate;
        Price = price;
        this.feedback_id = feedback_id;
        this.image = image;
        this.orderStatus = orderStatus;
        this.product_id = product_id;
        this.orderitem_id = orderitem_id;
        this.currency_symbol = currency_symbol;
        this.currency_position = currency_position;
    }

    public String getCurrency_symbol() {
        return currency_symbol;
    }

    public String getCurrency_position() {
        return currency_position;
    }

    public String getOrdername() {
        return ordername;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public String getPrice() {
        return Price;
    }

    public String getFeedback_id() {
        return feedback_id;
    }

    public String getImage() {
        return image;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getOrderitem_id() {
        return orderitem_id;
    }
}
