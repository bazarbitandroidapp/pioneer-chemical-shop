package com.pureweblopment.pioneerchemicalshop.Model;

/**
 * Created by divya on 10/4/18.
 */

public class CMSPages {
    String title;
    String slug;
    String is_Select;

    public CMSPages(String title, String slug, String is_Select) {
        this.title = title;
        this.slug = slug;
        this.is_Select = is_Select;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getIs_Select() {
        return is_Select;
    }

    public void setIs_Select(String is_Select) {
        this.is_Select = is_Select;
    }
}
