package com.pureweblopment.pioneerchemicalshop.Model;

/**
 * Created by divya on 22/2/18.
 */

public class DefaultVariation {
    String z_proattritemrelid_pk;
    String z_productid_fk;
    String z_attributetermid_fk;
    String z_attributeid_fk;
    String z_productattrrelid_fk;
    String attritem_name;
    String name;
    String z_attributeid_pk;

    public DefaultVariation(String z_proattritemrelid_pk, String z_productid_fk,
                            String z_attributetermid_fk, String z_attributeid_fk,
                            String z_productattrrelid_fk, String attritem_name,
                            String name, String z_attributeid_pk) {
        this.z_proattritemrelid_pk = z_proattritemrelid_pk;
        this.z_productid_fk = z_productid_fk;
        this.z_attributetermid_fk = z_attributetermid_fk;
        this.z_attributeid_fk = z_attributeid_fk;
        this.z_productattrrelid_fk = z_productattrrelid_fk;
        this.attritem_name = attritem_name;
        this.name = name;
        this.z_attributeid_pk = z_attributeid_pk;
    }

    public String getZ_proattritemrelid_pk() {
        return z_proattritemrelid_pk;
    }

    public String getZ_productid_fk() {
        return z_productid_fk;
    }

    public String getZ_attributetermid_fk() {
        return z_attributetermid_fk;
    }

    public String getZ_attributeid_fk() {
        return z_attributeid_fk;
    }

    public String getZ_productattrrelid_fk() {
        return z_productattrrelid_fk;
    }

    public String getAttritem_name() {
        return attritem_name;
    }

    public String getName() {
        return name;
    }

    public String getZ_attributeid_pk() {
        return z_attributeid_pk;
    }
}
