package com.pureweblopment.pioneerchemicalshop.Model;

import java.io.Serializable;

/**
 * Created by divya on 28/10/17.
 */

public class AddressList implements Serializable {
    String FirstName;
    String LastName;
    String Address;
    String City;
    String Pincode;
    String State;
    String Phone_number;
    String userAddressID;
    String landmark;
    String country;
    String address_type;

    public AddressList(String firstName, String lastName, String address, String city, String pincode, String state,
                       String phone_number, String userAddressID, String landmark, String country, String address_type) {
        FirstName = firstName;
        LastName = lastName;
        Address = address;
        City = city;
        Pincode = pincode;
        State = state;
        Phone_number = phone_number;
        this.userAddressID = userAddressID;
        this.landmark = landmark;
        this.country = country;
        this.address_type = address_type;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getAddress() {
        return Address;
    }

    public String getCity() {
        return City;
    }

    public String getPincode() {
        return Pincode;
    }

    public String getState() {
        return State;
    }

    public String getPhone_number() {
        return Phone_number;
    }

    public String getUserAddressID() {
        return userAddressID;
    }

    public String getLandmark() {
        return landmark;
    }

    public String getCountry() {
        return country;
    }

    public String getAddress_type() {
        return address_type;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public void setCity(String city) {
        City = city;
    }

    public void setPincode(String pincode) {
        Pincode = pincode;
    }

    public void setState(String state) {
        State = state;
    }

    public void setPhone_number(String phone_number) {
        Phone_number = phone_number;
    }

    public void setUserAddressID(String userAddressID) {
        this.userAddressID = userAddressID;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setAddress_type(String address_type) {
        this.address_type = address_type;
    }
}
