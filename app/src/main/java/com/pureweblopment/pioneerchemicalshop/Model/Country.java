package com.pureweblopment.pioneerchemicalshop.Model;

/**
 * Created by divya on 4/1/18.
 */

public class Country {
    String CountryName;
    String CountryId;
    String Status;

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getCountryId() {
        return CountryId;
    }

    public void setCountryId(String countryId) {
        CountryId = countryId;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
