package com.pureweblopment.pioneerchemicalshop.Model;

import java.util.ArrayList;

/**
 * Created by divya on 25/10/17.
 */

public class MegaMenu {
    String menuname;
    ArrayList<ChildCategory> childCategories;

    public MegaMenu(String menuname, ArrayList<ChildCategory> childCategories) {
        this.menuname = menuname;
        this.childCategories = childCategories;
    }

    public String getMenuname() {
        return menuname;
    }

    public ArrayList<ChildCategory> getChildCategories() {
        return childCategories;
    }
}
