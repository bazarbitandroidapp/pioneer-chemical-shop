package com.pureweblopment.pioneerchemicalshop.Model;

public class Promocode {
    String promocode;
    String title;
    String details;
    String discount;

    public Promocode(String promocode, String title, String details, String discount) {
        this.promocode = promocode;
        this.title = title;
        this.details = details;
        this.discount = discount;
    }

    public String getPromocode() {
        return promocode;
    }

    public String getTitle() {
        return title;
    }

    public String getDetails() {
        return details;
    }

    public String getDiscount() {
        return discount;
    }
}
