package com.pureweblopment.pioneerchemicalshop.Model;

/**
 * Created by divya on 8/9/17.
 */

public class Category {
    String Category_image = "";
    String Category_Name = "";
    String slug = "";

    public Category(String category_image, String category_Name, String slug) {
        Category_image = category_image;
        Category_Name = category_Name;
        this.slug = slug;
    }

    public Category(String category_Name, String slug) {
        Category_Name = category_Name;
        this.slug = slug;
    }

    public String getCategory_image() {
        return Category_image;
    }

    public String getCategory_Name() {
        return Category_Name;
    }

    public String getSlug() {
        return slug;
    }
}
