package com.pureweblopment.pioneerchemicalshop.Model;

/**
 * Created by divya on 21/12/17.
 */

public class Wishlist extends Item {
    String ProductName;
    String ProductDescription;
    String ProductPrice;
    String ProductImage;
    String quantity;
    String stock_status;
    String orderwishlist_id;
    String product_id;

    public Wishlist(String productName, String productDescription, String productPrice,
                    String productImage, String quantity, String stock_status,
                    String orderwishlist_id, String product_id) {
        ProductName = productName;
        ProductDescription = productDescription;
        ProductPrice = productPrice;
        ProductImage = productImage;
        this.quantity = quantity;
        this.stock_status = stock_status;
        this.orderwishlist_id = orderwishlist_id;
        this.product_id = product_id;
    }

    public String getProductName() {
        return ProductName;
    }

    public String getProductDescription() {
        return ProductDescription;
    }

    public String getProductPrice() {
        return ProductPrice;
    }

    public String getProductImage() {
        return ProductImage;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getStock_status() {
        return stock_status;
    }

    public String getOrderwishlist_id() {
        return orderwishlist_id;
    }

    public String getProduct_id() {
        return product_id;
    }
}
