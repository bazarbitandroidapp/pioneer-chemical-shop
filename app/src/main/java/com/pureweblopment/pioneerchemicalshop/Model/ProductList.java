package com.pureweblopment.pioneerchemicalshop.Model;

/**
 * Created by divya on 13/9/17.
 */

public class ProductList {
    String Productname;
    String Productprice;
    String Productsale_price;
    String ProductId;
    String main_image;
    String slug;
    String exists_in_wishlist;
    String Rating;
    String pre_booking_enabled;
    String pre_booking_startdate;
    String pre_booking_enddate;
    String currentdatetime;

    public ProductList(String productname, String productprice, String productsale_price, String productId,
                       String main_image, String slug, String exists_in_wishlist,
                       String Rating, String pre_booking_enabled,
                       String pre_booking_startdate, String pre_booking_enddate, String currentdatetime) {
        Productname = productname;
        Productprice = productprice;
        Productsale_price = productsale_price;
        ProductId = productId;
        this.main_image = main_image;
        this.slug = slug;
        this.exists_in_wishlist = exists_in_wishlist;
        this.Rating = Rating;
        this.pre_booking_enabled = pre_booking_enabled;
        this.pre_booking_startdate = pre_booking_startdate;
        this.pre_booking_enddate = pre_booking_enddate;
        this.currentdatetime = currentdatetime;
    }

    public String getProductname() {
        return Productname;
    }

    public String getProductprice() {
        return Productprice;
    }

    public String getProductsale_price() {
        return Productsale_price;
    }

    public String getProductId() {
        return ProductId;
    }

    public String getMain_image() {
        return main_image;
    }

    public String getSlug() {
        return slug;
    }

    public String getExists_in_wishlist() {
        return exists_in_wishlist;
    }

    public String getRating() {
        return Rating;
    }

    public void setProductname(String productname) {
        Productname = productname;
    }

    public void setProductprice(String productprice) {
        Productprice = productprice;
    }

    public void setProductsale_price(String productsale_price) {
        Productsale_price = productsale_price;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setExists_in_wishlist(String exists_in_wishlist) {
        this.exists_in_wishlist = exists_in_wishlist;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getPre_booking_enabled() {
        return pre_booking_enabled;
    }

    public String getPre_booking_startdate() {
        return pre_booking_startdate;
    }

    public String getPre_booking_enddate() {
        return pre_booking_enddate;
    }

    public String getCurrentdatetime() {
        return currentdatetime;
    }
}
