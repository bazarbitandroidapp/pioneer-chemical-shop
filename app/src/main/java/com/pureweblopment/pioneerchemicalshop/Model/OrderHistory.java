package com.pureweblopment.pioneerchemicalshop.Model;

import java.util.ArrayList;

/**
 * Created by divya on 31/10/17.
 */

public class OrderHistory extends Item {
    String order_id;
    String order_payment_type;
    String is_prebooking;
    ArrayList<OrderHistoryItems> arrayOrderHistoryItems;

    public OrderHistory(String order_id, String order_payment_type,
                        ArrayList<OrderHistoryItems> arrayOrderHistoryItems, String is_prebooking) {
        this.order_id = order_id;
        this.order_payment_type = order_payment_type;
        this.arrayOrderHistoryItems = arrayOrderHistoryItems;
        this.is_prebooking = is_prebooking;
    }

    public String getOrder_id() {
        return order_id;
    }

    public ArrayList<OrderHistoryItems> getArrayOrderHistoryItems() {
        return arrayOrderHistoryItems;
    }

    public String getOrder_payment_type() {
        return order_payment_type;
    }

    public void setOrder_payment_type(String order_payment_type) {
        this.order_payment_type = order_payment_type;
    }

    public String getIs_prebooking() {
        return is_prebooking;
    }
}
