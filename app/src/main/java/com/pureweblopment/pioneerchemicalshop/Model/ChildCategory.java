package com.pureweblopment.pioneerchemicalshop.Model;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by divya on 25/10/17.
 */

public class ChildCategory {
    String ChildCategoryName;
    String ChildCategorySlug;
    ArrayList<SubCategory> subCategories;
    JSONArray jsonArray;

    public ChildCategory(String childCategoryName, String childCategorySlug, ArrayList<SubCategory> subCategories) {
        ChildCategoryName = childCategoryName;
        ChildCategorySlug = childCategorySlug;
        subCategories = subCategories;
    }

    public ChildCategory(String childCategoryName, String childCategorySlug, JSONArray jsonArray) {
        ChildCategoryName = childCategoryName;
        ChildCategorySlug = childCategorySlug;
        this.jsonArray = jsonArray;
    }

    public ChildCategory(String childCategoryName, String childCategorySlug) {
        ChildCategoryName = childCategoryName;
        ChildCategorySlug = childCategorySlug;
    }

    public String getChildCategoryName() {
        return ChildCategoryName;
    }

    public String getChildCategorySlug() {
        return ChildCategorySlug;
    }

    public ArrayList<SubCategory> getSubCategories() {
        return subCategories;
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }
}
