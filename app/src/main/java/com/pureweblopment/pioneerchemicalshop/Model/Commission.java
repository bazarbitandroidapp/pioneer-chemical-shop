package com.pureweblopment.pioneerchemicalshop.Model;

public class Commission {
    String CommissionId;
    String CommissionPercentage;
    String CommissionAmount;
    String zOrderidfk;
    String zUseridfk;
    String CommissionOkReason;
    String CommissionRejectReason;
    String UserName;
    String order_id;

    public Commission(String commissionId, String commissionPercentage, String commissionAmount,
                      String zOrderidfk, String zUseridfk, String commissionOkReason,
                      String commissionRejectReason, String userName, String order_id) {
        CommissionId = commissionId;
        CommissionPercentage = commissionPercentage;
        CommissionAmount = commissionAmount;
        this.zOrderidfk = zOrderidfk;
        this.zUseridfk = zUseridfk;
        CommissionOkReason = commissionOkReason;
        CommissionRejectReason = commissionRejectReason;
        UserName = userName;
        this.order_id = order_id;
    }

    public String getCommissionId() {
        return CommissionId;
    }

    public String getCommissionPercentage() {
        return CommissionPercentage;
    }

    public String getCommissionAmount() {
        return CommissionAmount;
    }

    public String getzOrderidfk() {
        return zOrderidfk;
    }

    public String getzUseridfk() {
        return zUseridfk;
    }

    public String getCommissionOkReason() {
        return CommissionOkReason;
    }

    public String getCommissionRejectReason() {
        return CommissionRejectReason;
    }

    public String getUserName() {
        return UserName;
    }

    public String getOrder_id() {
        return order_id;
    }
}
